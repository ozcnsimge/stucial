# GET /api/register-data

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            X-RateLimit-Remaining: 59
            X-RateLimit-Limit: 60
            Cache-Control: no-cache, private

    + Body

            {"successful":true,"code":200,"response":{"universities":[{"id":1,"name":"Izmir University of Economics"}],"departments":[{"id":168,"name":"Accounting and Auditing Program"},{"id":128,"name":"Accounting and Tax Applications"},{"id":158,"name":"Aerospace Engineering"},{"id":129,"name":"Applied English and Translation"},{"id":130,"name":"Architectural Restoration"},{"id":185,"name":"Architecture"},{"id":131,"name":"Banking and Insurance"},{"id":159,"name":"Biomedical Engineering"},{"id":169,"name":"Business Administration"},{"id":147,"name":"Child Development"},{"id":180,"name":"Cinema and Digital Media"},{"id":132,"name":"Civil Aviation Cabin Services"},{"id":133,"name":"Civil Aviation Transportation Management"},{"id":160,"name":"Civil Engineering"},{"id":162,"name":"Computer Engineering"},{"id":134,"name":"Computer Programming"},{"id":135,"name":"Construction Technology"},{"id":157,"name":"Culinary Arts and Management"},{"id":170,"name":"Economics"},{"id":148,"name":"Elderly Care"},{"id":163,"name":"Electrical and Electronics Engineering"},{"id":189,"name":"Fashion and Textile Design"},{"id":164,"name":"Food Engineering"},{"id":136,"name":"Footwear Design and Manufacturing"},{"id":137,"name":"Foreign Trade"},{"id":138,"name":"Furniture and Decoration"},{"id":165,"name":"Genetics and Bioengineering"},{"id":139,"name":"Graphic Design"},{"id":183,"name":"Health Management"},{"id":186,"name":"Industrial Design"},{"id":187,"name":"Industrial Engineering"},{"id":188,"name":"Interior Architecture and Environmental Design"},{"id":140,"name":"Interior Design"},{"id":171,"name":"International Trade and Finance"},{"id":155,"name":"Justice"},{"id":141,"name":"Lavaboratory Technology"},{"id":174,"name":"Law"},{"id":156,"name":"Legal Office Management and Secretariat"},{"id":172,"name":"Logistics Management "},{"id":142,"name":"Marketing"},{"id":175,"name":"Mathematics"},{"id":166,"name":"Mechanical Engineering"},{"id":167,"name":"Mechatronics Engineering"},{"id":181,"name":"Media and Communication"},{"id":150,"name":"Medical Documentation and Secreteriat"},{"id":152,"name":"Medical Imaging Techniques"},{"id":151,"name":"Medical Laboratory Techniques"},{"id":184,"name":"Nursing"},{"id":143,"name":"Occupational Health and Safety"},{"id":153,"name":"Opticianry"},{"id":149,"name":"Paramedic"},{"id":177,"name":"Physics"},{"id":154,"name":"Physiotherapy"},{"id":173,"name":"Political Science and International Relations"},{"id":178,"name":"Psychology"},{"id":182,"name":"Public Relations and Advertising"},{"id":144,"name":"Radio and Tv Programming"},{"id":145,"name":"Real Estate and Real Estate Management"},{"id":179,"name":"Sociology"},{"id":161,"name":"Software Engineering"},{"id":146,"name":"Tourism and Hotel Management"},{"id":176,"name":"Translation and Interpretation (English)"},{"id":190,"name":"Visual Communication Design"}]}}


# POST /api/register

+ Request (application/json; charset=utf-8)

        {
            "student_id": "20130601026",
            "name": "Stucial Tester",
            "university": 1,
            "department": 185,
            "username": "demo",
            "password": "stucial2018"
        }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            X-RateLimit-Remaining: 38
            X-RateLimit-Limit: 60
            Cache-Control: no-cache, private

    + Body

            {"successful":true,"code":200,"response":{"id":17,"name":"Stucial Tester","username":"demo","email":null,"phone":null,"university":"Izmir University of Economics","department":"Architecture","token":"ZKJpH58B3oO7m0qgtkNu8mRSOQkt06BPFfyzwmYnUNc3NjvJKuxIJpataU9OdDJ1"}}


# POST /api/login

+ Request (application/json; charset=utf-8)

        {
            "username": "demo",
            "password": "stucial2018"
        }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            X-RateLimit-Remaining: 58
            X-RateLimit-Limit: 60
            Cache-Control: no-cache, private

    + Body

            {"successful":true,"code":200,"response":{"id":2,"name":"Stucial Tester","username":"demo","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/7324_200512_2_447326_16366.jpg","email":"demo@stucial.com","phone":"05436456686","university":"Izmir University of Economics","department":"Architecture","token":"FXUpOd52CgneoOpCDRcYnUXkfJf0ZEge2noR0SQxAcZIR1kedmylOhg2eOQTyHoH"}}


# GET /api/logout

+ Request

    + Headers

            Authorization: Bearer Gd4ocKiEWE1cwo7LTA9IhyzgFoAdV4pp0bmjtjIj6oGLUAob2cjUvFIZN8p0TKxc



+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 54

    + Body

            {"successful":true,"code":200,"response":[]}


# GET /api/schedule

+ Request

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR



+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 58

    + Body

            {"successful":true,"code":200,"response":{"4":[{"section":1515,"course":{"code":"SE 380","name":"Mobile Application Development"},"start":"09:00","end":"11:50","class":"C 206","students":2}]}}


# POST /api/search

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "query": ""
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 56

    + Body

            {"successful":true,"code":200,"response":[{"id":1,"name":"Furkan Kavlak","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/9110_836555_1_083754_58205.jpg","sub":"Software Engineering\nIzmir University of Economics","type":"student"},{"id":2,"name":"Stucial Tester","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/7324_200512_2_447326_16366.jpg","sub":"Architecture\nIzmir University of Economics","type":"student"},{"id":3,"name":"Simge \u00d6zcan","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/7736_213764_3_637175_32818.jpg","sub":"Software Engineering\nIzmir University of Economics","type":"student"},{"id":4,"name":"Simge G\u00fc\u00e7l\u00fckol","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/7002_507542_4_154219_13949.jpg","sub":"Industrial Engineering\nIzmir University of Economics","type":"student"},{"id":5,"name":"Hatice Ertu\u011frul","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/9838_945837_5_643458_79137.jpg","sub":"Software Engineering\nIzmir University of Economics","type":"student"},{"id":1,"name":"Oyun Geli\u015ftirme Tak\u0131m\u0131","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/5763_583744_1_616195_84358.jpg","sub":"\u00dclkemizde emekleme a\u015famas\u0131nda olan oyun sekt\u00f6r\u00fc, bu t\u00fcr organizasyonlar sayesinde ilerlemeye ve b\u00fcy\u00fcmeye devam ediyor. Bizim i\u00e7in bu \u00e7abalar \u00e7ok \u00f6nemli \u00e7\u00fcnk\u00fc T\u00fcrkiye'de halen lisans d\u00fczeyinde 'Bilgisayar Oyunlar\u0131' e\u011fitimi ilk ve tek olarak \u0130zmir Ekonomi \u00dcniversitesi'nde veriliyor.","type":"club"},{"id":2,"name":"Yaz\u0131l\u0131m Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/1721_246089_2_616173_84523.jpg","sub":"\u0130zmir Ekonomi \u00dcniversitesi Yaz\u0131l\u0131m Kul\u00fcb\u00fc yani IEU Software Community (ISC), 2017 y\u0131l\u0131 g\u00fcz d\u00f6neminde faaliyete ge\u00e7mi\u015f bir \u00f6\u011frenci toplulu\u011fudur. 2017-2018 \u00f6\u011fretim y\u0131l\u0131nda ba\u015fta M\u00fchendislik \u00f6\u011frencileri olmak \u00fczere okulumuzun t\u00fcm b\u00f6l\u00fcmlerinden aktif \u00fcyelerimiz bulunmaktad\u0131r. Toplulu\u011fumuzun amac\u0131 bili\u015fim sekt\u00f6r\u00fcne merakl\u0131 ve kendini bu alanda geli\u015ftirmek istenen \u00f6\u011frencilerle birlikte, yaz\u0131l\u0131m d\u00fcnyas\u0131na daha donan\u0131ml\u0131 insanlar yeti\u015ftirmeye yard\u0131mc\u0131 olurken sosyal etkinlikleri de g\u00f6z ard\u0131 etmemek.\r\n\r\n\u00dcniversite \u00f6\u011frencilerini i\u015f hayat\u0131na haz\u0131rlamaya ba\u015flay\u0131p, okul sonras\u0131 hayatlar\u0131nda ba\u011flant\u0131lar\u0131n\u0131 kurmalar\u0131nda yard\u0131mc\u0131 olmay\u0131 umuyoruz. Yapt\u0131\u011f\u0131m\u0131z e\u011fitim, seminer ve workshoplar sayesinde g\u00fcn\u00fcm\u00fcz d\u00fcnyas\u0131nda aktif rol oynayan yaz\u0131l\u0131m\u0131 herkesin hayat\u0131n\u0131n bir par\u00e7as\u0131 haline getirmek istiyoruz. Bunun yan\u0131nda sosyal sorumluluk projelerini de bir arada y\u00fcr\u00fct\u00fcyoruz.\r\n\r\nDetayl\u0131 bilgi: ieusoftwarecommunity.com","type":"club"},{"id":3,"name":"Motor Sporlar\u0131 Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/6119_905464_3_614097_65173.jpg","sub":"Motor sporlar\u0131na g\u00f6n\u00fcl veren, r\u00fczgar\u0131 her daim y\u00fcz\u00fcnde hissetmeyi sevenlerin kurdu\u011fu bu kul\u00fcp \u0130zmir Ekonomi \u00dcniversitesi merakl\u0131lar\u0131n\u0131 motor sporlar\u0131 hakk\u0131nda bilgilendirme misyonu ta\u015f\u0131yor. D\u00fczenlenmekte olan her \u00e7e\u015fit motor sporlar\u0131 organizasyonunda aktif veya pasif olarak yer almay\u0131 ve konu hakk\u0131nda uzman ki\u015fileri kampusa davet ederek seminerler d\u00fczenlemeyi hedefleyen kul\u00fcp motor sporlar\u0131 tutkunlar\u0131n\u0131n bulu\u015fma noktas\u0131.","type":"club"},{"id":4,"name":"Almanca Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/7398_171642_4_614196_73636.jpg","sub":"Kul\u00fcb\u00fcn amac\u0131, Almancan\u0131n yayg\u0131nla\u015ft\u0131r\u0131lmas\u0131 ve Almanca bilen ki\u015filerin meslek\u00ee, sosyal, ki\u015fisel, sanatsal ve k\u00fclt\u00fcrel geli\u015fimini sa\u011flayarak gerek meslek\u00ee gerekse sosyal ya\u015famda ki\u015filer aras\u0131nda almanca kullan\u0131m\u0131 sa\u011flamak, ki\u015filer aras\u0131ndaki ileti\u015fimi geli\u015ftirmek, Ekonomi \u00dcniversitesi \u00f6\u011frencileri almanca e\u011fitimi alm\u0131\u015f bulunan ki\u015filer ve \u00f6\u011frenciler aras\u0131nda Almancan\u0131n do\u011fru, d\u00fczg\u00fcn ve bilin\u00e7li kullan\u0131m\u0131 sa\u011flamakt\u0131r.\r\n\r\nEkonomi \u00dcniversitesi Almanca Kul\u00fcb\u00fc, kul\u00fcp y\u00f6netmeli\u011fi uyar\u0131nca kul\u00fcp dan\u0131\u015fman\u0131n\u0131n denetiminde \u00e7al\u0131\u015fmak \u00fczere kurulmu\u015ftur.","type":"club"},{"id":5,"name":"Alt K\u00fclt\u00fcr Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/7597_978178_5_614229_55202.jpg","sub":"Ekonomi \u00dcniversitesi Alt K\u00fclt\u00fcr Toplulu\u011fu olarak amac\u0131m\u0131z bilim kurgu, fantastik d\u00fcnya, \u00e7izgi roman, anime, oyun ve fantasy role play(fantastik rol yapma oyunu)  gibi ve benzeri alt k\u00fclt\u00fcr hobileriyle ilgilenen insanlar\u0131n bir araya gelmesini sa\u011flamak ve ortak hobilere sahip insanlar\u0131 birbiriyle tan\u0131\u015ft\u0131rmak, e\u011flenmek ama\u00e7l\u0131 etkinliklerle toplulu\u011fun aktifli\u011fini sa\u011flamakt\u0131r.","type":"club"},{"id":1,"name":"ACC 307","photo":null,"sub":"Audit and Assurance\nIzmir University of Economics","type":"course"},{"id":2,"name":"ACC 322","photo":null,"sub":"Corporate Law for Accountants\nIzmir University of Economics","type":"course"},{"id":3,"name":"ACC 401","photo":null,"sub":"Strategic Management Accounting and Control\nIzmir University of Economics","type":"course"},{"id":4,"name":"ACC 402","photo":null,"sub":"Essentials of Financial Statement Analysis\nIzmir University of Economics","type":"course"},{"id":5,"name":"ACC 420","photo":null,"sub":"Corporate Risk Management\nIzmir University of Economics","type":"course"}]}


# GET /api/profile/1

+ Request

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR



+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 58

    + Body

            {"successful":true,"code":200,"response":{"profile":{"id":1,"name":"Furkan Kavlak","username":"furkankavlak","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/9110_836555_1_083754_58205.jpg","university_id":1,"university":"Izmir University of Economics","department_id":161,"department":"Software Engineering","friendship":0,"blocked":false,"private":true,"counts":{"friends":10,"clubs":1,"courses":4}},"schedule":[]}}


# GET /api/profile/1/friends

+ Request

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR



+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 59

    + Body

            {"successful":true,"code":200,"response":[{"id":3,"name":"Simge \u00d6zcan","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/7736_213764_3_637175_32818.jpg","department":"Software Engineering"},{"id":4,"name":"Simge G\u00fc\u00e7l\u00fckol","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/7002_507542_4_154219_13949.jpg","department":"Industrial Engineering"},{"id":5,"name":"Hatice Ertu\u011frul","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/9838_945837_5_643458_79137.jpg","department":"Software Engineering"},{"id":7,"name":"Irem Kavlak","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAmklEQVRoge3ZwQmAMBAFUSMWYKkpJaWmBFtYYQnDZ95ZFoZ4WJJx1ax3Fr+cex2edhdnkdnAYAODDQw2MNjAkNDwtE+s73NdEs7BBgYbGGxgsIHBBoaEhnF+RWv3Y289fwlZnJbwL9nAYAODDQw2MNjAkLDzJZyDDQw2MNjAYAODDQw2MPS\/T\/deB1YknIMNDDYw2MBgA4MNDB8K+RK2E3qgMQAAAABJRU5ErkJggg==","department":"Interior Architecture and Environmental Design"},{"id":8,"name":"Hakan Eren","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAeElEQVRoge3asQ2AMAwAwYTJGJ3RWCEFgie6q63IL7eZ47zGzx1fL\/AADQ0aGjQ0aGjQ0KChQUODhgYNDRoaNDRoaNDQoKFBQ4OGBg0NGho0NGho0NCgoUFDg4YGDQ07NMzVwfWvi9f58ms73EFDg4YGDQ0aGjQ03LxHBYF86sXyAAAAAElFTkSuQmCC","department":"International Trade and Finance"},{"id":9,"name":"Ece Sa\u011fduyu","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/9521_245790_9_659571_24674.jpg","department":"Industrial Engineering"},{"id":10,"name":"Ezgi Ersoydan","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAkklEQVRoge3awQnDMBAAwSikQJeQ0lJCSkwJ0UPg4dh5mxOL\/BBI6\/N9bHpf\/7+5Zdpzd01YDYYaDDUYajDUYJjQsPZPaawJ+1CDoQZDDYYaDDUYajDUYKjBUIOhBkMNhhoMExpexyeevcbeMWEfajDUYKjBUIOhBsOEu911dtzZN4mbJvxLNRhqMNRgqMFQg+EH8uEMcTPzfz0AAAAASUVORK5CYII=","department":"Industrial Engineering"},{"id":11,"name":"Melis Tan","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAmUlEQVRoge3YwQ2AIBAFUddYoCVYgqVQgiVYoi38A+pkM+9MCBM4bKhzP5bMuK9w5Szh2daXj\/EFGxhsYLCBwQYGGxg6NFQ+82F1uAcbGGxgsIHBBgYbGGxg2KbvmHxpzh2WO9yDDQw2MNjAYAODDQwd\/vkqXPdXajJBdnhLNjDYwGADgw0MNjDYwGADgw0MNjDYwGADQ4eGB3wCDOGpkwzOAAAAAElFTkSuQmCC","department":"Industrial Engineering"},{"id":15,"name":"Buse Turgay","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAnklEQVRoge3ZwQmAMBAFUSMpwNIsyZIszRIswX9YyLjMO0vIsB4WHedxbT+3r75AARsYbGCwgcEGBhsYbGAYtcflW\/D9pE9+6jAHGxhsYLCBwQYGGxjmkg0nFN6twxxsYLCBwQYGGxhsYOjQMBr8202\/861KTZblDu+SDQw2MNjAYAODDQwdGmb5icmWVrtBdpiDDQw2MNjAYAODDQwv7NYPI+SiuqsAAAAASUVORK5CYII=","department":"Visual Communication Design"},{"id":16,"name":"Semih Pehlivan","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAh0lEQVRoge3ZwQkEIRAAwXW5wC60De1C2xR8DFwhXW9RGj8yrmvU8\/y2V36nDr2nNvqjGgw1GGow1GCowVCDoQZDDYYaDDUYajDUYFj7kznWCfdQg6EGQw2GGgw1GGowfMZ33Pl4nn0sn3APNRhqMNRgqMFQg6E5n6EGQw2GGgw1GGownNDwAuPCCgIjO4pUAAAAAElFTkSuQmCC","department":"Software Engineering"}]}


# GET /api/profile/1/clubs

+ Request

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR



+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 58

    + Body

            {"successful":true,"code":200,"response":[{"id":1,"name":"Oyun Geli\u015ftirme Tak\u0131m\u0131","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/5763_583744_1_616195_84358.jpg","description":"\u00dclkemizde emekleme a\u015famas\u0131nda olan oyun sekt\u00f6r\u00fc, bu t\u00fcr organizasyonlar sayesinde ilerlemeye ve b\u00fcy\u00fcmeye devam ediyor. Bizim i\u00e7in bu \u00e7abalar \u00e7ok \u00f6nemli \u00e7\u00fcnk\u00fc T\u00fcrkiye'de halen lisans d\u00fczeyinde 'Bilgisayar Oyunlar\u0131' e\u011fitimi ilk ve tek olarak \u0130zmir Ekonomi \u00dcniversitesi'nde veriliyor."}]}


# GET /api/course/778

+ Request

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR



+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 55

    + Body

            {"successful":true,"code":200,"response":{"details":{"id":778,"code":"SE 380","name":"Mobile Application Development","university":"Izmir University of Economics","is_enrolled":false},"sections":[{"id":1515,"times":[{"day":4,"start":"09:00","end":"11:50","class":"C 206\r"}]}],"students":[{"id":1,"name":"Furkan Kavlak","username":"furkankavlak","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/9110_836555_1_083754_58205.jpg","department":"Software Engineering"}],"files":[]}}


# GET /api/section/1515

+ Request

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR



+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 53

    + Body

            {"successful":true,"code":200,"response":{"id":1515,"course":{"id":778,"code":"SE 380","name":"Mobile Application Development"},"students":[{"id":1,"name":"Furkan Kavlak","username":"furkankavlak","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/9110_836555_1_083754_58205.jpg","department":"Software Engineering"}]}}


# POST /api/course/enroll

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "section": "1515"
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 59

    + Body

            {"successful":true,"code":200,"response":[]}


# POST /api/course/disenroll

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "course": "778"
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 57

    + Body

            {"successful":true,"code":200,"response":[]}


# GET /api/announcements

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {}

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 55

    + Body

            {"successful":true,"code":200,"response":[{"id":4,"title":"\u201cDijital \u00c7a\u011fda Bankac\u0131l\u0131k: \u0130\u015f Bankas\u0131 bunun neresinde?\u201d","date":"24\/04\/2018"},{"id":3,"title":"MEZUN\u0130YET T\u00d6REN\u0130","date":"20\/04\/2018"},{"id":2,"title":"Ozan Arslan TRT Radyo Haber'de","date":"01\/03\/2018"}]}


# GET /api/announcement/2

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {}

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 57

    + Body

            {"successful":true,"code":200,"response":{"id":2,"title":"Ozan Arslan TRT Radyo Haber'de","content":"Siyaset Bilimi ve Uluslararas\u0131 \u0130li\u015fkiler b\u00f6l\u00fcm\u00fc \u00f6\u011fretim \u00fcyesi Dr. Ozan Arslan her Cuma saat 13:15'te TRT Radyo Haber'in \"Radyo Ajandas\u0131\" program\u0131na konuk olarak uluslararas\u0131 ili\u015fkiler g\u00fcndemini de\u011ferlendiriyor.","date":"01\/03\/2018"}}


# GET /api/clubs

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {}

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 59
            Transfer-Encoding: chunked

    + Body

            {"successful":true,"code":200,"response":[{"id":1,"name":"Oyun Geli\u015ftirme Tak\u0131m\u0131","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/5763_583744_1_616195_84358.jpg","description":"\u00dclkemizde emekleme a\u015famas\u0131nda olan oyun sekt\u00f6r\u00fc, bu t\u00fcr organizasyonlar sayesinde ilerlemeye ve b\u00fcy\u00fcmeye devam ediyor. Bizim i\u00e7in bu \u00e7abalar \u00e7ok \u00f6nemli \u00e7\u00fcnk\u00fc T\u00fcrkiye'de halen lisans d\u00fczeyinde 'Bilgisayar Oyunlar\u0131' e\u011fitimi ilk ve tek olarak \u0130zmir Ekonomi \u00dcniversitesi'nde veriliyor."},{"id":2,"name":"Yaz\u0131l\u0131m Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/1721_246089_2_616173_84523.jpg","description":"\u0130zmir Ekonomi \u00dcniversitesi Yaz\u0131l\u0131m Kul\u00fcb\u00fc yani IEU Software Community (ISC), 2017 y\u0131l\u0131 g\u00fcz d\u00f6neminde faaliyete ge\u00e7mi\u015f bir \u00f6\u011frenci toplulu\u011fudur. 2017-2018 \u00f6\u011fretim y\u0131l\u0131nda ba\u015fta M\u00fchendislik \u00f6\u011frencileri olmak \u00fczere okulumuzun t\u00fcm b\u00f6l\u00fcmlerinden aktif \u00fcyelerimiz bulunmaktad\u0131r. Toplulu\u011fumuzun amac\u0131 bili\u015fim sekt\u00f6r\u00fcne merakl\u0131 ve kendini bu alanda geli\u015ftirmek istenen \u00f6\u011frencilerle birlikte, yaz\u0131l\u0131m d\u00fcnyas\u0131na daha donan\u0131ml\u0131 insanlar yeti\u015ftirmeye yard\u0131mc\u0131 olurken sosyal etkinlikleri de g\u00f6z ard\u0131 etmemek.\r\n\r\n\u00dcniversite \u00f6\u011frencilerini i\u015f hayat\u0131na haz\u0131rlamaya ba\u015flay\u0131p, okul sonras\u0131 hayatlar\u0131nda ba\u011flant\u0131lar\u0131n\u0131 kurmalar\u0131nda yard\u0131mc\u0131 olmay\u0131 umuyoruz. Yapt\u0131\u011f\u0131m\u0131z e\u011fitim, seminer ve workshoplar sayesinde g\u00fcn\u00fcm\u00fcz d\u00fcnyas\u0131nda aktif rol oynayan yaz\u0131l\u0131m\u0131 herkesin hayat\u0131n\u0131n bir par\u00e7as\u0131 haline getirmek istiyoruz. Bunun yan\u0131nda sosyal sorumluluk projelerini de bir arada y\u00fcr\u00fct\u00fcyoruz.\r\n\r\nDetayl\u0131 bilgi: ieusoftwarecommunity.com"},{"id":3,"name":"Motor Sporlar\u0131 Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/6119_905464_3_614097_65173.jpg","description":"Motor sporlar\u0131na g\u00f6n\u00fcl veren, r\u00fczgar\u0131 her daim y\u00fcz\u00fcnde hissetmeyi sevenlerin kurdu\u011fu bu kul\u00fcp \u0130zmir Ekonomi \u00dcniversitesi merakl\u0131lar\u0131n\u0131 motor sporlar\u0131 hakk\u0131nda bilgilendirme misyonu ta\u015f\u0131yor. D\u00fczenlenmekte olan her \u00e7e\u015fit motor sporlar\u0131 organizasyonunda aktif veya pasif olarak yer almay\u0131 ve konu hakk\u0131nda uzman ki\u015fileri kampusa davet ederek seminerler d\u00fczenlemeyi hedefleyen kul\u00fcp motor sporlar\u0131 tutkunlar\u0131n\u0131n bulu\u015fma noktas\u0131."},{"id":4,"name":"Almanca Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/7398_171642_4_614196_73636.jpg","description":"Kul\u00fcb\u00fcn amac\u0131, Almancan\u0131n yayg\u0131nla\u015ft\u0131r\u0131lmas\u0131 ve Almanca bilen ki\u015filerin meslek\u00ee, sosyal, ki\u015fisel, sanatsal ve k\u00fclt\u00fcrel geli\u015fimini sa\u011flayarak gerek meslek\u00ee gerekse sosyal ya\u015famda ki\u015filer aras\u0131nda almanca kullan\u0131m\u0131 sa\u011flamak, ki\u015filer aras\u0131ndaki ileti\u015fimi geli\u015ftirmek, Ekonomi \u00dcniversitesi \u00f6\u011frencileri almanca e\u011fitimi alm\u0131\u015f bulunan ki\u015filer ve \u00f6\u011frenciler aras\u0131nda Almancan\u0131n do\u011fru, d\u00fczg\u00fcn ve bilin\u00e7li kullan\u0131m\u0131 sa\u011flamakt\u0131r.\r\n\r\nEkonomi \u00dcniversitesi Almanca Kul\u00fcb\u00fc, kul\u00fcp y\u00f6netmeli\u011fi uyar\u0131nca kul\u00fcp dan\u0131\u015fman\u0131n\u0131n denetiminde \u00e7al\u0131\u015fmak \u00fczere kurulmu\u015ftur."},{"id":5,"name":"Alt K\u00fclt\u00fcr Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/7597_978178_5_614229_55202.jpg","description":"Ekonomi \u00dcniversitesi Alt K\u00fclt\u00fcr Toplulu\u011fu olarak amac\u0131m\u0131z bilim kurgu, fantastik d\u00fcnya, \u00e7izgi roman, anime, oyun ve fantasy role play(fantastik rol yapma oyunu)  gibi ve benzeri alt k\u00fclt\u00fcr hobileriyle ilgilenen insanlar\u0131n bir araya gelmesini sa\u011flamak ve ortak hobilere sahip insanlar\u0131 birbiriyle tan\u0131\u015ft\u0131rmak, e\u011flenmek ama\u00e7l\u0131 etkinliklerle toplulu\u011fun aktifli\u011fini sa\u011flamakt\u0131r."},{"id":6,"name":"Atat\u00fcrk\u00e7\u00fc D\u00fc\u015f\u00fcnce Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/8163_910293_6_614248_92093.jpg","description":"\u0130zmir Ekonomi \u00dcniversitesi  Atat\u00fcrk\u00e7\u00fc D\u00fc\u015f\u00fcnce Kul\u00fcb\u00fc, Mustafa Kemal  Atat\u00fcrk\u2019\u00fcm\u00fcz\u00fcn  kurdu\u011fu  ve  Cumhuriyetimizin temel   ilkelerine ba\u011fl\u0131  ve bu ilkeler do\u011frultusunda  \u00dcniversitemizde toplumsal  ve ulusal  olaylara kar\u015f\u0131 hassasiyeti artt\u0131r\u0131p, gen\u00e7 arkada\u015flar\u0131m\u0131z\u0131n  bu konulara ilgisini  artt\u0131rmay\u0131 ama\u00e7lamaktad\u0131r."},{"id":7,"name":"Cut Paper Tasar\u0131m Kul\u00fcb\u00fc","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAfElEQVRoge3YsQ2AMAwAQWASRmQURswojAAFUg70V7vwy53XY4zl47bZC7ygBkMNhhoMNRhqMNRgqMFQg6EGQw2GGgw1GP7QsD6cm\/XSPPf9duYPd6jBUIOhBkMNhhoMNRhqMNRgqMFQg6EGQw2GGgw1GGow1GCowVCD4QIHEAWxUonyFQAAAABJRU5ErkJggg==","description":"Kul\u00fcb\u00fcn amac\u0131 \u0130zmir Ekonomi \u00dcniversitesi kamp\u00fcslerinin mevcut durumlar\u0131 ve gelecekteki durumlar\u0131 \u00fczerine incelemeler ve \u00e7al\u0131\u015fmalar yapmak; eksikliklerin ve ihtiya\u00e7lar\u0131n tespitini yap\u0131p giderilmesi \u00fczerine faaliyete ge\u00e7mek; ger\u00e7ekle\u015ftirilmesi planlanan projeler ve uygulamalar \u00fczerine ara\u015ft\u0131rmalar s\u00f6yle\u015filer vb. etkinlikler d\u00fczenleme yoluyla fikir ve \u00f6neriler geli\u015ftirmek; bu s\u00fcre\u00e7lere \u00f6\u011frencileri de dahil etmektir. Kul\u00fcb\u00fcn \u00e7al\u0131\u015fma alanlar\u0131, tasar\u0131m kavram\u0131n\u0131n etkin oldu\u011fu alanlar\u0131n t\u00fcm\u00fcn\u00fc kapsamaktad\u0131r."},{"id":8,"name":"\u00c7eviri Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/4929_675918_8_614273_68364.jpg","description":"Ba\u015fta \u0130ngilizce olmak \u00fczere yabanc\u0131 dillere olan ilgiyi peki\u015ftirici payla\u015f\u0131mlarda bulunmay\u0131,\r\nT\u00fcrkiye'deki yabanc\u0131 dil b\u00f6l\u00fcmlerine kaynak olacak \u00e7al\u0131\u015fmalarda bulunmay\u0131,\r\nYabanc\u0131 dildeki kitaplar ile dilimizdekiler aras\u0131nda senkronizasyon sa\u011flamay\u0131,\r\nM\u00fctercimlik ba\u015fta olmak \u00fczere \u00e7eviri ve dil \u00fczerine de\u011fi\u015fik meslek gruplar\u0131n\u0131 bir araya toplamay\u0131,\r\nDil ve Terc\u00fcme alanlar\u0131nda detayl\u0131 ara\u015ft\u0131rmalar yaparak bireysel ve \u00f6zg\u00fcr \u00e7eviri kavramlar\u0131n\u0131 benimsetmeyi,\r\nNitelikli \u00e7evirmenlerin yeti\u015ftirilmesinde izlenmesi gereken yollar\u0131 ortaya koymay\u0131,\r\nGenel bir \u00f6z ge\u00e7mi\u015f havuzu olu\u015fturarak terc\u00fcman ile b\u00fcro ya da terc\u00fcman ile m\u00fc\u015fteri aras\u0131nda ba\u011f kurmay\u0131,\r\n\u00f6\u011frenci arkada\u015flar\u0131n kendilerini geli\u015ftirmelerine, hem dil ve terc\u00fcme hem de bili\u015fim teknolojilerini i\u015flerinde kullanma becerisi kazand\u0131rmay\u0131 ama\u00e7lamaktay\u0131z."},{"id":9,"name":"\u00c7evre,Ekonomi ve Teknoloji Kul\u00fcb\u00fc","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAj0lEQVRoge3YwQ2AIAxAUTEO5giOxEiO4GiOYA8Fvs1\/58bwDYeGtsX08wpO5urP\/TmzTzjHaDYw2MBgA4MNDDYw2CBJkqQ\/aME53\/nGsoHBBgYbGGxgsIGhQkNbtcwlOuKjkRUy\/kcSv1bhLtnAYAODDQw2MNjAUGHny3+rnL8aVrhLNjDYwGADgw0MNjC8kV4UiCQh6RwAAAAASUVORK5CYII=","description":null},{"id":10,"name":"\u00c7in K\u00fclt\u00fcr\u00fc Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/3495_406860_10_614314_96979.jpg","description":"\u00c7in K\u00fclt\u00fcr Kul\u00fcb\u00fc, d\u00fcnyan\u0131n b\u00fcy\u00fcyen g\u00fcc\u00fc haline gelen \u00c7in hakk\u0131nda daha fazla bilgi sahibi olmak isteyen, \u00c7in geleneklerine ve k\u00fclt\u00fcr\u00fcne merakl\u0131 olan okulumuzdaki ve di\u011fer \u00fcniversitelerdeki \u00f6\u011frencilerin bir araya gelebilece\u011fi bir platform olu\u015fturmay\u0131 ama\u00e7lar."},{"id":11,"name":"Da\u011fc\u0131l\u0131k ve Trekking Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/6628_197950_11_614330_17995.jpg","description":"Da\u011fc\u0131l\u0131k sporunu bilimsel bir \u00e7er\u00e7eve i\u00e7erisinde ve amat\u00f6r bir yakla\u015f\u0131mla alg\u0131lay\u0131p, bu sporla ilgilenmek isteyen t\u00fcm Ekonomi \u00dcniversitesi \u00f6\u011frencilerine tamamen \u00fccretsiz olarak da\u011fc\u0131l\u0131k sporunu \u00f6\u011frenme ve yapma olana\u011f\u0131 sa\u011flamay\u0131 ama\u00e7lar.\r\n\r\nE\u011fitim program\u0131m\u0131z temelde iki b\u00f6l\u00fcmden olu\u015fuyor; teorik e\u011fitimler ve pratik e\u011fitimler. Teorik e\u011fitimlerde; y\u00fcr\u00fcy\u00fc\u015f bilgisinden temel yaz ve k\u0131\u015f kamp\u00e7\u0131l\u0131\u011f\u0131na, ilk yard\u0131mdan do\u011fada arama-kurtarmaya, \u00e7\u0131\u011f bilgisinden meteorolojiye, k\u0131\u015f teknik e\u011fitiminden kaya t\u0131rman\u0131\u015f\u0131na kadar bir\u00e7ok de\u011fi\u015fik alanda teorik bilgiler sporcu adaylar\u0131na aktar\u0131l\u0131r. Pratik e\u011fitimler ise, teorikte \u00f6\u011frenilen bilgilerin \u00fclkemizin bir\u00e7ok de\u011fi\u015fik b\u00f6lgesindeki da\u011flarda uygulanmas\u0131yla ge\u00e7ekle\u015fir."},{"id":12,"name":"D\u0131\u015f Ticaret Kul\u00fcb\u00fc","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAnElEQVRoge3YwQmAMBQEUSMWYomWYomWYgv\/sOCwzjuHhDE5fFxb1vVMV95n6sw9tdGHbGCwgcEGBhsYbGBoaFjThfNhLmswGjbcgw0MNjDYwGADgw0MDQ3rs2Eu58hvOfkJGf1wDW\/JBgYbGGxgsIHBBoa\/zXzZYS63W8NbsoHBBgYbGGxgsIGhYeZruAcbGGxgsIHBBgYbGBoaXkHXDnL\/c9EpAAAAAElFTkSuQmCC","description":"\u0130zmir Ekonomi \u00dcniversitesi'ne ve D\u0131\u015f Ticaret B\u00f6l\u00fcm\u00fcne aidiyet duygusunu artt\u0131rmak ve D\u0131\u015f Ticaret konulu seminer, sempozyum, e\u011fitim ve sosyal sorumluluk etkinlikleri ger\u00e7ekle\u015ftirmeyi hedeflemektedir."},{"id":13,"name":"Ebru Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/8867_312417_13_614360_38528.jpg","description":"Geleneksel T\u00fcrk el sanatlar\u0131ndan biri olan Ebru Sanat\u0131n\u0131 okul \u00e7ap\u0131nda tan\u0131t\u0131p \u00f6\u011frencilerimize sevdirmek gayesiyle kurulmu\u015ftur. \u00d6\u011frencilerimizin yarat\u0131c\u0131l\u0131klar\u0131n\u0131n geli\u015fmesini destekleyerek el becerisini en \u00fcst d\u00fczeye \u00e7\u0131kar\u0131p d\u00fczenlenen sergilerle de bireyin \u00f6zg\u00fcvenine katk\u0131da bulunmak ama\u00e7lar\u0131 aras\u0131nda yer almaktad\u0131r."},{"id":14,"name":"ECOGENES\u0130S (Genetik ve Biyom\u00fchendislik Kul\u00fcb\u00fc)","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/8846_127151_14_614387_77451.jpg","description":"Okulumuzda ba\u015fta Genetik ve Biyom\u00fchendislik, Biyomedikal ve T\u0131p alanlar\u0131nda \u00f6\u011frenim g\u00f6ren yada biyoloji bilimine ilgi duyan herkesin bilgi ve tecr\u00fcbesini artt\u0131rmay\u0131 hedefleyerek ba\u015flad\u0131\u011f\u0131m\u0131z bu yolda gerek konferanslar gerekse de teknik gezilerle her kul\u00fcp \u00fcyesinin geli\u015fimine en \u00fcst seviyede katk\u0131 sa\u011flamay\u0131 ama\u00e7lamaktay\u0131z. Ayr\u0131ca giri\u015fimcilik konusunda verdi\u011fimiz \u00f6nem ile de \u00f6ncelikli olarak okulumuz b\u00fcnyesinde projeler geli\u015ftirerek \u00fclkemizdeki biyolojik alanlarda giri\u015fim say\u0131s\u0131n\u0131 artt\u0131rmay\u0131 ve giri\u015fim ruhunu a\u015f\u0131lamaya \u00e7al\u0131\u015fmaktay\u0131z. \r\n\r\n Y\u0131l boyunca yapaca\u011f\u0131m\u0131z etkinlikler sayesinde hem yeni \u015feyler \u00f6\u011frenecek hem de koskocaman bir ailenin par\u00e7as\u0131 olacaks\u0131n. Bu ailenin bir par\u00e7as\u0131 olmak istersen bizimle ileti\u015fime ge\u00e7men yeterli; \r\n\r\necogenesisiue.gmail.com"},{"id":15,"name":"Eko-IEEE Kul\u00fcb\u00fc (Institute of Electrical and Electronics Engineers)","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/5899_931813_15_614419_76810.jpg","description":"Elektrik elektronik m\u00fchendislerinin, teknik \u00f6\u011fretmenlerin, tekniker ve teknisyenlerin ve bu dallarda e\u011fitim g\u00f6ren \u00f6\u011frencilerin e\u011fitim ve i\u015f ya\u015famlar\u0131n\u0131n yan\u0131s\u0131ra sosyal ya\u015famlar\u0131nda da ileti\u015fim i\u00e7inde olmalar\u0131n\u0131 sa\u011flar ve bu ama\u00e7la meslekte g\u00fcncel konular\u0131 ve hedef kitlesinden gelen  talepleri esas alarak  bilgi ve becerilerin  artt\u0131r\u0131ld\u0131\u011f\u0131,  e\u011fitim, seminer ve teknik geziler d\u00fczenler."},{"id":16,"name":"Ekonomi 1903-Be\u015fikta\u015f Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/9368_100182_16_614443_57232.jpg","description":"Be\u015fikta\u015f Spor Kul\u00fcb\u00fc taraftar\u0131 \u00f6\u011frencilerimiz taraf\u0131ndan kurulan kul\u00fcp, Be\u015fikta\u015f Spor Kul\u00fcb\u00fc\u2019n\u00fcn \u00e7e\u015fitli bran\u015flardaki kar\u015f\u0131la\u015fmalar\u0131na taraftar deste\u011fi sa\u011flamaktad\u0131r."},{"id":17,"name":"Ekonomi Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/7370_528376_17_614460_75285.jpg","description":"\u0130zmir Ekonomi \u00dcniversitesi Ekonomi Kul\u00fcb\u00fc, k\u00fcreselle\u015fen d\u00fcnyada, ekonomideki de\u011fim ve geli\u015fimleri yak\u0131ndan takip ederek \u00f6rencilere en iyi \u015fekilde aktarmay\u0131 hedeflemektedir. Bu hedefler do\u011frultusunda; konferanslar, a\u00e7\u0131k oturumlar, s\u00f6yle\u015filer ve paneller d\u00fczenlemek kul\u00fcb\u00fcn en temel ama\u00e7lar\u0131ndand\u0131r. \u00d6rencilerin, yeniliklere a\u00e7\u0131k, analitik d\u00fc\u015f\u00fcnebilen ve olaylar\u0131 farkl\u0131 boyutlar\u0131yla ele alabilen, geli\u015fmi\u015f bireylere d\u00f6n\u00fc\u015fmelerine yard\u0131mc\u0131 olmay\u0131 ama\u00e7 edinmi\u015ftir."},{"id":18,"name":"Ekonomili Aslanlar","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/3232_995967_18_614478_92211.jpg","description":"\u0130zmir Ekonomi \u00dcniversitesi'ndeki Galatasarayl\u0131lar\u0131 bir araya getiren topluluk."},{"id":19,"name":"End\u00fcstri Sistemleri Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/6640_688934_19_614495_73255.jpg","description":"End\u00fcstri Sistemleri Toplulu\u011fu, \u00fcyeleri ile birlikte ger\u00e7ekle\u015ftirdi\u011fi \u00e7al\u0131\u015fmalarla End\u00fcstri Sistemleri M\u00fchendisli\u011fi B\u00f6l\u00fcm\u00fc\u2019n\u00fcn tan\u0131t\u0131m\u0131 ve geli\u015ftirilmesine katk\u0131da bulunmay\u0131, d\u00fczenledi\u011fi seminer, konferans, teknik gezi ve sosyal aktivitelerle \u00fcyelerinin akademik ve sosyal birikimlerini art\u0131rmay\u0131, end\u00fcstri sistemleri m\u00fchendisli\u011fi \u00f6\u011frencileri, \u00f6\u011fretim \u00fcyeleri ve mezunlar\u0131n\u0131 bir araya getirmeyi ama\u00e7 edinmi\u015ftir. \r\n\r\nEnd\u00fcstri Sistemleri Toplulu\u011fu, ulusal ve uluslararas\u0131 End\u00fcstri M\u00fchendisli\u011fi ile ilgili organizasyon ve konferanslar\u0131 takip ederek kul\u00fcp \u00fcyelerini bilgilendirmektedir."},{"id":20,"name":"ESN \u0130UE (Erasmus Student Network) Club","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/3395_179314_20_614517_65676.jpg","description":"Erasmus Student Network (ESN) Avrupadaki en b\u00fcy\u00fck interdisipliner \u00f6\u011frenci olu\u015fumlar\u0131ndan birisidir. Bu network\u2019\u00fcn bir par\u00e7as\u0131 olan ESN IUE, A\u011fustos 2014\u2019te kurulmu\u015f olup 2015\u2019te toplanan Uluslararas\u0131 Platformda \u0130zmir\u2019in resmi \u015fubelerinden birisi haline gelmi\u015ftir ESN IUE, di\u011fer ESN olu\u015fumlar\u0131 gibi kar amac\u0131 g\u00fctmeyen bir organizasyondur. \r\n\r\n\u015eubenin temel amac\u0131; Erasmus\u2019a giden \u0130zmir Ekonomi \u00dcniversitesi b\u00fcnyesindeki \u00f6\u011frencilere ve Erasmus de\u011fi\u015fim program\u0131na \u0130zmir Ekonomi \u00dcniversitesinde kat\u0131lmay\u0131 se\u00e7en, \u00fclke d\u0131\u015f\u0131ndan gelen Erasmus \u00f6\u011frencilerine yard\u0131mc\u0131 olmakt\u0131r. \u00d6\u011frencilere adaptasyon s\u00fcre\u00e7leri, kalacak yer sa\u011flanmas\u0131 gibi \u00e7e\u015fitli konularda aktif yard\u0131m sa\u011flamakta olup, \u00e7e\u015fitli etkinlikler ve Sosyal Erasmus projeleri d\u00fczenleyerek \u00e7e\u015fitli alanlarda faaliyet g\u00f6stermektedir."},{"id":21,"name":"E\u015fit Platform Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/8409_574594_21_614533_38384.jpg","description":"E\u015fit Platform Kul\u00fcb\u00fc, teoride savunulan e\u015fitli\u011fi g\u00fcnl\u00fck hayata yans\u0131tmak, her t\u00fcrl\u00fc din, dil, \u0131rk, ya\u015f, sosyoekonomik stat\u00fc,cinsiyet,etnik,cinsel tabanl\u0131 ayr\u0131mc\u0131l\u0131k ve mizojini kar\u015f\u0131t\u0131 olan feminist ve kuir (queer) yakla\u015f\u0131mlar\u0131 i\u00e7selle\u015ftirmi\u015f bir kul\u00fcpt\u00fcr. Bu konularla ilgili olarak, film ve belgesel g\u00f6sterimleri, konferans,semineri at\u00f6lyeler, kuir piknik ve sportif faaliyetler, ayr\u0131mc\u0131l\u0131k kar\u015f\u0131t\u0131 g\u00fcnler, kamp\u00fcs eylemleri d\u00fczenler."},{"id":22,"name":"Factory of Bussiness Club","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/5157_111474_22_614551_69843.jpg","description":"Factory of Business (F.O.B.) \u00f6\u011frenci toplulu\u011fu \u0130zmir Ekonomi \u00dcniversitesi (\u0130E\u00dc) b\u00fcnyesinde 2016 y\u0131l\u0131n\u0131n bahar d\u00f6neminde resmi olarak faaliyete ge\u00e7mi\u015f bir \u00f6\u011frenci toplulu\u011fudur. \r\nAmac\u0131m\u0131z, okulumuzun \u00f6\u011frencilerini i\u015f d\u00fcnyas\u0131n\u0131n g\u00fc\u00e7l\u00fc isimleriyle bir araya getirmek ve bu isimlerin tecr\u00fcbeleriyle \u00f6\u011frencilerimizi i\u015f d\u00fcnyas\u0131 i\u00e7in daha donan\u0131ml\u0131 k\u0131lmak. Bunun i\u00e7in elimizden geldi\u011fi kadar konferans, teknik geziler ve sertifika programlar\u0131 gibi etkinlikler d\u00fczenliyoruz. Bunlar\u0131n yan\u0131s\u0131ra bir birinden e\u011flenceli etkinlikler d\u00fczenleyerek F.O.B ailesi olarak \u00fcniversite y\u0131llar\u0131m\u0131zda unutamayaca\u011f\u0131m\u0131z \u00e7ok de\u011ferli zamanlar ge\u00e7irece\u011fiz. Ayr\u0131ca b\u00fct\u00fcn \u00f6\u011frenci topluluklar\u0131n\u0131n yapmas\u0131 gerekti\u011fine inand\u0131\u011f\u0131m\u0131z sosyal projeler d\u00fczenleyerek, kitap,k\u0131rtasiye \u00fcr\u00fcnleri,k\u0131yafet,spor e\u015fyalar\u0131 gibi eksikleri olan okullara elimizden geldi\u011fi kadar yard\u0131mda bulunuyoruz."},{"id":23,"name":"Foto\u011fraf Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/4597_840519_23_614567_92589.jpg","description":"\u0130zmir Ekonomi \u00dcniversitesi Foto\u011fraf Kul\u00fcb\u00fc, foto\u011fraf sanat\u0131na g\u00f6n\u00fcl vermi\u015f \u0130E\u00dc \u00f6\u011frenci ve mezunlar\u0131n\u0131n bir araya geldikleri topluluktur."},{"id":24,"name":"Frans\u0131zca Kul\u00fcb\u00fc","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAkUlEQVRoge3YsQ2AMBAEQR5RCCVSCiVSCh2gCwwsz07swCs5OLn2dQptR3pylPBu8723eIQNDDYw2MBgA4MNDB0aKt98WBWeeys1Gcsd3pINDDYw2MBgA4MNDB0235IfTebX2J9P\/yo\/xQYGGxhsYLCBoUPDzzZfaOw0THR4SzYw2MBgA4MNDDYwdGiQJEnStROanA0qp7OGTwAAAABJRU5ErkJggg==","description":"Frans\u0131zca Kul\u00fcb\u00fc \u00f6\u011frencilerin yabanc\u0131 dillerini geli\u015ftirmek, Frans\u0131zca'ya olan ilgiyi te\u015fvik etmek, ilgilerinin s\u00fcreklili\u011fini sa\u011flamak, yurti\u00e7i ve yurtd\u0131\u015f\u0131 aktivitelerde okul ile i\u015fbirli\u011finde bulunarak \u00f6\u011frencilere imkanlar yaratabilmek amac\u0131yla kurulmu\u015ftur."},{"id":25,"name":"Futbol Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/6384_594000_25_614595_73609.jpg","description":"\u0130zmir Ekonomi \u00dcniversitesi Futbol Kul\u00fcb\u00fc'nde, futbola g\u00f6n\u00fcl veren herkesi bir araya getirerek futbola hizmet ediyoruz. Futbolun sadece bir spor dal\u0131 de\u011fil ayn\u0131 zamanda bir ya\u015fam tarz\u0131 oldu\u011funu d\u00fc\u015f\u00fcn\u00fcyorsan bize kat\u0131l ve futbol me\u015falesini \u0130zmir Ekonomi \u00dcniversitesi'nde ta\u015f\u0131mam\u0131za yard\u0131mc\u0131 ol."},{"id":26,"name":"G\u0131da M\u00fchendisli\u011fi Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/6787_327846_26_614611_54519.jpg","description":"G\u0131da m\u00fchendisli\u011fi, bilimsel bilgiler ve m\u00fchendislik bilgileri yard\u0131m\u0131yla g\u0131dalar\u0131n g\u00fcvenilir bir \u015fekilde \u00fcretimini, haz\u0131rlanmas\u0131n\u0131, i\u015flenmesini, paketlenmesini, da\u011f\u0131t\u0131lmas\u0131n\u0131 sa\u011flayan ve g\u0131dalardan uygun bir \u015fekilde yararlanmay\u0131 sa\u011flayan m\u00fchendislik dal\u0131d\u0131r.\r\n\r\nTemel amac\u0131 insanlar\u0131n sa\u011fl\u0131kl\u0131 beslenmesidir."},{"id":27,"name":"Halk Danslar\u0131 Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/2827_998403_27_614630_29887.jpg","description":"\u0130EU Halk Danslar\u0131 Kul\u00fcb\u00fc, T\u00fcrkiye'de odalar taraf\u0131ndan kurulan ilk vak\u0131f \u00fcniversitesi olma \u00f6zelli\u011fi ta\u015f\u0131yan \u0130zmir Ekonomi \u00dcniversitesi'nin e\u011fitim ve \u00f6\u011fretime ge\u00e7ti\u011fi 2001 \u2013 2002 Akademik Y\u0131l\u0131'nda kurulmu\u015ftur. Kul\u00fcp ilk g\u00f6sterisini IEU 1. Bahar \u015eenli\u011fi'nde Zeybek Ekibi ile ger\u00e7ekle\u015ftirdi. Daha sonra kul\u00fcp, \u0130zmir Ekonomi \u00dcniversitesi'nin b\u00fct\u00fcn Akademik Y\u0131l A\u00e7\u0131l\u0131\u015f, Kurulu\u015f Y\u0131ld\u00f6n\u00fcm\u00fc T\u00f6renlerinde, Bahar \u015eenliklerinde ve nitelikli organizasyonlarda en \u00f6n s\u0131rada yerini alm\u0131\u015f, bir\u00e7ok devlet ba\u015fkan\u0131, bakan vb. gibi organizasyondaki \u00fcst d\u00fczey protokole k\u00fclt\u00fcrel simgelerin vurguland\u0131\u011f\u0131 hediyeler takdim etmi\u015ftir. Her sene misafir etti\u011fi ekiplerle birlikte halk danslar\u0131 geceleri d\u00fczenleyerek k\u00fclt\u00fcr miras\u0131n\u0131 her yeni nesle aktarmaya \u00e7al\u0131\u015fmaktad\u0131r.\r\n\r\nOrta Asya'dan Anadolu'ya ge\u00e7en T\u00fcrkler, y\u00fczy\u0131llar i\u00e7erisinde farkl\u0131 k\u0131talarla bulu\u015fmalar\u0131 s\u0131ras\u0131nda de\u011fi\u015fkenlik g\u00f6steren farkl\u0131 danslar\u0131 benimsemi\u015flerdir. IEU Halk Danslar\u0131 Kul\u00fcb\u00fc de, bu farkl\u0131 danslar\u0131n g\u00f6sterilerini b\u00fcnyesinde toplama ba\u015far\u0131s\u0131n\u0131 g\u00f6stermi\u015ftir. Ege, Karadeniz, Trakya, \u0130\u00e7 Anadolu, G\u00fcney Do\u011fu Anadolu y\u00f6relerinden farkl\u0131 g\u00f6steriler olu\u015fturulmas\u0131 sa\u011flanm\u0131\u015ft\u0131r. Ekonominin Sultanlar\u0131(Halay) g\u00f6sterisinde y\u00f6relerin modernize b\u00fct\u00fcnle\u015fmesi ile farkl\u0131 bir g\u00f6rsellik sa\u011flanm\u0131\u015ft\u0131r.\r\n\r\nSergilenen Danslar: Zeybek, Kafkas, Halay, \u00dcsk\u00fcp, Roman, K\u00f6\u00e7ek\u00e7e, Karadeniz, Anamur, Fethiye, Bendir, Semazen"},{"id":28,"name":"Havac\u0131l\u0131k Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/7598_446789_28_614644_40074.jpg","description":"Havac\u0131l\u0131k Kul\u00fcb\u00fc olarak \u00fcniversitemizde hava sporlar\u0131 faaliyetlerini etkin k\u0131lmak amac\u0131yla kurulan \"Ekohavk\",\u00fcniversite b\u00fcnyesinde havac\u0131l\u0131kla ilgilenen herkesi tek \u00e7at\u0131 alt\u0131nda toplay\u0131p bu alanda e\u011fitim ve etkinlikler d\u00fczenlemektedir.Bu konuda havac\u0131l\u0131k alan\u0131nda faaliyet g\u00f6steren profesyonel bilgi birikimine ve uygulamalar\u0131na sahip olan ulusal ve uluslararas\u0131 ge\u00e7erlili\u011fe sahip kurulu\u015flardan e\u011fitim alan\u0131nda i\u015fbirli\u011fi yapmay\u0131 ama\u00e7lamaktay\u0131z."},{"id":29,"name":"Hayvanseverler Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/4022_276942_29_614660_23529.jpg","description":"D\u00fcnyan\u0131n bir k\u00f6\u015fesinde bir ku\u015f kalbinin atmaya devam etmesi, e\u011fer bizim elimizde ise, \u0130E\u00dc Hayvanseverler Kul\u00fcb\u00fc o kalbin durmamas\u0131 i\u00e7in her fedakarl\u0131\u011f\u0131 yapar."},{"id":30,"name":"Hukuk Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/6016_952736_30_614682_45464.jpg","description":"2012 y\u0131l\u0131nda \u0130zmir Ekonomi \u00dcniversitesi Hukuk Fak\u00fcltesinin ilk \u00f6\u011frencileri olarak kurulan kul\u00fcb\u00fcm\u00fcz , fak\u00fclte \u00f6\u011frencilerimizin hukuk vizyon ve misyonunu geli\u015ftirerek hem hukuk bilgilerini hem de bu bilgilerin hayat\u0131n ola\u011fan ak\u0131\u015f\u0131na do\u011fru bir \u015fekilde uygulanmas\u0131 bak\u0131m\u0131ndan yard\u0131mc\u0131 olabilmektir. Faaliyetlerimizi ve ama\u00e7lar\u0131m\u0131z\u0131 hayata ge\u00e7irirken hi\u00e7 bir kurum , kurulu\u015f veya siyasi partinin d\u00fc\u015f\u00fcncelerine de\u011fil sadece Hukuk Devletinin sahip oldu\u011fu demokratik ilkerine ve Atat\u00fcrk \u0130lke ve \u0130nk\u0131laplar\u0131na ba\u011fl\u0131 olarak ger\u00e7ekle\u015ftirerek \u00dcniversitemizin sayg\u0131nl\u0131\u011f\u0131na de\u011fer katmak temel amac\u0131m\u0131zd\u0131r."},{"id":31,"name":"\u0130E\u00dc Rotaract Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/4466_348841_31_614709_17424.jpg","description":"Toplumdaki f\u0131rsat e\u015fitsizli\u011finin ortadan kalkmas\u0131n\u0131 hedef alan projelerin yan\u0131 s\u0131ra, \u00fcye ve misafirlerin mesleki ve ki\u015fisel geli\u015fimine katk\u0131 sa\u011flayacak e\u011fitimler ve uygulamalar ger\u00e7ekle\u015ftirmektedir."},{"id":32,"name":"\u0130novasyon ve Giri\u015fimcilik Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/7235_838012_32_614725_90030.jpg","description":"\u0130novasyon ve Giri\u015fimcilik Kul\u00fcb\u00fc, faaliyetlerinin her a\u015famas\u0131nda \u00f6\u011frencilerin g\u00f6rev ald\u0131\u011f\u0131, kar amac\u0131 g\u00fctmeyen bir topluluktur. \u0130novasyon ve Giri\u015fimcilik Kul\u00fcb\u00fc \u00fcniversite \u00f6\u011frencilerinin sosyal, k\u00fclt\u00fcrel ve akademik ya\u015fant\u0131lar\u0131nda birtak\u0131m de\u011fi\u015fiklikler yaparak onlar\u0131n ki\u015fisel geli\u015fimlerine katk\u0131da bulunmay\u0131, i\u015f d\u00fcnyas\u0131 ve akademik \u00e7evreleri bir araya getirerek \u015fehrimizde ve \u00fcniversitemizde yeni ve yararl\u0131 bir s\u00fcre\u00e7 yaratmay\u0131 ama\u00e7lamaktad\u0131r. Kul\u00fcp gerek yap\u0131s\u0131yla, \u00e7al\u0131\u015fma d\u00fczeniyle, sahip oldu\u011fu de\u011ferleriyle, hedefleriyle her zaman \u00f6\u011frencilerin pusulas\u0131 olmay\u0131, \u00fcyelerimizin i\u015f d\u00fcnyas\u0131 ile ba\u011flant\u0131 halinde olmas\u0131n\u0131, mezun olmadan \u00f6nce alabilece\u011fi \u00e7al\u0131\u015fma disiplinini, her zaman hayal edilen \u00e7al\u0131\u015fma ortam\u0131n\u0131n ve sonsuza kadar s\u00fcrecek olan arkada\u015fl\u0131klar\u0131n bulunabilece\u011fi bir ortam\u0131 yarat\u0131r."},{"id":33,"name":"\u0130\u015fletme Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/9059_492097_33_614744_69000.jpg","description":"T\u00fcrkiye \u00e7ap\u0131nda sayg\u0131n bir yere sahip olan, giri\u015fimci ve yenilik\u00e7i \u00e7izgisini koruyarak \u0130ktisadi ve \u0130dari Bilimler Fak\u00fcltesi \u00f6\u011frencisinin par\u00e7as\u0131 olmaktan gurur duyaca\u011f\u0131, lider, \u00e7a\u011fda\u015f bir kul\u00fcp olarak kul\u00fcb\u00fcm\u00fcz\u00fcn \u00f6ncelikli misyonu, i\u015f d\u00fcnyas\u0131yla \u00fcniversitemizin s\u0131cak ili\u015fkiler kurmas\u0131n\u0131 sa\u011flamak ve bu do\u011frultuda Ekonomi \u00dcniversitesi\u2019ni i\u015f d\u00fcnyas\u0131na tan\u0131tmak, \u00f6\u011frencileri hen\u00fcz \u00fcniversite y\u0131llar\u0131nda \u00e7al\u0131\u015fma hayat\u0131yla kar\u015f\u0131la\u015ft\u0131rmakt\u0131r. \u0130\u015fletme b\u00f6l\u00fcm\u00fcn\u00fcn \u00f6\u011frencileri, kul\u00fcb\u00fcn do\u011fal \u00fcyesidir. Di\u011fer b\u00f6l\u00fcmlerdeki \u00f6\u011frenciler de kul\u00fcp \u00fcyesi olabilmekte ve aktivitelerde yer alabilmektedir."},{"id":34,"name":"K\u0131z\u0131lay Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/8458_225054_34_614764_92617.jpg","description":"\u0130E\u00dc K\u0131z\u0131lay Kul\u00fcb\u00fc olarak, T\u00fcrk K\u0131z\u0131lay\u0131n\u0131n temel ilkelerinin \u00fcniversitemiz \u00f6\u011frencileriyle birlikte uygulanmas\u0131n\u0131 sa\u011flayarak, gen\u00e7 k\u0131z\u0131layc\u0131lar\u0131n bu sosyal ortamda sorumluluklar\u0131n\u0131 bilerek, k\u00fclt\u00fcr ve de\u011ferlerini koruyarak; din, dil, mezhep ay\u0131rmadan t\u00fcm insanl\u0131k i\u00e7in beraber \u00e7al\u0131\u015faca\u011f\u0131 bir ortam yarat\u0131yoruz.\r\n\r\nBu ortamda sorumluluklar\u0131m\u0131z yan\u0131 s\u0131ra; \u00f6\u011frencilerin sosyalle\u015fmeleri, kendi \u00f6zg\u00fcvenini kazanmalar\u0131, Avrupa f\u0131rsatlar\u0131 konusunda bilgilendirme yaparak \u00f6\u011frencilere yurt d\u0131\u015f\u0131na \u00e7\u0131kmalar\u0131 i\u00e7in yol g\u00f6steriyoruz. Ayr\u0131ca kulub\u00fcm\u00fcz; gen\u00e7lerle ilgili yerel proje ve e\u011fitimler d\u00fczenleyerek, onlar\u0131n sosyal ya\u015famlar\u0131nda daha aktif olmalar\u0131n\u0131 sa\u011fl\u0131yor.\r\n\r\nUNUTMA SEN YOKSAN, B\u0130R EKS\u0130\u011e\u0130Z!"},{"id":35,"name":"Kitap Kul\u00fcb\u00fc","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAhUlEQVRoge3ZwQmAMBAAQRULsERLs0RLsATzEBzCzjvcsZDfredxLmOu+3p988u0bXClrAZDDYYaDDUYajDUYKjBUIOhBkMNhhoMNRhqMNRgqMFQg6EGQw2GGRrW8Wssa\/984rdn7BEz\/KUaDDUYajDUYKjBUIOhBkMNhhoMNRhqMMzQ8ABZYgsjRk3TOQAAAABJRU5ErkJggg==","description":null},{"id":36,"name":"Latin Dans Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/1506_928264_36_614791_44618.jpg","description":"2002 y\u0131l\u0131nda resmi olarak faaliyetlerine ba\u015flayan kul\u00fcp her ge\u00e7en g\u00fcn artan \u00fcye say\u0131s\u0131 ile \u0130zmir Ekonomi \u00dcniversitesi\u2019nin en aktif kul\u00fcplerinden birisidir. Her sene 100\u2019\u00fc a\u015fk\u0131n \u00fcyesi olan kul\u00fcpte \u015fimdiye kadar 1500 e yak\u0131n dans e\u011fitimi alan \u00f6\u011frenci say\u0131s\u0131 bulunmaktad\u0131r. E\u015fli salon danslar\u0131n\u0131 ve sosyal Latin danslar\u0131n\u0131 tan\u0131tmak ve sevdirmek i\u00e7in her d\u00f6nem \u00e7e\u015fitli faaliyetler d\u00fczenlemektedir. Yurt i\u00e7indeki organizasyonlarda \u0130zmir Ekonomi \u00dcniversitesi\u2019ni temsil etmek i\u00e7in kurulan ve dans bran\u015flar\u0131nda yeterli e\u011fitmen kadrosu ile ger\u00e7ekle\u015ftirilen e\u011fitimlerle olu\u015fturulan g\u00f6steri gruplar\u0131yla \u015fehir d\u0131\u015f\u0131ndaki \u00fcniversiteler dahil dans festivallerine kat\u0131larak Latin Danslar\u0131 Kul\u00fcb\u00fc\u2019n\u00fc en iyi \u015fekilde temsil etmektedir."},{"id":37,"name":"Lojistik Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/9772_313581_37_614809_14297.jpg","description":"\"Lojistik Kul\u00fcb\u00fc kuruldu\u011fu 2004-2005 e\u011fitim-\u00f6\u011fretim y\u0131l\u0131ndan bu yana bilimsel ve sosyal alanda bir \u00e7ok ba\u015far\u0131l\u0131 organizasyon ger\u00e7ekle\u015ftirmi\u015ftir. G\u00f6n\u00fcll\u00fck esas\u0131yla \u00e7al\u0131\u015fmalar\u0131n\u0131 s\u00fcrd\u00fcren Lojistik Kul\u00fcb\u00fc bir \u00f6\u011frenci toplulu\u011fudur. \u00dcyelerinin ki\u015fisel geli\u015fiminde tamamlay\u0131c\u0131 rol oynamas\u0131n\u0131n yan\u0131nda, 150'den fazla \u00fcyesi ve 9 ki\u015filik y\u00f6netim kuruluyla teorik bilgiyi prati\u011fe \u00e7evirmeyi ama\u00e7 edinerek bilimsel ve sosyal alanlarda ki projelerini tasarlamakta ve uygulamaktad\u0131r.\""},{"id":38,"name":"Matematik Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/9838_511693_38_614828_41023.jpg","description":"Matematik Kul\u00fcb\u00fc matematik\u00e7ilerin, matematik \u00f6\u011fretmenlerinin, matematikle ilgilenen herkesin fikirlerini duyurabilece\u011fi, g\u00f6r\u00fc\u015f ve \u00f6nerilerini payla\u015fabilece\u011fi bir platform olu\u015fturma \u00e7abas\u0131n\u0131n \u00fcr\u00fcn\u00fcd\u00fcr. Amac\u0131m\u0131z \u00e7\u0131kartt\u0131\u011f\u0131m\u0131z yay\u0131nlarla, d\u00fczenleyece\u011fimiz konferans ve seminerlerle ve internet \u00fczerinden ger\u00e7ekle\u015ftirece\u011fimiz etkinliklerle matemati\u011fi korkulan  de\u011fil, sevilen bir ders ve bir u\u011fra\u015f haline getirebilmektir.Ayr\u0131ca, genel olarak \u00f6\u011fretilen matematikle ya\u015famdaki matemati\u011fin birbirinden farkl\u0131 oldu\u011fu bilinmektedir. Bu \u00e7eli\u015fkinin azalmas\u0131na\/\u00e7\u00f6z\u00fclmesine katk\u0131 yapmak ve matemati\u011fi do\u011fru yol, y\u00f6ntem ve materyallerle zevkli bir u\u011fra\u015fa d\u00f6n\u00fc\u015ft\u00fcrmek temel amac\u0131m\u0131zd\u0131r."},{"id":39,"name":"Moda ve Stil Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/5985_791973_39_614867_92235.jpg","description":"\u00dcniversitemizde \u00f6zellikle \u00e7ok \u00f6nemli b\u00f6l\u00fcmlerin ba\u015f\u0131nda gelen Moda ve Tasar\u0131m b\u00f6l\u00fcm\u00fc \u00f6\u011frencileri yan\u0131 s\u0131ra moda ve tasar\u0131m konular\u0131yla ilgilenen t\u00fcm \u00f6\u011frencilerle \u00e7al\u0131\u015fma imkan\u0131 olu\u015fturarak, bu konularda \u00fcnlenmi\u015f \u00e7e\u015fitli konuklar\u0131 davet ederek bilgi ve deneyimleri payla\u015fmak amac\u0131yla kurulan \u00f6\u011frenci grubumuza sizleri de bekliyoruz."},{"id":40,"name":"Modern Dans Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/4208_981238_40_614882_34529.jpg","description":"Ekonomi \u00dcniversitesi \u00f6\u011frencilerine akademik bilgilerinin yan\u0131nda kendilerini rahat\u00e7a ifade edebilecekleri, geli\u015ftirebilecekleri, sanat\u0131n ve dans\u0131n farkl\u0131l\u0131\u011f\u0131n\u0131 fark\u0131ndal\u0131\u011f\u0131n\u0131 ya\u015fayabilecekleri, amat\u00f6r ruhlar\u0131n bir araya gelerek olu\u015fturdu\u011fu sosyal bir ortam sunmak amac\u0131yla kurulmu\u015ftur. \u00dcniversite \u00f6\u011frencilerinin temel modern dans e\u011fitimini alarak yarat\u0131c\u0131l\u0131klar\u0131n\u0131 bedensel hareketlerle ortaya koymalar\u0131na olanak sa\u011flamaktad\u0131r."},{"id":41,"name":"Mor Kollektif Kul\u00fcb\u00fc","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAkElEQVRoge3WsQ2AMAwAQYwYkBEYJaNkBEakYAEXhrysvzqK\/EoKx5j3ljOuM3mySnK2\/es5fmADgw0MNjDYwGADQ4eGo\/a6JRtkVF30WtLQ4S\/ZwGADgw0MNjDYwBD5DQerwzvYwGADgw0MNjDYwGADgw0MNjDYwGADgw0MNjDYwGADgw0MNjDYwGADQ4eGB5LVDSLf00KnAAAAAElFTkSuQmCC","description":"Feminizm, kad\u0131nlar\u0131n haklar\u0131n\u0131 tan\u0131yarak bu haklar\u0131n korunmas\u0131 amac\u0131yla e\u015fitsizliklerin ortadan kald\u0131r\u0131lmas\u0131na y\u00f6nelik muhtelif ideolojiler, toplumsal hareketler ve kitle \u00f6rg\u00fctlerinden olu\u015fur. Kad\u0131n hareketi do\u011frudan kad\u0131nlar\u0131 ilgilendiren ve dolayl\u0131 olarak k\u00fclt\u00fcr\u00fcm\u00fcz\u00fc ilgilendiren konularda bilin\u00e7 uyand\u0131r\u0131r. Feminizmin temel objektifleri e\u011fitim, i\u015f, \u00e7ocuk bak\u0131m\u0131 gibi konularda e\u015fit haklara sahip olmaktan, yasal k\u00fcrtaj hakk\u0131ndan, kad\u0131n sa\u011fl\u0131\u011f\u0131 konusunda ilerlemelere, tacizin ve tecav\u00fcz\u00fcn engellenmesinden lezbiyen haklar\u0131na kadar uzan\u0131r."},{"id":42,"name":"MUN (Model United Nations) Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/2707_384843_42_614923_53113.jpg","description":"MUN (Model United Nations) evrensel ve uluslar aras\u0131 bir konferans olup T\u00fcrkiye aya\u011f\u0131n\u0131n da T\u00fcrkiye nin \u00e7e\u015fitli illerinde d\u00fczenlendi\u011fi bir Birle\u015fmi\u015f Milletler sim\u00fclasyonudur. D\u00fcnya \u00fczerinde BM\u2019nin tan\u0131d\u0131\u011f\u0131 tek resmi etkinliktir ve Ba\u015fbakanl\u0131k taraf\u0131ndan finansal olarak desteklenir. MUN\u2019 da d\u00fcnyan\u0131n ve T\u00fcrkiye\u2019nin \u00e7e\u015fitli yerlerinden kat\u0131lan lise ve \u00fcniversite \u00f6\u011frencileri, resmi formatta, komitelere ayr\u0131larak BM\u2019 de oldu\u011fu gibi d\u00fcnyadaki g\u00fcncel sorunlara tart\u0131\u015fma yoluyla \u00e7\u00f6z\u00fcm \u00fcretirler. Ortaya \u00e7\u0131kan sonu\u00e7 Birle\u015fmi\u015f Milletler Genel Sekreterine g\u00f6nderilir. MUN\u2019 da, BM prosed\u00fcrleri bir bir uygulan\u0131r. MUN\u2019 un amac\u0131, \u00f6\u011frencilerin ara\u015ft\u0131rma ve tart\u0131\u015fma becerilerini geli\u015ftirmek, \u0130ngilizceyi rahat\u00e7a kullanarak d\u00fcnya sorunlar\u0131na kafa yormalar\u0131n\u0131 sa\u011flamak, sosyal \u00e7evrelerini geli\u015ftirerek g\u00fc\u00e7l\u00fc bir CV olu\u015fturabilmelerini sa\u011flamak."},{"id":43,"name":"M\u00fcnazara Kul\u00fcb\u00fc","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAdklEQVRoge3YQQ2AMBAAQUBBpeBfBVKQgAT6IGEhM+\/mms39bh1jX+ac53H75pVp2+SXZRoaNDRoaNDQoKFBQ4OGBg0NGho0NGho0NDwh4b12XHP3ion\/WEPGho0NGho0NCgoUFDg4YGDQ0aGjQ0aGjQAADAF1xbJgm5qfdldwAAAABJRU5ErkJggg==","description":null},{"id":44,"name":"M\u00fczisyenler Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/6222_764201_44_614966_50912.jpg","description":"\u0130zmir Ekonomi \u00dcniversitesi \u00f6\u011frencilerinin m\u00fczik konusunda ilgileri, yetenekleri ve becerileri do\u011frultusunda m\u00fczi\u011fe g\u00f6n\u00fcl veren \u00f6\u011frencileri bir araya toplamak ve birlikte m\u00fczi\u011fe dair farkl\u0131 projelere imza atmak amac\u0131yla kurulmu\u015f kul\u00fcb\u00fcm\u00fcz, farkl\u0131 m\u00fczik tarzlar\u0131n\u0131 da ayn\u0131 \u00e7at\u0131 alt\u0131nda toplamay\u0131 hedeflemi\u015ftir. Tarz\u0131n ne olursa olsun m\u00fczik hayat tarz\u0131nsa bize kat\u0131lman\u0131 bekliyoruz."},{"id":45,"name":"Psikoloji Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/2845_273994_45_615207_26288.jpg","description":"Birer \u201cpsikolog\u201d aday\u0131 olarak, hepimiz \u201cBen de hep psikoloji okumak istemi\u015ftim ama\u2026\u201d \u015feklindeki s\u00f6ylemlerle \u00e7ok s\u0131k kar\u015f\u0131la\u015f\u0131yoruz. Bu s\u00f6ylenceden yola \u00e7\u0131karak, Psikoloji'yle ili\u015fkili payla\u015f\u0131ma, bilgiye ve ilgiye a\u00e7l\u0131\u011f\u0131 gidermek i\u00e7in ortak payla\u015f\u0131m alan\u0131m\u0131z\u0131 yaratt\u0131k. Hen\u00fcz \u00e7ok gen\u00e7 say\u0131labilecek olan bu bilim dal\u0131na g\u00f6n\u00fcl vermi\u015f bir topluluk olarak can damarlar\u0131m\u0131z\u0131n geni\u015flemesine katk\u0131da bulunabilece\u011fini d\u00fc\u015f\u00fcnd\u00fc\u011f\u00fcm\u00fcz psikolojiye g\u00f6n\u00fcl vermi\u015f sizlere ihtiyac\u0131m\u0131z var. Bu toplulu\u011fun i\u00e7indeki bitmek bilmez merak d\u00fcrt\u00fcs\u00fcn\u00fcn d\u0131\u015favurumu bizlere ne mi kazand\u0131racak: Payla\u015f\u0131ld\u0131k\u00e7a b\u00fcy\u00fcyen ve bizler i\u00e7in hayati bir bilgi birikimi\u2026"},{"id":46,"name":"Radyo Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/9431_642835_46_615225_61136.jpg","description":"\u00dcniversitemiz \u00f6\u011frencilerinin bireysel giri\u015fimleri sonucunda okulumuzda bir ileti\u015fim a\u011f\u0131n\u0131n olu\u015fmas\u0131 ve \u00fcniversite Radyosu, RadyoEko projesinin hayata ge\u00e7mesi ile Radyo Kul\u00fcb\u00fc kurulmu\u015ftur. Kul\u00fcb\u00fcm\u00fcz\u00fcn ve i\u00e7eri\u011fi olan ileti\u015fim ve radyo kollar\u0131n\u0131n en \u00f6nemli \u00f6zelli\u011fi tamam\u0131yla \u00f6\u011frencilerin giri\u015fimi ve sosyal geli\u015fimlerine dayal\u0131 \u00f6zg\u00fcr bir platform olu\u015fudur. Dolay\u0131s\u0131yla bu ama\u00e7 do\u011frultusunda emek vermek isteyen \u00f6zg\u00fcr, yenilik\u00e7i, giri\u015fimci ve yeteneklerinin bilincinde olan t\u00fcm fertlerimiz bizlerin birer par\u00e7as\u0131d\u0131r."},{"id":47,"name":"Reklamc\u0131l\u0131k Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/4892_767550_47_615239_70000.jpg","description":"Kul\u00fcb\u00fcm\u00fcz Reklamc\u0131l\u0131k d\u00fcnyas\u0131ndaki geli\u015fmeleri takip ederek kazand\u0131\u011f\u0131 bilgi birikimini yapt\u0131\u011f\u0131 etkinlikler ile di\u011fer \u00f6\u011frencilerle payla\u015fmas\u0131 amac\u0131yla kurulmu\u015ftur.\r\n\r\nKul\u00fcb\u00fcm\u00fcz, Reklamc\u0131l\u0131\u011f\u0131n toplumsal yap\u0131s\u0131n\u0131, yap\u0131c\u0131 ve y\u0131k\u0131c\u0131 de\u011fi\u015fim g\u00fcc\u00fcn\u00fcn oldu\u011funun fark\u0131nda ve bu fark\u0131ndal\u0131\u011f\u0131n gerektirdi\u011fi sorumlulu\u011fu \u00fcstlenmi\u015f ki\u015filerin olu\u015fturdu\u011fu bir kul\u00fcpt\u00fcr. Kul\u00fcb\u00fcm\u00fcz\u00fcn \u201cHerkes; hem \u00f6\u011frenci hem \u00f6\u011fretmendir.\u201d ana felsefesini benimsemi\u015f ve bu felsefemiz do\u011frultusunda herkesin \u00f6zg\u00fcrce fikrini payla\u015fabildi\u011fi, hiyerar\u015finin de\u011fil; al\u0131nan kararlara kul\u00fcp \u00fcyelerinin katk\u0131s\u0131n\u0131n oldu\u011fu bir sistem do\u011frultusunda \u00e7al\u0131\u015fmalar\u0131n\u0131 s\u00fcrd\u00fcrecektir."},{"id":48,"name":"Ritim Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/6598_439390_48_615256_94440.jpg","description":"\u0130zmir Ekonomi \u00dcniversitesi b\u00fcnyesinde 2006 y\u0131l\u0131nda kurulmu\u015f olan ve bir\u00e7ok g\u00f6steride yer alan Ritim Kul\u00fcb\u00fcn\u00fcn amac\u0131, \u00fcyeleri aras\u0131ndaki dostluk ba\u011f\u0131n\u0131 g\u00fc\u00e7lendirmek, e\u011flenip \u00f6\u011frenerek \u00f6\u011frendiklerini uygulamal\u0131 olarak g\u00f6sterilerde sunmakt\u0131r. Darbuka, tumba, bendir, erbane ba\u015fta olmak \u00fczere \u00e7ekitli vurmal\u0131 \u00e7alg\u0131 aletleri i\u00e7in nota ile birlikte s\u0131f\u0131rdan e\u011fitim verilmekte ve her y\u0131l \u00e7e\u015fitli kompozisyonlar olu\u015fturulmaktad\u0131r. Perk\u00fcsyon aletlerinin yan\u0131 s\u0131ra di\u011fer m\u00fczik aletlerini de \u00e7alan arkada\u015flarla ortak par\u00e7alar olu\u015fturuyoruz. Kul\u00fcb\u00fcm\u00fczde e\u011flenmek birinci s\u0131rada yer almaktad\u0131r. Kurulu\u015fundan bu yana daima geli\u015fim g\u00f6steren Ritim Kul\u00fcb\u00fc, yurti\u00e7i ve yurt d\u0131\u015f\u0131 bir\u00e7ok aktivitelerde ve projelerde g\u00f6rev alm\u0131\u015ft\u0131r. Deneyimli e\u011fitmenlerle e\u011fitimlere devam eden kul\u00fcb\u00fcn b\u00fcnyesinde 100 \u00fcyesi bulunmaktad\u0131r. Grubumuz de\u011fi\u015fik Arap, Oryantal, Salsa vb. ezgilerle g\u00f6sterilerine renk katmaktad\u0131r."},{"id":49,"name":"Sa\u011fl\u0131k Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/8510_822987_49_615275_40948.jpg","description":"\u00d6\u011frenci kul\u00fcb\u00fcm\u00fcz, yak\u0131n ve uzak \u00e7evresine kar\u015f\u0131 duyarl\u0131, \u00fclke ve d\u00fcnya sorunlar\u0131n\u0131 izleyen ve \u00e7\u00f6z\u00fcm konusunda \u00e7al\u0131\u015fmalar yapan, bilgi ve k\u00fclt\u00fcr\u00fc payla\u015fan bir kul\u00fcpt\u00fcr. Ayr\u0131ca \u00fcniversitemizin misyonu do\u011frultusunda akademik \u00e7al\u0131\u015fmalar\u0131n yan\u0131s\u0131ra \u00f6\u011frencilerin bedensel, zihinsel, k\u00fclt\u00fcrel, sanatsal geli\u015fimlerini sa\u011flamak ve bu alanlardaki yeteneklerini ve niteliklerini artt\u0131rmak amac\u0131yla faaliyet g\u00f6stermekte ve \u00e7al\u0131\u015fmalar\u0131n\u0131 bu \u00e7er\u00e7evede s\u00fcrd\u00fcrmekteyiz. Bu do\u011frultuda kul\u00fcb\u00fcm\u00fcz arac\u0131l\u0131\u011f\u0131yla \u00f6\u011frencilerimiz kongre,seminer,konferans,s\u00f6yle\u015fi,sergi,panel.g\u00f6steri gibi hem sosyal alanda hem de akademik alanda faydalanacaklar\u0131 etkinlikleri yapmaktay\u0131z."},{"id":50,"name":"Sinema ve medya kul\u00fcb\u00fc","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAmElEQVRoge3YwQmAMBAFUSMpJKVYmqWlFEuxhR+QOCzzzmFxiIew7cjc40pPPnPztDOcRWYDgw0MNjDYwGADQ4WGnj+\/fpF8XoV7sIHBBgYbGGxgsIGhQkODv\/kSPT+6fwkZTqvwL9nAYAODDQw2MNjAYAODDQw2MNjAYAODDQw2MCzs+RLJ9u5zFe7BBgYbGGxgsIHBBoYX8tkRRz0Dbr8AAAAASUVORK5CYII=","description":null},{"id":51,"name":"Siyaset Platform Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/7219_327734_51_615297_55749.jpg","description":"Kul\u00fcb\u00fcn amac\u0131 \u015fu \u015fekildedir; \u00f6\u011frencilerin ders d\u0131\u015f\u0131nda bilgi, g\u00f6rg\u00fc, deneyimini artt\u0131r\u0131c\u0131 nitelikte sosyal, k\u00fclt\u00fcrel faaliyetler y\u00fcr\u00fctmelerini sa\u011flamak, ilgili konularda duyarl\u0131l\u0131\u011f\u0131n geli\u015ftirilmesine katk\u0131da bulunmak, siyasal, toplumsal ve ilgili di\u011fer kurumlarda \u00f6\u011frencilerin bilgilenmesi, fikir payla\u015f\u0131m\u0131nda bulunmas\u0131 i\u00e7in \u00e7al\u0131\u015fmalarda bulunmak. Ama\u00e7lar\u0131 do\u011frultusunda konferans, panel, s\u00f6yle\u015fi, sempozyum, vb. aktivitelerde bulunarak \u00f6\u011frencileri konuyla ilgili uzman ki\u015filerle bulu\u015fturarak ufuklar\u0131n\u0131 geni\u015fletmek ve alana ili\u015fkin g\u00fcncel bilgilerden haberdar olmalar\u0131n\u0131 sa\u011flamak ayr\u0131ca bilgilendirici yay\u0131nlar yapmak; dergi vb. yay\u0131n faaliyetlerinde bulmak. Ulusal-uluslararas\u0131 kurum ve kurulu\u015flarla ortak projeler y\u00fcr\u00fctmek ve \u00fcniversite \u00f6\u011frencilerini bu etkile\u015fim i\u00e7erisinde aktif hale getirmek."},{"id":52,"name":"Sosyoloji Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/3065_671408_52_615314_36753.jpg","description":"Sosyal ve K\u00fclt\u00fcrel faaliyetlerle \u00fcniversite \u00f6\u011frencilerinin moral ve ki\u015filik geli\u015fmesini desteklemek, toplum bilincinin olu\u015fmas\u0131n\u0131 sa\u011flamak, bo\u015f zamanlar\u0131n\u0131 de\u011ferlendirmelerini ve akademik geli\u015fmelerine katk\u0131 sa\u011flamak amac\u0131yla kurulmu\u015f sosyoloji kul\u00fcb\u00fc ger\u00e7ekle\u015ftirilecek sosyal-k\u00fclt\u00fcrel etkinliklerle geli\u015fim ve de\u011fi\u015fimi topluma kazand\u0131rmay\u0131 ama\u00e7 edinmi\u015ftir."},{"id":53,"name":"Sualt\u0131 Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/8246_545138_53_615345_77171.jpg","description":"Amac\u0131 \u00f6ncelikli olarak sualt\u0131 d\u00fcnyas\u0131na bilin\u00e7li ve iyi e\u011fitimli dalg\u0131\u00e7lar kazand\u0131rmak olan kul\u00fcb\u00fcm\u00fcz\u00fcn yeni olmas\u0131na ra\u011fmen k\u0131sa s\u00fcrede \u00e7ok \u00f6nemli geli\u015fmeler kaydeden ve T\u00fcrkiye de en \u00e7ok tercih edilen \u00fcniversitelerden biri haline gelen \u00fcniversitemiz ile ayn\u0131 paralelde k\u0131sa s\u00fcrede \u00f6nemli bir yere geldi\u011fini d\u00fc\u015f\u00fcn\u00fcyoruz. Bug\u00fcn 60'\u0131 aktif olmak \u00fczere 400 den fazla \u00fcyemiz ile faaliyetlerimize devam etmekteyiz.\r\n\r\n\u00d6zellikle e\u011fitmenlerimizin kendi i\u00e7imizden olmas\u0131n\u0131n avantaj\u0131 ile samimi ve aktif bir ortamda her seviye e\u011fitimi verebiliyoruz. Genel olarak bak\u0131\u015f a\u00e7\u0131m\u0131z\u0131 a\u00e7\u0131klamak gerekirse ise; Biz \u00fcniversiteyi sadece akademik altyap\u0131 kazan\u0131lan bir yer olarak g\u00f6rm\u00fcyoruz. Bu g\u00f6r\u00fc\u015f\u00fcm\u00fcz do\u011frultusunda \u00fcyelerimize vaat etti\u011fimiz yegane \u015fey, \u00f6ncelikle s\u0131cak bir sosyal ortam. Kul\u00fcb\u00fcm\u00fczdeki herkesi \u00fcyeden \u00f6nce arkada\u015flar\u0131m\u0131z olarak g\u00f6r\u00fcyoruz ve bu arkada\u015fl\u0131klar sualt\u0131yla s\u0131n\u0131rl\u0131 kalm\u0131yor. Akl\u0131n\u0131za gelebilecek her konuda birbirimize destek olmaya \u00e7al\u0131\u015f\u0131yoruz.\r\n\r\n\u0130\u015fimizi ger\u00e7ekten \u00e7ok ciddiye al\u0131yoruz fakat bu e\u011flenceli vakit ge\u00e7iremeyece\u011fimiz anlam\u0131na gelmiyor. T\u00fcm aktivitelerimizde herkesin olabildi\u011fince keyif al\u0131p e\u011flenmesi, kendine bir \u015feyler katabilmesi \u00f6ncelikli amac\u0131m\u0131z. Bu g\u00f6r\u00fc\u015f\u00fcm\u00fczden dolay\u0131 di\u011fer baz\u0131 sualt\u0131 organizasyonlar\u0131ndan ayr\u0131 bir konumda oldu\u011fumuzu d\u00fc\u015f\u00fcn\u00fcyoruz. Genel aktivitelerimizi sayacak olursak, d\u00fczenli aral\u0131klarla de\u011fi\u015fik dal\u0131\u015f noktalar\u0131na geziler d\u00fczenliyoruz, film belgesel g\u00f6sterimleri yap\u0131yoruz, sualt\u0131 d\u0131\u015f\u0131nda da birlikteli\u011fimizin devam\u0131 amac\u0131yla organizasyonlar d\u00fczenliyoruz, her sene ba\u015f\u0131nda \u00fcniversitemize yeni ba\u015flayan \u00f6\u011frencilerin adaptasyonu amac\u0131 ile d\u00fczenlenen oryantasyon haftalar\u0131nda kul\u00fcb\u00fcm\u00fcze ait konferanslar d\u00fczenliyoruz, temizlik dal\u0131\u015flar\u0131 organize ediyoruz."},{"id":54,"name":"Think End\u00fcstriyel Tasar\u0131m Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/1214_337805_54_615364_17256.jpg","description":"Think; bir \u0130zmir Ekonomi \u00dcniversitesi \u00f6\u011frenci kul\u00fcb\u00fcd\u00fcr. Think\u2019 in ana amac\u0131 \u00f6\u011frencilerin tasar\u0131m yeteneklerini geli\u015ftirmek ve tasar\u0131m i\u00e7in yeni sosyal alanlar\u0131 yaratmakt\u0131r."},{"id":55,"name":"T\u0131p \u00d6\u011frencileri Kul\u00fcb\u00fc","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAmUlEQVRoge3ZQQ2AQAwAQUoQhBSkIOWkIAVJSKCPhluanXdD2ByP5ohrH8vPrbNfoIANDDYw2MBgA4MNDDYwbPnR4z5fZ\/JbcOHTOpyDDQw2MNjAYAODDQzR4J4vknOzUjOrYYdvyQYGGxhsYLCBwQaGDjtfh3OwgcEGBhsYbGCwgcEGhvp7vu9\/AXc4BxsYbGCwgcEGBhsYHrFmE9BK3ipWAAAAAElFTkSuQmCC","description":null},{"id":56,"name":"Tiyatro Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/5404_924454_56_615388_27535.jpg","description":"2002 y\u0131l\u0131nda kurulan kul\u00fcp \u00fcniversite b\u00fcnyesindeki en aktif kul\u00fcplerin ba\u015f\u0131nda gelir.2002 y\u0131l\u0131ndan bu yana \u00f6\u011frencilerin sosyalle\u015fmesine katk\u0131s\u0131 d\u0131\u015f\u0131nda; d\u00fczene kar\u015f\u0131 olan ele\u015ftirel yap\u0131s\u0131 ve insanlara tiyatro yoluyla sanat\u0131 sevdirmek ve de hayata kar\u015f\u0131 ele\u015ftirel ve farkl\u0131 bir bak\u0131\u015f a\u00e7\u0131s\u0131yla bakmalar\u0131n\u0131 sa\u011flamay\u0131 ama\u00e7lam\u0131\u015f olup bu do\u011frultuda bir\u00e7ok \u00f6\u011frenci yeti\u015ftirmi\u015ftir. Amfitrionmix, G\u00f6zlerimi Kapar\u0131m Vazifemi Yapar\u0131m, G\u00fcl\u00fc Seven Dikenine Saplan\u0131r, \u015eahlar\u0131da Vururlar, Lysistrata, Ka\u00e7 Baba Ka\u00e7  ve T\u00f6re, Cimri ve bir\u00e7ok sergilenmi\u015f oyun yan\u0131nda Atilla \u0130lhan \u015eiir g\u00f6sterisi ve Aziz Nesin Anma gibi \u00f6nemli g\u00fcnlerde oyunlar sergilemi\u015ftir."},{"id":57,"name":"Toplum G\u00f6n\u00fcll\u00fcleri Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/2211_203234_57_615406_12145.jpg","description":"Toplum G\u00f6n\u00fclleri,\"ele\u015ftirmek i\u00e7in de\u011fil de\u011fi\u015ftirmek i\u00e7in\" felsefesiyle \u00e7\u0131kt\u0131\u011f\u0131 yolda, \u00e7e\u015fitli sosyal sorumluluk projeleri hayata ge\u00e7irerek gen\u00e7lerin enerjisini toplumsal faydaya d\u00f6n\u00fc\u015ft\u00fcrmeyi, bunu yaparken de gen\u00e7lerin ki\u015fisel geli\u015fimini sa\u011flamay\u0131 hedefleyen bir de\u011fi\u015fim ve geli\u015fim projesidir. Gen\u00e7lerin \u00f6nc\u00fcl\u00fc\u011f\u00fcnde ve yeti\u015fkinlerin rehberli\u011finde toplumsal bar\u0131\u015f, dayan\u0131\u015fma ve de\u011fi\u015fimi hedeflemektedir."},{"id":58,"name":"T\u00fcrk Dili ve Tarih Ara\u015ft\u0131rmalar\u0131 Kul\u00fcb\u00fc","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/6449_275535_58_615430_24357.jpg","description":"T\u00fcrk Dili ve Edebiyat\u0131\u2019na ilgi duyan \u00f6\u011frencileri bir araya getirerek, T\u00fcrk Dili ve Edebiyat\u0131 alan\u0131nda yap\u0131lacak olan \u00e7al\u0131\u015fmalar\u0131 organize etmek ve di\u011fer \u00f6\u011frencilerle payla\u015fmak kul\u00fcb\u00fcn kurulu\u015f amac\u0131d\u0131r."},{"id":59,"name":"T\u00fcrk Sanat M\u00fczi\u011fi Kul\u00fcb\u00fc (Korosu)","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAlklEQVRoge3aQQ2AMAxAUUoQgiQkIG0SkIQUJNBDBz\/Nf+dm4WeXZSyWnH0cycla93m9zqwffMdsNjDYwGADgw0MNjB0aIi\/DnOFOuyDDQw2MNjAYAODDQw2MGzlK2ZuF2sPyx32wQYGGxhsYLCBwQYGGxhsYLCBwQYGGxhsYOjQ0OHfbiTnfJM4lw0MNjDYwGADgw0MDwA+C6E3UEBkAAAAAElFTkSuQmCC","description":null},{"id":60,"name":"Yap\u0131 Kul\u00fcb\u00fc (\u0130n\u015faat)","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/5851_794702_60_615454_23030.jpg","description":"Yap\u0131 Kul\u00fcb\u00fc ailesi Ekonomili olman\u0131n ayr\u0131cal\u0131\u011f\u0131n\u0131 \u00f6\u011frencilere daha iyi hissettirmek ve onlar\u0131n ki\u015fisel geli\u015fimlerine katk\u0131da bulunabilmek i\u00e7in \u00e7al\u0131\u015f\u0131r. \u00d6\u011frenciler kul\u00fcp \u00e7al\u0131\u015fmalar\u0131nda tak\u0131m olabilmenin \u00f6nemini kavrayarak hayatlar\u0131nda daha aktif ve sosyal roller \u00fcstlenmektedirler. Kul\u00fcb\u00fcm\u00fcz\u00fc; i\u015f sekt\u00f6r\u00fcn\u00fc \u00f6\u011frencilerle bir araya getiren ve onlar\u0131n mesleki d\u00fcnyay\u0131 daha iyi tan\u0131mas\u0131n\u0131 sa\u011flayan, sosyal ve k\u00fclt\u00fcrel anlamda geli\u015fmeyi sa\u011flayan bir k\u00f6pr\u00fc olarak da tan\u0131mlayabiliriz."}]}


# GET /api/club/1

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {}

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 57

    + Body

            {"successful":true,"code":200,"response":{"details":{"id":1,"name":"Oyun Geli\u015ftirme Tak\u0131m\u0131","photo":"http:\/\/stucial.com\/images\/club\/profile\/medium\/5763_583744_1_616195_84358.jpg","description":"\u00dclkemizde emekleme a\u015famas\u0131nda olan oyun sekt\u00f6r\u00fc, bu t\u00fcr organizasyonlar sayesinde ilerlemeye ve b\u00fcy\u00fcmeye devam ediyor. Bizim i\u00e7in bu \u00e7abalar \u00e7ok \u00f6nemli \u00e7\u00fcnk\u00fc T\u00fcrkiye'de halen lisans d\u00fczeyinde 'Bilgisayar Oyunlar\u0131' e\u011fitimi ilk ve tek olarak \u0130zmir Ekonomi \u00dcniversitesi'nde veriliyor.","university":"Izmir University of Economics","is_member":false},"members":[{"id":1,"name":"Furkan Kavlak","username":"furkankavlak","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/9110_836555_1_083754_58205.jpg","department":"Software Engineering"},{"id":8,"name":"Hakan Eren","username":"Heren","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAeElEQVRoge3asQ2AMAwAwYTJGJ3RWCEFgie6q63IL7eZ47zGzx1fL\/AADQ0aGjQ0aGjQ0KChQUODhgYNDRoaNDRoaNDQoKFBQ4OGBg0NGho0NGho0NCgoUFDg4YGDQ07NMzVwfWvi9f58ms73EFDg4YGDQ0aGjQ03LxHBYF86sXyAAAAAElFTkSuQmCC","department":"International Trade and Finance"},{"id":15,"name":"Buse Turgay","username":"buseturgay","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAnklEQVRoge3ZwQmAMBAFUSMpwNIsyZIszRIswX9YyLjMO0vIsB4WHedxbT+3r75AARsYbGCwgcEGBhsYbGAYtcflW\/D9pE9+6jAHGxhsYLCBwQYGGxjmkg0nFN6twxxsYLCBwQYGGxhsYOjQMBr8202\/861KTZblDu+SDQw2MNjAYAODDQwdGmb5icmWVrtBdpiDDQw2MNjAYAODDQwv7NYPI+SiuqsAAAAASUVORK5CYII=","department":"Visual Communication Design"}]}}


# POST /api/club/apply

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "club": "1"
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 56

    + Body

            {"successful":true,"code":200,"response":[]}


# POST /api/club/leave

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "club": "1"
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 56

    + Body

            {"successful":true,"code":200,"response":[]}


# GET /api/friendship/requests

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {}

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 59

    + Body

            {"successful":true,"code":200,"response":[]}


# GET /api/friendship/suggests

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {}

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 58

    + Body

            {"successful":true,"code":200,"response":[{"id":3,"name":"Simge \u00d6zcan","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/7736_213764_3_637175_32818.jpg","university":"Izmir University of Economics","department":"Software Engineering","mutual_friends":0},{"id":7,"name":"Irem Kavlak","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAmklEQVRoge3ZwQmAMBAFUSMWYKkpJaWmBFtYYQnDZ95ZFoZ4WJJx1ax3Fr+cex2edhdnkdnAYAODDQw2MNjAkNDwtE+s73NdEs7BBgYbGGxgsIHBBoaEhnF+RWv3Y289fwlZnJbwL9nAYAODDQw2MNjAkLDzJZyDDQw2MNjAYAODDQw2MPS\/T\/deB1YknIMNDDYw2MBgA4MNDB8K+RK2E3qgMQAAAABJRU5ErkJggg==","university":"Izmir University of Economics","department":"Interior Architecture and Environmental Design","mutual_friends":0},{"id":4,"name":"Simge G\u00fc\u00e7l\u00fckol","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/7002_507542_4_154219_13949.jpg","university":"Izmir University of Economics","department":"Industrial Engineering","mutual_friends":0},{"id":14,"name":"ahmet gecgel","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAmElEQVRoge3YwQmAMBAFUSMWYgmWZGkpyRIsxRKM8IPDMu8cQoa9hG3LmPPaB09m9eN+PbPOf8Z0NjDYwGADgw0MNjBUaGh\/feaCKszBBgYbGGxgsIHBBgYbGLb4jSPbxexnucIcbGCwgcEGBhsYbGBwz8dgA4MNDDYw2MBgA4MNDB\/2fNkFXvC2CnOwgcEGBhsYbGCwgeEBTgYPJBD12t0AAAAASUVORK5CYII=","university":"Izmir University of Economics","department":"Law","mutual_friends":0},{"id":15,"name":"Buse Turgay","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAnklEQVRoge3ZwQmAMBAFUSMpwNIsyZIszRIswX9YyLjMO0vIsB4WHedxbT+3r75AARsYbGCwgcEGBhsYbGAYtcflW\/D9pE9+6jAHGxhsYLCBwQYGGxjmkg0nFN6twxxsYLCBwQYGGxhsYOjQMBr8202\/861KTZblDu+SDQw2MNjAYAODDQwdGmb5icmWVrtBdpiDDQw2MNjAYAODDQwv7NYPI+SiuqsAAAAASUVORK5CYII=","university":"Izmir University of Economics","department":"Visual Communication Design","mutual_friends":0},{"id":12,"name":"Hasan alex bamboulis","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAnklEQVRoge3ZwQmAMBQEUSMWZimWYimWYmmW4B4Cjsu880cZ4uETx3kfS+bcr\/eZL562hq8ks4HBBgYbGGxgsIGhoWHkWxrWCOe+Sk3W24ZvyQYGGxhsYLCBwQaGhp1vy0eT9Wsu7yp\/xQYGGxhsYLCBoaFh\/s439xdwouEcbGCwgcEGBhsYbGBoaGi452s4BxsYbGCwgcEGBhsYGhoeA9gXnw+RvhQAAAAASUVORK5CYII=","university":"Izmir University of Economics","department":"Industrial Engineering","mutual_friends":0},{"id":5,"name":"Hatice Ertu\u011frul","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/9838_945837_5_643458_79137.jpg","university":"Izmir University of Economics","department":"Software Engineering","mutual_friends":0},{"id":9,"name":"Ece Sa\u011fduyu","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/9521_245790_9_659571_24674.jpg","university":"Izmir University of Economics","department":"Industrial Engineering","mutual_friends":0},{"id":10,"name":"Ezgi Ersoydan","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAkklEQVRoge3awQnDMBAAwSikQJeQ0lJCSkwJ0UPg4dh5mxOL\/BBI6\/N9bHpf\/7+5Zdpzd01YDYYaDDUYajDUYJjQsPZPaawJ+1CDoQZDDYYaDDUYajDUYKjBUIOhBkMNhhoMExpexyeevcbeMWEfajDUYKjBUIOhBsOEu911dtzZN4mbJvxLNRhqMNRgqMFQg+EH8uEMcTPzfz0AAAAASUVORK5CYII=","university":"Izmir University of Economics","department":"Industrial Engineering","mutual_friends":0},{"id":16,"name":"Semih Pehlivan","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAh0lEQVRoge3ZwQkEIRAAwXW5wC60De1C2xR8DFwhXW9RGj8yrmvU8\/y2V36nDr2nNvqjGgw1GGow1GCowVCDoQZDDYYaDDUYajDUYFj7kznWCfdQg6EGQw2GGgw1GGowfMZ33Pl4nn0sn3APNRhqMNRgqMFQg6E5n6EGQw2GGgw1GGownNDwAuPCCgIjO4pUAAAAAElFTkSuQmCC","university":"Izmir University of Economics","department":"Software Engineering","mutual_friends":0}]}


# POST /api/friendship/add

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "student": "1"
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 59

    + Body

            {"successful":true,"code":200,"response":[]}


# POST /api/friendship/answer

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "student": "1",
                "answer": "accept"
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 60

    + Body

            {"successful":true,"code":200,"response":[]}


# POST /api/friendship/remove

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "student": "1"
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 59

    + Body

            {"successful":true,"code":200,"response":[]}


# GET /api/messages

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {}

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 38

    + Body

            {"successful":true,"code":200,"response":[{"id":1,"title":"Bitiremeyenler","short":"B","photo":null,"unseen":0,"muted":false,"type":0,"last":{"id":31,"message":"http:\/\/app.stucial.com \ud83d\udc4d\ud83c\udffb","time":"04:27","sender":{"id":1,"name":"Furkan Kavlak"}}},{"id":1798,"title":"Architecture","short":"A","photo":null,"unseen":0,"muted":false,"type":4,"last":{"id":7,"message":"Stucial Tester joined.","time":"11:08","sender":{"id":null,"name":null}}},{"id":2,"title":"ACC 307","short":"A","photo":null,"unseen":0,"muted":false,"type":2,"last":null}]}


# GET /api/messages/1/info

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {}

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 58

    + Body

            {"successful":true,"code":200,"response":{"id":1,"title":"Bitiremeyenler","photo":null,"type":0,"muted":true,"content":[],"participants":[{"id":1,"name":"Furkan Kavlak","photo":"http:\/\/stucial.com\/images\/student\/profile\/small\/9110_836555_1_083754_58205.jpg","department":"Software Engineering"},{"id":2,"name":"Stucial Tester","photo":"http:\/\/stucial.com\/images\/student\/profile\/small\/7324_200512_2_447326_16366.jpg","department":"Architecture"},{"id":3,"name":"Simge \u00d6zcan","photo":"http:\/\/stucial.com\/images\/student\/profile\/small\/7736_213764_3_637175_32818.jpg","department":"Software Engineering"},{"id":4,"name":"Simge G\u00fc\u00e7l\u00fckol","photo":"http:\/\/stucial.com\/images\/student\/profile\/small\/7002_507542_4_154219_13949.jpg","department":"Industrial Engineering"},{"id":5,"name":"Hatice Ertu\u011frul","photo":"http:\/\/stucial.com\/images\/student\/profile\/small\/9838_945837_5_643458_79137.jpg","department":"Software Engineering"}]}}


# POST /api/messages/1

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "first": null,
                "last": null,
                "type": 0
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 56

    + Body

            {"successful":true,"code":200,"response":[{"_id":69,"text":"Api testi ger\u00e7ekle\u015ftiriyorum.","createdAt":"2018-04-26T01:31:14+03:00","user":{"_id":2,"name":"Stucial Tester","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/7324_200512_2_447326_16366.jpg"}},{"_id":31,"text":"http:\/\/app.stucial.com \ud83d\udc4d\ud83c\udffb","createdAt":"2018-04-25T04:27:49+03:00","user":{"_id":1,"name":"Furkan Kavlak","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/9110_836555_1_083754_58205.jpg"}},{"_id":23,"text":"Tamamd\u0131r \ud83d\udc4d\ud83c\udffb s\u00f6yliyim dedim","createdAt":"2018-04-24T23:00:29+03:00","user":{"_id":3,"name":"Simge \u00d6zcan","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/7736_213764_3_637175_32818.jpg"}},{"_id":22,"text":"oturumlar\u0131 s\u0131f\u0131rlar\u0131m yay\u0131nlamadan \u00f6nce d\u00fczelir \ud83d\udc4d\ud83c\udffb","createdAt":"2018-04-24T22:59:52+03:00","user":{"_id":1,"name":"Furkan Kavlak","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/9110_836555_1_083754_58205.jpg"}},{"_id":21,"text":"Oturumlar 2\u015fer kere a\u00e7\u0131lm\u0131\u015f neden bilmiyorum d\u00fczelttim ama o hatay\u0131","createdAt":"2018-04-24T22:59:38+03:00","user":{"_id":1,"name":"Furkan Kavlak","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/9110_836555_1_083754_58205.jpg"}},{"_id":20,"text":"Bildirimler size de iki kere geliyor mu","createdAt":"2018-04-24T22:58:55+03:00","user":{"_id":3,"name":"Simge \u00d6zcan","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/7736_213764_3_637175_32818.jpg"}},{"_id":19,"text":"Peki\ud83d\ude05","createdAt":"2018-04-24T22:58:29+03:00","user":{"_id":4,"name":"Simge G\u00fc\u00e7l\u00fckol","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/7002_507542_4_154219_13949.jpg"}},{"_id":18,"text":"bug\u00fcn \u00e7ok konu\u015fmay\u0131n test edecekler birazdan \ud83d\ude04","createdAt":"2018-04-24T22:58:04+03:00","user":{"_id":1,"name":"Furkan Kavlak","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/9110_836555_1_083754_58205.jpg"}},{"_id":17,"text":"Art\u0131k burdan konu\u015fal\u0131m bence\ud83d\ude02","createdAt":"2018-04-24T22:57:25+03:00","user":{"_id":4,"name":"Simge G\u00fc\u00e7l\u00fckol","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/7002_507542_4_154219_13949.jpg"}},{"_id":16,"text":"S\u00fcper\ud83d\ude04","createdAt":"2018-04-24T22:57:06+03:00","user":{"_id":4,"name":"Simge G\u00fc\u00e7l\u00fckol","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/7002_507542_4_154219_13949.jpg"}},{"_id":15,"text":"Yeni versiyonda bildirime t\u0131klay\u0131nca mesaj\u0131 da a\u00e7\u0131yor \ud83d\ude48","createdAt":"2018-04-24T22:56:42+03:00","user":{"_id":1,"name":"Furkan Kavlak","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/9110_836555_1_083754_58205.jpg"}},{"_id":14,"text":"\ud83d\ude04","createdAt":"2018-04-24T22:56:26+03:00","user":{"_id":4,"name":"Simge G\u00fc\u00e7l\u00fckol","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/7002_507542_4_154219_13949.jpg"}},{"_id":13,"text":"Bildirim \u00f6zelli\u011fi hay\u0131rl\u0131 olsun \ud83d\ude01","createdAt":"2018-04-24T22:56:04+03:00","user":{"_id":3,"name":"Simge \u00d6zcan","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/7736_213764_3_637175_32818.jpg"}},{"_id":12,"text":"\ud83e\udd2d","createdAt":"2018-04-24T22:55:28+03:00","user":{"_id":4,"name":"Simge G\u00fc\u00e7l\u00fckol","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/7002_507542_4_154219_13949.jpg"}},{"_id":11,"text":"Selam! \ud83d\udc4b\ud83c\udffb","createdAt":"2018-04-24T22:46:59+03:00","user":{"_id":2,"name":"Stucial Tester","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/7324_200512_2_447326_16366.jpg"}},{"_id":3,"text":"\ud83e\udd17\ud83c\udf89","createdAt":"2018-04-22T12:00:44+03:00","user":{"_id":3,"name":"Simge \u00d6zcan","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/7736_213764_3_637175_32818.jpg"}},{"_id":2,"text":"\ud83c\uddf9\ud83c\uddf7","createdAt":"2018-04-22T02:40:50+03:00","user":{"_id":4,"name":"Simge G\u00fc\u00e7l\u00fckol","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/7002_507542_4_154219_13949.jpg"}},{"_id":1,"text":"As bayraklar\u0131 \ud83c\uddf9\ud83c\uddf7\ud83c\uddf9\ud83c\uddf7\ud83c\uddf9\ud83c\uddf7","createdAt":"2018-04-21T21:04:26+03:00","user":{"_id":1,"name":"Furkan Kavlak","avatar":"http:\/\/stucial.com\/images\/student\/profile\/small\/9110_836555_1_083754_58205.jpg"}}]}


# POST /api/messages/send

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "group": "1",
                "message": "Api testi gerçekleştiriyorum."
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 54

    + Body

            {"successful":true,"code":200,"response":{"id":69}}


# POST /api/messages/mute

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "message": "1",
                "mute": true
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 59

    + Body

            {"successful":true,"code":200,"response":[]}


# POST /api/messages/leave

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "message": "1"
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 56

    + Body

            {"successful":true,"code":200,"response":[]}


# GET /api/events/2018-04-02

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {}

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 55

    + Body

            {"successful":true,"code":200,"response":{"events":[{"id":1,"name":"Test Etkinli\u011fi","place":"T\u00f6ren alan\u0131","date":"2018-04-24","photo":"http:\/\/stucial.com\/images\/event\/small\/9142_509041_1_216900_82901.jpg","attendees":1}],"markeds":{"2018-04-24":{"marked":true}}}}


# GET /api/event/1

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {}

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 49

    + Body

            {"successful":true,"code":200,"response":{"event":{"id":1,"name":"Test Etkinli\u011fi","place":"T\u00f6ren alan\u0131","description":"Hep beraber test yap\u0131yoruz!","date":"24\/04\/2018\n13:00 - 14:00","photo":"http:\/\/stucial.com\/images\/event\/big\/9142_509041_1_216900_82901.jpg","is_attending":false},"attendees":[{"id":1,"name":"Furkan Kavlak","photo":"http:\/\/stucial.com\/images\/student\/profile\/small\/9110_836555_1_083754_58205.jpg","department":"Software Engineering"}]}}


# POST /api/event/attending

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "event": "1",
                "type": "attend"
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 57

    + Body

            {"successful":true,"code":200,"response":[]}


# GET /api/departments

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {}

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 40

    + Body

            {"successful":true,"code":200,"response":[{"id":168,"name":"Accounting and Auditing Program","short":"AAP"},{"id":128,"name":"Accounting and Tax Applications","short":"ATA"},{"id":158,"name":"Aerospace Engineering","short":"AE"},{"id":129,"name":"Applied English and Translation","short":"AET"},{"id":130,"name":"Architectural Restoration","short":"AR"},{"id":185,"name":"Architecture","short":"A"},{"id":131,"name":"Banking and Insurance","short":"BI"},{"id":159,"name":"Biomedical Engineering","short":"BE"},{"id":169,"name":"Business Administration","short":"BA"},{"id":147,"name":"Child Development","short":"CD"},{"id":180,"name":"Cinema and Digital Media","short":"CDM"},{"id":132,"name":"Civil Aviation Cabin Services","short":"CACS"},{"id":133,"name":"Civil Aviation Transportation Management","short":"CATM"},{"id":160,"name":"Civil Engineering","short":"CE"},{"id":162,"name":"Computer Engineering","short":"CE"},{"id":134,"name":"Computer Programming","short":"CP"},{"id":135,"name":"Construction Technology","short":"CT"},{"id":157,"name":"Culinary Arts and Management","short":"CAM"},{"id":170,"name":"Economics","short":"E"},{"id":148,"name":"Elderly Care","short":"EC"},{"id":163,"name":"Electrical and Electronics Engineering","short":"EEE"},{"id":189,"name":"Fashion and Textile Design","short":"FTD"},{"id":164,"name":"Food Engineering","short":"FE"},{"id":136,"name":"Footwear Design and Manufacturing","short":"FDM"},{"id":137,"name":"Foreign Trade","short":"FT"},{"id":138,"name":"Furniture and Decoration","short":"FD"},{"id":165,"name":"Genetics and Bioengineering","short":"GB"},{"id":139,"name":"Graphic Design","short":"GD"},{"id":183,"name":"Health Management","short":"HM"},{"id":186,"name":"Industrial Design","short":"ID"},{"id":187,"name":"Industrial Engineering","short":"IE"},{"id":188,"name":"Interior Architecture and Environmental Design","short":"IAED"},{"id":140,"name":"Interior Design","short":"ID"},{"id":171,"name":"International Trade and Finance","short":"ITF"},{"id":155,"name":"Justice","short":"J"},{"id":141,"name":"Lavaboratory Technology","short":"LT"},{"id":174,"name":"Law","short":"L"},{"id":156,"name":"Legal Office Management and Secretariat","short":"LOMS"},{"id":172,"name":"Logistics Management ","short":"LM"},{"id":142,"name":"Marketing","short":"M"},{"id":175,"name":"Mathematics","short":"M"},{"id":166,"name":"Mechanical Engineering","short":"ME"},{"id":167,"name":"Mechatronics Engineering","short":"ME"},{"id":181,"name":"Media and Communication","short":"MC"},{"id":150,"name":"Medical Documentation and Secreteriat","short":"MDS"},{"id":152,"name":"Medical Imaging Techniques","short":"MIT"},{"id":151,"name":"Medical Laboratory Techniques","short":"MLT"},{"id":184,"name":"Nursing","short":"N"},{"id":143,"name":"Occupational Health and Safety","short":"OHS"},{"id":153,"name":"Opticianry","short":"O"},{"id":149,"name":"Paramedic","short":"P"},{"id":177,"name":"Physics","short":"P"},{"id":154,"name":"Physiotherapy","short":"P"},{"id":173,"name":"Political Science and International Relations","short":"PSIR"},{"id":178,"name":"Psychology","short":"P"},{"id":182,"name":"Public Relations and Advertising","short":"PRA"},{"id":144,"name":"Radio and Tv Programming","short":"RTP"},{"id":145,"name":"Real Estate and Real Estate Management","short":"REREM"},{"id":179,"name":"Sociology","short":"S"},{"id":161,"name":"Software Engineering","short":"SE"},{"id":146,"name":"Tourism and Hotel Management","short":"THM"},{"id":176,"name":"Translation and Interpretation (English)","short":"TI"},{"id":190,"name":"Visual Communication Design","short":"VCD"}]}


# GET /api/department/161

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {}

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 57

    + Body

            {"successful":true,"code":200,"response":{"details":{"id":161,"name":"Software Engineering","university":"Izmir University of Economics","short":"SE","is_member":false},"students":[{"id":1,"name":"Furkan Kavlak","username":"furkankavlak","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/9110_836555_1_083754_58205.jpg","department":"Software Engineering"},{"id":3,"name":"Simge \u00d6zcan","username":"ozcnsimge","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/7736_213764_3_637175_32818.jpg","department":"Software Engineering"},{"id":5,"name":"Hatice Ertu\u011frul","username":"Haticeertugrul","photo":"http:\/\/stucial.com\/images\/student\/profile\/medium\/9838_945837_5_643458_79137.jpg","department":"Software Engineering"},{"id":16,"name":"Semih Pehlivan","username":"semihpehlivan","photo":"data:image\/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEEAAABBCAIAAAABlV4SAAAABnRSTlMAAAAAAABupgeRAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAAh0lEQVRoge3ZwQkEIRAAwXW5wC60De1C2xR8DFwhXW9RGj8yrmvU8\/y2V36nDr2nNvqjGgw1GGow1GCowVCDoQZDDYYaDDUYajDUYFj7kznWCfdQg6EGQw2GGgw1GGowfMZ33Pl4nn0sn3APNRhqMNRgqMFQg6E5n6EGQw2GGgw1GGownNDwAuPCCgIjO4pUAAAAAElFTkSuQmCC","department":"Software Engineering"}]}}


# GET /api/settings

+ Request

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR



+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 59

    + Body

            {"successful":true,"code":200,"response":{"name":"Stucial Tester","username":"demo","email":"demo@stucial.com","phone":"05436456686","department_id":185,"notification_messages":true,"notification_friendship":true,"notification_announcements":true,"privacy_private":true}}


# POST /api/settings

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "name": "Stucial Tester",
                "username": "demo",
                "email": "demo@stucial.com",
                "phone": "05436456686",
                "department_id": 185,
                "notification_messages": true,
                "notification_friendship": true,
                "notification_announcements": true,
                "privacy_private": true
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 59

    + Body

            {"successful":true,"code":200,"response":[]}


# POST /api/report

+ Request (application/json; charset=utf-8)

    + Headers

            Authorization: Bearer EVgZuoLowEUHYRI7CrqWs3jJRw2lgit0NN2rwJb9T8cWcyTSWoc5lNI2IwblbszR

    + Body

            {
                "type": 1,
                "id": "1",
                "description": "Because he is the best!"
            }

+ Response 200 (application/json)

    + Headers

            X-Powered-By: PHP/7.2.0
            Cache-Control: no-cache, private
            Vary: Authorization
            X-RateLimit-Limit: 60
            X-RateLimit-Remaining: 48

    + Body

            {"successful":true,"code":200,"response":[]}


