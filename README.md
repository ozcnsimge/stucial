# Stucial

## Availability
Currently, only available for Ege University's and Izmir University of Economics' students.

## What is Stucial?
```
Üniversite hayatın artık eskisi gibi olmayacak!
Kampüs yaşamındaki tüm iletişim ve stalk eksikliğine çözümü Stucial ile bulacaksın..
Stucial ile;
- Ders programına kolayca erişebilecek
- Arkadaşlarının ders programlarını takip edebilecek
- Seninle aynı derste hatta derslikte olan kişileri görebilecek ve sınıf grubundan mesajlaşabilecek
- Ders aralarında başka kimlerin boşta olduğunu görebilecek
- Üniversitendeki etkinliklerden ve o etkinliğe kimlerin katıldığından haberdar olabilecek, hatta katılımcılar ile iletişime geçebilecek
- Üyesi olduğun öğrenci kulüplerini takip edebilecek ve kulüp içi sohbet edebilecek
- Ders içeriklerine ve notlarına kolayca ulaşabilecek

Bu liste yaratıcılığınıza bağlı olarak uzar da gider..
Kısaca söyleyelim; stalk’un ve sohbetin dibine vuracaksınız..

E hadi ne duruyorsun, seni bekliyoruz!
```
```
Your university life will not be same anymore!
You will find the solution to the lack of communication and stalk in your campus life with Stucial..
CURRENTLY, only available for Izmir University of Economics..

With Stucial;
- You can easily access to the course schedule
- You will be able to follow your friends' schedule
- You can see the people in your classroom, and you can message with them
- You can see who is also idle in your free times
- You will be aware of the events in your university and see who is participating them, and even communicate with the participants
- You can follow student clubs which you are a member and chat within the club
- Easily access to the course contents and notes

This list expands depending on your creativity ..
You will be on the top level of communication and stalk.

So, why are you still looking, we are waiting for you!
```

## Download
[Available on App Store](https://itunes.apple.com/us/app/stucial/id1370668213)

[Available on Google Play](https://play.google.com/store/apps/details?id=com.stucial)

## On Press
1. http://www.haberturk.com/izmir-haberleri/60641314-ogrencilerin-yeni-paylasim-agi-geliyor
2. https://www.sabah.com.tr/egeli/2018/05/08/izmirli-ogrenciler-paylasim-agi-yapti
3. http://www.milliyet.com.tr/ogrenciler-kendilerine-ozel-ege-2664158/
4. http://www.egetelgraf.com/ogrencilerin-yeni-paylasim-agi-geliyor/
5. http://www.ieu.edu.tr/tr/news/type/read/id/5526
6. http://www.ieu.edu.tr/en/news/type/read/id/5526
7. http://www.iha.com.tr/izmir-haberleri/ogrencilerin-yeni-paylasim-agi-geliyor-izmir-2004617/
8. http://www.nhaworldnews.com/izmir/ogrencilerin-yeni-paylasim-agi-geliyor-h1723.html
9. https://www.sondakikhaber.com/ogrencilerin-yeni-paylasim-agi-geliyor/
10. http://www.sanalbasin.com/ogrenciler-kendi-sosyal-aginda-bulusacak-24947127
11. https://www.malatyagercek.com/izmir/ogrencilerin-yeni-paylasim-agi-geliyor-h1795893.html
12. http://www.habersitesi.com/ogrencilerin-yeni-paylasim-agi-geliyor-167336h.htm
13. http://www.kayseritempo.org/ogrencilerin-yeni-paylasim-agi-geliyor-381840.html
14. https://www.samsungazetesi.com/izmir/ogrencilerin-yeni-paylasim-agi-geliyor-h1046248.html
15. https://www.bolgegundem.com/ogrencilerin-yeni-paylasim-agi-geliyor-438257h.htm
16. http://www.habergazetesi.com.tr/haber/5192939/ogrencilerin-yeni-paylasim-agi-geliyor
17. https://www.haberler.com/ogrencilerin-yeni-paylasim-agi-geliyor-10824330-haberi/
18. http://www.sanalbasin.com/ogrencilerin-yeni-paylasim-agi-geliyor-24949536/
19. http://www.aydinhedef.com.tr/ege/07/05/2018/ogrencilerin-yeni-paylasim-agi-geliyor/galeri/1
20. http://www.aydinhedef.com.tr/ege/07/05/2018/ogrencilerin-yeni-paylasim-agi-geliyor/galeri/2
21. http://www.okulnews.com/teknoloji/ogrencilerin-yeni-paylasim-agi-geliyor-h211.html
22. http://izmirhaber.info/haber/ogrencilerin-yeni-paylasim-agi-geliyor-4316685.html
