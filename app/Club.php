<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Identicon;

class Club extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description', 'photo', 'chairman_id', 'university_id', 'message_group'];

    public function chairman(){
		return $this->belongsTo('App\Student', 'chairman_id');
    }

    public function university(){
		return $this->belongsTo('App\University', 'university_id');
    }

    public function messageGroup(){
		return $this->belongsTo('App\MessageGroup', 'message_group');
    }

    public function members(){
        return $this->belongsToMany('App\Student', 'club_members', 'club_id', 'student_id')->whereNull('club_members.deleted_at');
    }

	public function photo($size = 'medium'){
		if($this->photo)
			return url('images/club/profile/'.$size.'/'.$this->photo.'.jpg');
		else
			return Identicon::getImageDataUri($this->name);
	}
}
