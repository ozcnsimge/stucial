<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'content', 'post_id', 'student_id'
	];

	/**
	 * Soft Delete Date.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];

	public function post(){
		return $this->belongsTo('App\Post', 'post_id');
	}

	public function student(){
		return $this->belongsTo('App\User', 'student_id');
	}
}
