<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Course extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name', 'university_id'];

    public function university(){
		return $this->belongsTo('App\University', 'university_id');
    }

    public function sections(){
		return $this->hasMany('App\Section', 'course_id');
    }

    public function files(){
		return $this->hasMany('App\CourseFile', 'course_id');
    }

    public function students(){
        $sections = $this->sections->pluck('id')->toArray();
        $enrollments = CourseEnrollment::whereIn('section_id', $sections)->pluck('student_id')->toArray();
        $students = Student::whereIn('id', $enrollments)->get();
        return $students;
    }
}
