<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CourseComment extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['comment', 'student_id', 'course_id'];

    public function student(){
		return $this->belongsTo('App\Student', 'student_id');
    }

    public function course(){
		return $this->belongsTo('App\Course', 'course_id');
    }
}
