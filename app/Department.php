<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function students(){
		return $this->hasMany('App\Student', 'department_id');
    }

    public function short(){
        $words = explode(" ", $this->name);
        $acronym = "";

        foreach ($words as $w) {
            if(isset($w[0]) && ctype_upper($w[0]))
                $acronym .= $w[0];
        }

        return strtoupper($acronym);
    }
}
