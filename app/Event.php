<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Identicon;

class Event extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'place', 'description', 'photo', 'start', 'end', 'created_by', 'university_id', 'message_group'];


	public function photo($size = 'small'){
		if($this->photo)
			return url('images/event/'.$size.'/'.$this->photo.'.jpg');
		else
			return Identicon::getImageDataUri($this->name);
	}

    public function attendees(){
        return $this->belongsToMany('App\Student', 'event_attendees', 'event_id', 'student_id')->whereNull('event_attendees.deleted_at');
    }
}
