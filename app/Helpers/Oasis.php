<?php

namespace App\Helpers;

use Curl;

class Oasis {

	/**
	 *
	 * @return bool
	 */
	public static function login()
	{
		$response = Curl::to('https://odin-oasis.izmirekonomi.edu.tr/oasis/login.php')
			->setCookieFile(__DIR__.'/../../cookies.txt')
	        ->withData(array(
				'userid' => env('OASIS_ID'),
				'password' => env('OASIS_PASSWORD'),
			))
	        ->post();

		$response = Curl::to('https://odin-oasis.izmirekonomi.edu.tr/oasis/login.php')
			->setCookieFile(__DIR__.'/../../cookies.txt')
	        ->withData( array(
				'ssno' => env('OASIS_PIN'),
				'ltype' => '2',
			))
	        ->post();

		return isset($response);
    }


	/**
	 * Doğrulama
	 *
	 * @param int $no
	 * @return bool
	 */
	public static function verify($no)
	{
		// 11 karakterden oluşmalıdır
		if (strlen($no) != 11)
			return false;

		// 20 ile başlamalı
		if(substr($no, 0, 2) != '20')
			return false;

		return true;
    }

	/**
	 * API üzerinden öğrenci numarası veri doğruluk kontrolü
	 *
	 * @param int $no
	 * @param string $name
	 * @return bool
	 */
	public static function validate($no, $name)
	{
		if(!self::verify($no))
			return false;
		return true;

		self::login();

		$response = Curl::to('https://odin-oasis.izmirekonomi.edu.tr/oasis/student/messages/private_messages/search.php')
			->setCookieFile(__DIR__.'/../../cookies.txt')
	        ->withData( array(
				'value' => $no
			))
	        ->post();

		$response_name = explode('</span>', $response);
		$response_name = explode('(', $response_name[1]);
		$response_name = mb_strtoupper(trim($response_name[0]), "UTF-8");

		$name = mb_strtoupper(trim($name), "UTF-8");

		return ($name != "" && $response_name == $name);
	}
}
