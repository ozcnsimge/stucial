<?php

namespace App\Helpers;

use Curl;

class Push {

	/**
	 *
	 * @return bool
	 */
	public static function send($to, $title, $body, $data)
	{
		$data = array(
			'to' => $to,
			'title' => $title,
			'body' => $body,
			'sound' => 'default',
			'data' => $data
		);

		$response = Curl::to('https://exp.host/--/api/v2/push/send')
	        ->withData($data)
            ->asJson( true )
	        ->post();

		return $response;
    }
}
