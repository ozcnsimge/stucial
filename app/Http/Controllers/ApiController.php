<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Hash;
use Push;
use Image;
use Oasis;
use Storage;
use Validator;
use Carbon\Carbon;

use App\Club;
use App\Event;
use App\Report;
use App\Course;
use App\Section;
use App\Student;
use App\Department;
use App\University;
use App\Announcement;
use App\Message;
use App\MessageGroup;
use App\StudentSession;
use App\Post;

use Illuminate\Http\Request;

class ApiController extends Controller
{

	public static function response($code = 200, $response = []){
		return array(
			'successful' => $code == 200,
			'code' => $code,
			'response' => $response
		);
	}

	public function registerData(Request $request){
		$universities = University::select('id', 'name')
			->orderBy('name', 'asc')
			->get();

		$auth = Auth::guard('api')->user();
		$university_id = intval($request->get('university'));
		if($auth)
			$university = $auth->university;
		else {
			if($university_id == 0)
				$university_id = 1;
			$university = University::find($university_id);
		}

		$departments = [];
		if(isset($university))
			$departments = $university->departments()->select('id', 'name')
				->orderBy('name', 'asc')
				->get();

		$response = self::response(200, array(
				'universities' => $universities,
				'departments' => $departments
		));

		return response()->json($response);
	}

	public function register(Request $request){
		$validator = Validator::make($request->all(), [
			'student_id' => 'required|max:255',
			'name' => 'required|max:255',
			'university' => 'required|integer',
			'department' => 'required|integer',
			'username' => 'required|max:255',
			'password' => 'required|min:6',
		]);

		if ($validator->fails()) {
			$response = self::response(400);
			return response()->json($response);
		}

		if(Student::where('username', $request->get('username'))->exists()){
			$response = self::response(406);
			return response()->json($response);
		}

		if($request->get('university') == 1 && !Oasis::validate($request->get('student_id'), $request->get('name'))){
			$response = self::response(407);
			return response()->json($response);
		}

		$department = Department::find($request->get('department'));
		if(!$department) return response()->json(self::response(404));

		$student = Student::create([
			'name' => $request->get('name'),
			'student_id' => $request->get('student_id'),
			'username' => $request->get('username'),
			'password' => bcrypt($request->get('password')),
			'university_id' => $request->get('university'),
			'department_id' => $request->get('department')
		]);

		if($department->message_group)
			$student->participate($department->message_group);

		$response = self::response(200, array(
			'id' => $student->id,
			'name' => $student->name,
			'username' => $student->username,
			// 'photo' => $student->photo(),
			'email' => $student->email,
			'phone' => $student->phone,
			'university_id' => $student->university_id,
			'university' => $student->university->name,
			'department_id' => $student->department_id,
			'department' => $student->department->name,
			'token' => StudentSession::generate($student, $request->get('push_token'))
		));

		$notification_text = Student::count().". kayıt: ".$student->name;
		Student::find(1)->notify('Yeni Kayıt',
			$notification_text,
			[
				'type' => 'profile',
				'id' => $student->id,
				'title' => $student->name,
				'text' => $notification_text
			]
		);

		return response()->json($response);
	}

	public function login(Request $request){

		$student = Student::where('username', $request->get('username'))->orWhere('phone', $request->get('username'))->first();
		if($student && Hash::check($request->get('password'), $student->password)){
			$response = self::response(200, array(
				'id' => $student->id,
				'name' => $student->name,
				'username' => $student->username,
				'photo' => $student->photo(),
				'email' => $student->email,
				'phone' => $student->phone,
				'university_id' => $student->university_id,
				'university' => $student->university->name,
				'department_id' => $student->department_id,
				'department' => $student->department->name,
				'token' => StudentSession::generate($student, $request->get('push_token'))
			));
			return response()->json($response);
		} else {
			$response = self::response(401);
		}
		return response()->json($response);
	}

	public function logout(Request $request){
		$destroy = StudentSession::destroy($request->bearerToken());

		$response = self::response($destroy ? 200:401);

		return response()->json($response);
	}

	public function getSettings(Request $request){

		$auth = Auth::guard('api')->user();
		if(!$auth)
			return response()->json(self::response(401));

		$settings = [
			'name' => $auth->name,
			'username' => $auth->username,
			'email' => $auth->email,
			'phone' => $auth->phone,
			'department_id' => $auth->department_id,
			'notification_messages' => $auth->notification_messages==1,
			'notification_friendship' => $auth->notification_friendship==1,
			'notification_announcements' => $auth->notification_announcements==1,
			'privacy_private' => $auth->privacy_private==1,
		];

		$response = self::response(200, $settings);
		return response()->json($response);
	}

	public function setSettings(Request $request){

		$auth = Auth::guard('api')->user();
		if(!$auth)
			return response()->json(self::response(401));

		$validator = Validator::make($request->all(), [
			'name' => 'required|max:255',
			'department_id' => 'required|integer',
			'username' => 'required|max:255',
			'email' => 'email|nullable',
		]);

		if ($validator->fails()) {
			$response = self::response(400);
			return response()->json($response);
		}

		if(Student::where('username', $request->get('username'))->where('id', '!=', $auth->id)->exists()){
			$response = self::response(406);
			return response()->json($response);
		}

		if($auth->department_id != $request->get('department_id')){
			$new_department = Department::find($request->get('department_id'));
			if(!$new_department) return response()->json(self::response(500));
			$auth->absence($auth->department->message_group);
			$auth->participate($new_department->message_group);
		}

		$auth->name = $request->get('name');
		$auth->username = $request->get('username');
		$auth->department_id = $request->get('department_id');
		$auth->email = $request->get('email');
		$auth->phone = $request->get('phone');
		$auth->notification_messages = $request->get('notification_messages');
		$auth->notification_friendship = $request->get('notification_friendship');
		$auth->notification_announcements = $request->get('notification_announcements');
		$auth->privacy_private = $request->get('privacy_private');
		$auth->save();

		$response = self::response(200);
		return response()->json($response);
	}

	public function friendRequests(){
		$student = Auth::guard('api')->user();

		$requests = [];
		foreach($student->getFriendRequests() as $pending_request){
			$requests[] = array(
				'id' => $pending_request->sender->id,
				'name' => $pending_request->sender->name,
				'photo' => $pending_request->sender->photo(),
				'university' => $pending_request->sender->university->name,
				'department' => $pending_request->sender->department->name,
				'mutual_friends' => $student->getMutualFriendsCount($pending_request->sender)
			);
		}

		$response = self::response(200, $requests);

		return response()->json($response);
	}

	public function friendSuggestions(){
		$student = Auth::guard('api')->user();

		$suggestions = $student->getFriendSuggestions(10);
		$suggests = [];
		foreach($suggestions as $suggest){
			$suggests[] = array(
				'id' => $suggest->id,
				'name' => $suggest->name,
				'photo' => $suggest->photo(),
				'university' => $suggest->university->name,
				'department' => $suggest->department->name,
				'mutual_friends' => $student->getMutualFriendsCount($suggest)
			);
		}

		$friends = $student->getFriends()->pluck('id')->toArray();
		$suggestions = Student::where('id', '!=', $student->id)->where('university_id', $student->university_id)->whereNotIn('id', $friends)->inRandomOrder()->limit(10)->get();
		foreach($suggestions as $suggest){
			$suggests[] = array(
				'id' => $suggest->id,
				'name' => $suggest->name,
				'photo' => $suggest->photo(),
				'university' => $suggest->university->name,
				'department' => $suggest->department->name,
				'mutual_friends' => $student->getMutualFriendsCount($suggest)
			);
		}

		$response = self::response(200, $suggests);

		return response()->json($response);
	}

	public function sendFriendRequest(Request $request){
		$student = Auth::guard('api')->user();
		$recipient = Student::find($request->get('student'));

		if(!$recipient || !$student){
			$response = self::response(404);
		} elseif($student == $recipient || $student->isBlockedBy($recipient)){
			$response = self::response(403);
	 	} else {
			$student->befriend($recipient);
			$notifiaction_text = $student->name.' sizi arkadaşı olarak ekledi.';
			$recipient->notify(
				'Arkadaşlık İsteği',
				$notifiaction_text,
				[
					'type' => 'friendship',
					'id' => $student->id,
					'title' => $student->name,
					'text' => $notifiaction_text
				]
			);
			$response = self::response();
		}

		return response()->json($response);
	}

	public function answerFriendRequest(Request $request){
		$student = Auth::guard('api')->user();
		$sender = Student::find($request->get('student'));

		if(!$sender || !$student){
			$response = self::response(404);
		} else {

			if($request->get('answer') == 'accept'){
				$_request = $student->acceptFriendRequest($sender);
				$notifiaction_text = $student->name.' arkadaşlık isteğinizi kabul etti.';
				$sender->notify(
					'Yeni Arkadaş!',
					$notifiaction_text,
					[
						'type' => 'profile',
						'id' => $student->id,
						'title' => $student->name,
						'text' => $notifiaction_text
					]
				);
			} else
				$_request = $student->denyFriendRequest($sender);

			$response = self::response($_request ? 200:404);

		}

		return response()->json($response);
	}

	public function removeFriend(Request $request){
		$student = Auth::guard('api')->user();
		$recipient = Student::find($request->get('student'));

		if(!$recipient || !$student){
			$response = self::response(404);
		} else {
			$student->unfriend($recipient);
			$response = self::response();
		}

		return response()->json($response);
	}

	public function blockFriend(Request $request){
		$student = Auth::guard('api')->user();
		$recipient = Student::find($request->get('student'));

		if(!$recipient || !$student){
			$response = self::response(404);
		} elseif($student == $recipient){
			$response = self::response(403);
	 	} else {
			$student->blockFriend($recipient);
			$response = self::response();
		}

		return response()->json($response);
	}

	public function unblockFriend(Request $request){
		$student = Auth::guard('api')->user();
		$recipient = Student::find($request->get('student'));

		if(!$recipient || !$student){
			$response = self::response(404);
		} elseif($student == $recipient){
			$response = self::response(403);
	 	} else {
			$student->unblockFriend($recipient);
			$response = self::response();
		}

		return response()->json($response);
	}

	public function schedule(){
		$student = Auth::guard('api')->user();
		if(!$student)
			$response = self::response(401);

		$schedule = $student->schedule();

		$response = self::response(200, $schedule);

		return response()->json($response);
	}

	public function clubs(){
		$student = Auth::guard('api')->user();
		if(!$student)
			$response = self::response(401);

		$clubs = [];
		foreach($student->university->clubs as $club){
			$clubs[] = [
				'id' => $club->id,
				'name' => $club->name,
				'photo' => $club->photo(),
				'description' => $club->description,
			];
		}

		$response = self::response(200, $clubs);

		return response()->json($response);
	}

	public function club($id){
		$student = Auth::guard('api')->user();
		if(!$student)
			return response()->json(self::response(401));

		$club = Club::find($id);

		$members = [];
		foreach($club->members as $member){
			$members[] = [
				'id' => $member->id,
				'name' => $member->name,
				'username' => $member->username,
				'photo' => $member->photo(),
				'department' => $member->department->name,
			];
		}

		$details = [
			'id' => $club->id,
			'name' => $club->name,
			'photo' => $club->photo(),
			'description' => $club->description,
			'university' => $club->university->name,
			'is_member' => $student->isMember($club),
		];

		$response = [
			'details' => $details,
			'members' => $members,
		];

		$response = self::response(200, $response);

		return response()->json($response);
	}

	public function departments(){
		$student = Auth::guard('api')->user();
		if(!$student)
			$response = self::response(401);

		$departments = [];
		foreach(Department::orderBy('name', 'asc')->get() as $department){
			$departments[] = [
				'id' => $department->id,
				'name' => $department->name,
				'short' => $department->short()
			];
		}

		$response = self::response(200, $departments);

		return response()->json($response);
	}

	public function department($id){
		$auth = Auth::guard('api')->user();
		if(!$auth)
			return response()->json(self::response(401));

		$department = Department::find($id);
		if(!$department)
			return response()->json(self::response(404));

		$students = [];
		foreach($department->students as $student){
			$students[] = [
				'id' => $student->id,
				'name' => $student->name,
				'username' => $student->username,
				'photo' => $student->photo(),
				'department' => $student->department->name,
			];
		}

		$details = [
			'id' => $department->id,
			'name' => $department->name,
			'university' => $auth->university->name,
			'short' => $department->short(),
			'is_member' => $department->id == $auth->department_id,
		];

		$response = [
			'details' => $details,
			'students' => $students,
		];

		$response = self::response(200, $response);

		return response()->json($response);
	}

	public function applyClub(Request $request){
		$student = Auth::guard('api')->user();
		$club = Club::find($request->get('club'));

		if(!$student || !$club){
			$response = self::response(404);
		} else {
			$response = self::response( $student->applyClub($club) ? 200 : 500 );
		}

		return response()->json($response);
	}

	public function leaveClub(Request $request){
		$student = Auth::guard('api')->user();
		$club = Club::find($request->get('club'));

		if(!$student || !$club){
			$response = self::response(404);
		} else {
			$response = self::response( $student->leaveClub($club) ? 200 : 500 );
		}

		return response()->json($response);
	}

	public function freeTime(Request $request){
		$student = Auth::guard('api')->user();
		if(!$student)
			$response = self::response(401);

		$_students = [];
		if($request->get('day'))
			$_students = $student->freeTimeFriends((int) $request->get('day'), $request->get('start'), $request->get('end'));

		$students = [];
		foreach($_students as $student){
			$students[] = [
				'id' => $student->id,
				'name' => $student->name,
				'username' => $student->username,
				'photo' => $student->photo(),
				'department' => $student->department->name,
			];
		}


		$response = self::response(200, $students);

		return response()->json($response);
	}

	public function section($id){
		$student = Auth::guard('api')->user();
		if(!$student)
			$response = self::response(401);

		$section = Section::find($id);
		if(!$section){
			$response = self::response(401);
			return response()->json($response);
		}

		$response = [
			'id' => $section->id,
			'course' => [
				'id' => $section->course->id,
				'code' => $section->course->code,
				'name' => $section->course->name,
			],
			'students' => $section->getStudents()
		];

		$response = self::response(200, $response);

		return response()->json($response);
	}

	public function course($id){
		$auth_student = Auth::guard('api')->user();
		if(!$auth_student)
			$response = self::response(401);

		$course = Course::find($id);
		if(!$course){
			$response = self::response(401);
			return response()->json($response);
		}

		$students = [];
		foreach($course->students() as $student){
			$students[] = [
				'id' => $student->id,
				'name' => $student->name,
				'username' => $student->username,
				'photo' => $student->photo(),
				'department' => $student->department->name,
			];
		}

		$sections = [];
		foreach($course->sections as $section){
			$times = [];
			foreach($section->times->sortBy('day') as $time){
				$times[] = [
					'day' => $time->day,
					'start' => date('H:i', strtotime($time->start)),
					'end' => date('H:i', strtotime($time->end)),
					'class' => $time->class,
				];
			}
			$sections[] = [ 'id' => $section->id, 'times' => $times ];
		}

		$details = [
			'id' => $course->id,
			'code' => $course->code,
			'name' => $course->name,
			'university' => $course->university->name,
			'is_enrolled' => $auth_student->isEnrolled($course, true),
		];

		$files = [];
		foreach($course->files->sortBy('name') as $file){
			$files[] = [
				'id' => $file->id,
				'name' => $file->name,
				'url' => url(Storage::url($file->file))
			];
		}

		$response = [
			'details' => $details,
			'sections' => $sections,
			'students' => $students,
			'files' => $files,
		];

		$response = self::response(200, $response);

		return response()->json($response);
	}

	public function enrollSection(Request $request){
		$student = Auth::guard('api')->user();
		$section = Section::find($request->get('section'));

		if(!$student || !$section){
			$response = self::response(404);
		} else {
			$response = self::response( $student->enroll($section) ? 200 : 500 );
		}

		return response()->json($response);
	}

	public function disenrollSection(Request $request){
		$student = Auth::guard('api')->user();
		$course = Course::find($request->get('course'));

		if(!$student || !$course){
			$response = self::response(404);
		} else {
			$response = self::response( $student->disenroll($course) ? 200 : 500 );
		}

		return response()->json($response);
	}

	public function courseAddFile(Request $request, $id){
		$student = Auth::guard('api')->user();
		if(!$student)
			return response()->json(self::response(401));
		$course = Course::find($id);
		if(!$course)
			return response()->json(self::response(404));

		$validator = Validator::make($request->all(), [
			'file' => 'required|file|mimes:pdf|mimetypes:application/pdf'
		]);

		if ($validator->fails()) {
			$response = self::response(400);
			return response()->json($response);
		}

		$file = $request->file('file');

		$_file = Storage::disk('public')->put('docs', $request->file('file'));

		if($_file){
			$course_file = \App\CourseFile::create([
				'name' => $request->get('name'),
				'file' => $_file,
				'course_id' => $course->id,
				'added_by' => $student->id
			]);
		}

		$response = self::response( $course_file ? 200 : 500 );

		return response()->json($response);
	}

	public function profile($id){
		$auth = Auth::guard('api')->user();
		if(!$auth)
			return response()->json(self::response(401));

		$student = Student::where('id', $id)->orWhere('username', $id)->first();
		if(!$student || $auth->isBlockedBy($student))
			return response()->json(self::response(404));

		$friendship = 0;
		if($auth->isFriendWith($student))
			$friendship = 1;
		elseif($auth->hasFriendRequestFrom($student))
			$friendship = 2;
		elseif($student->hasFriendRequestFrom($auth))
			$friendship = 3;


		$profile = [
			'id' => $student->id,
			'name' => $student->name,
			'username' => $student->username,
			'photo' => $student->photo('medium'),
			'university_id' => $student->university_id,
			'university' => $student->university->name,
			'department_id' => $student->department_id,
			'department' => $student->department->name,
			'friendship' => $friendship,
			'blocked' => $student->isBlockedBy($auth),
			'private' => !$auth->can('show-schedule', $student),
			'counts' => [
				'friends' => $student->getFriendsCount(),
				'clubs' => $student->clubs->count(),
				'courses' => $student->enrollments->count(),
				'posts' => $student->posts->count()
			]
		];

		if($auth->id == $student->id){
			$profile['email'] = $student->email;
			$profile['phone'] = $student->phone;
		}

		$response = [
			'profile' => $profile,
			'schedule' => $auth->can('show-schedule', $student) ? $student->schedule() : [],
		];

		$response = self::response(200, $response);

		return response()->json($response);
	}

	public function profileFriends($id){
		$auth = Auth::guard('api')->user();
		if(!$auth)
			$response = self::response(401);

		$student = Student::find($id);
		if(!$student)
			$response = self::response(404);

		$friends = [];
		foreach($student->getFriends() as $friend){
			$friends[] = [
				'id' => $friend->id,
				'name' => $friend->name,
				'photo' => $friend->photo('medium'),
				'department' => $friend->department->name,
			];
		}

		$response = self::response(200, $friends);

		return response()->json($response);
	}

	public function profileClubs($id){
		$auth = Auth::guard('api')->user();
		if(!$auth)
			$response = self::response(401);

		$student = Student::find($id);
		if(!$student)
			$response = self::response(404);

		$clubs = [];
		foreach($student->clubs as $club){
			$clubs[] = [
				'id' => $club->id,
				'name' => $club->name,
				'photo' => $club->photo('medium'),
				'description' => $club->description,
			];
		}

		$response = self::response(200, $clubs);

		return response()->json($response);
	}

	public function search(Request $request){
		$auth = Auth::guard('api')->user();
		if(!$auth)
			$response = self::response(401);

		$query = $request->get('query');
		$query2 = substr_replace($query, ' ', 2, 0);
		$query3 = substr_replace($query, ' ', 2, 0);
		$students = Student::where('name', 'like', "%$query%")->where('university_id', $auth->university_id)->limit(5)->get();
		$clubs = Club::where('name', 'like', "%$query%")->where('university_id', $auth->university_id)->limit(5)->get();
		$courses = Course::where('university_id', $auth->university_id)
			->where(function($q) use ($query, $query2, $query3){
				$q	->where('code', 'like', "%$query%")
					->orWhere('code', 'like', "%$query2%")
					->orWhere('code', 'like', "%$query3%")
					->orWhere('name', 'like', "%$query%");
			})
			->limit(5)->get();

		$result = [];

		foreach($students as $student){
			if(!$auth->isBlockedBy($student))
				$result[] = [
					'id' => $student->id,
					'name' => $student->name,
					'photo' => $student->photo(),
					'sub' => $student->department->name."\n".$student->university->name,
					'type' => 'student'
				];
		}

		foreach($clubs as $club){
			$result[] = [
				'id' => $club->id,
				'name' => $club->name,
				'photo' => $club->photo(),
				'sub' => $club->description,
				'type' => 'club'
			];
		}

		foreach($courses as $course){
			$result[] = [
				'id' => $course->id,
				'name' => $course->code,
				'photo' => null,//$course->photo(),
				'sub' => $course->name."\n".$course->university->name,
				'type' => 'course'
			];
		}

		$response = self::response(200, $result);

		return response()->json($response);
	}

	public function changePhoto(Request $request){
		$validator = Validator::make($request->all(), [
			'photo' => 'required|image|mimes:jpeg,jpg,png'
		]);

		if ($validator->fails()) {
			$response = self::response(400);
			return response()->json($response);
		}

		$student = Auth::guard('api')->user();
		if(!$student)
			$response = self::response(401);

		$img = Image::make($request->file('photo')->getRealPath());
		$filename  = rand(1000,9999).'_'.rand(100000,999999).'_'.$student->id.'_'.substr(time(), 4, 6).'_'.rand(10000,99999);

		$img->backup();
		$img
			->fit(900, 900, function ($constraint) { $constraint->upsize(); })
			->save('images/student/profile/big/' . $filename . '.jpg');

		$img->reset();
		$img
			->fit(300, 300, function ($constraint) { $constraint->upsize(); })
			->save('images/student/profile/medium/' . $filename . '.jpg');

		$img->reset();
		$img
			->fit(100, 100, function ($constraint) { $constraint->upsize(); })
			->save('images/student/profile/small/' . $filename . '.jpg');

		$student->photo = $filename;
		$student->save();

		$response = self::response(200, $student->photo('small'));

		return response()->json($response);
	}


	public function sendMessage(Request $request){
		$student = Auth::guard('api')->user();
		if(!$student)
			$response = self::response(401);

		$message_group = MessageGroup::find($request->get('group'));
		if(!$message_group)
			return response()->json(self::response(404));

		$message = Message::create([
			'message' => $request->get('message'),
			'sender_id' => $student->id,
			'group_id' => $message_group->id,
		]);

		$participants = $message_group->unmuted_participants->where('id', '!=', $student->id)->where('notification_messages', 1)->pluck('id')->toArray();
		$tokens = StudentSession::whereIn('student_id', $participants)->whereNotNull('push_token')->pluck('push_token')->toArray();
		$title = $student->name.' @ '.$message_group->name;
		$data = ['type' => 'message', 'id' => $message_group->id, 'title' => $message_group->name, 'text' => $title.': '.$message->message];
		foreach($tokens as $to){
			Push::send($to, $title, $message->message, $data);
		}

		if($message){
			$response = self::response(200, ['id' => $message->id]);
		} else {
			$response = self::response(500);
		}
		return response()->json($response);
	}

	public function messageList(){
		$student = Auth::guard('api')->user();
		if(!$student)
			return response()->json(self::response(401));

		$messages = [];
		foreach($student->messageGroups as $message){
			$_last_message = $message->messages->last();
			if($_last_message)
				$last_message = [
					'id' => $_last_message->id,
					'message' => $_last_message->message,
					'time' => date('H:i', strtotime($_last_message->created_at)),
					'sender' => [
						'id' => $_last_message->sender_id ? $_last_message->sender->id : null,
						'name' => $_last_message->sender_id ? $_last_message->sender->name : null
					]
				];
			else
				$last_message = null;

			$messages[] = [
				'id' => $message->id,
				'title' => $message->name,
				'short' => $message->short(),
				'photo' => $message->photo(),
				'unseen' => $message->unseenCount($student),
				'muted' => $message->participation($student)->mute == 1,
				'type' => $message->type,
				'last' => $last_message,
			];
		}
		usort($messages, function($a, $b) {
			return $b['last']['id'] <=> $a['last']['id'];
		});

		$response = self::response(200, $messages);
		return response()->json($response);
	}

	public function messages(Request $request, $id){
		$student = Auth::guard('api')->user();
		if(!$student)
			return response()->json(self::response(401));

		$message_group = MessageGroup::find($id);
		if(!$message_group)
			return response()->json(self::response(404));

		$type = $request->get('type');

		$_messages = Message::where('group_id', $message_group->id);
		if($type == -1){
			$_messages = $_messages->where('id', '<', $request->get('first'));
		} elseif($type == 1){
			$_messages = $_messages->where('id', '>', $request->get('last'));
		}
		$_messages = $_messages->orderBy('id', 'desc')->limit(20)->get();

		if($type != -1 && count($_messages)>0){
			$message_group->setLastSeen($student, $_messages[0]);
		}

		$messages = [];
		foreach($_messages as $message){
			if($message->sender_id)
				$messages[] = [
		          '_id' => $message->id,
		          'text' => $message->message,
		          'createdAt' => Carbon::parse($message->created_at)->toAtomString(),
		          'user' => [
		            '_id' => $message->sender->id,
		            'name' => $message->sender->name,
		            'avatar' => $message->sender->photo('small'),
		          ],
				];
			else
				$messages[] = [
		          '_id' => $message->id,
		          'text' => $message->message,
		          'createdAt' => Carbon::parse($message->created_at)->toAtomString(),
		          'system' => true,
				];
		}

		$response = self::response(200, $messages);
		return response()->json($response);
	}

	public function messageInfo($id){
		$student = Auth::guard('api')->user();
		if(!$student)
			return response()->json(self::response(401));

		$message_group = MessageGroup::find($id);
		if(!$message_group)
			return response()->json(self::response(404));

		$participant = [];
		foreach($message_group->participants as $participant){
			$participants[] = [
				'id' => $participant->id,
				'name' => $participant->name,
				'photo' => $participant->photo('small'),
				'department' => $participant->department->name,
			];
		}

		$content = [];
		$_content = $message_group->content();
		if($_content){
			if($message_group->type == 2){
				$content = [
					'id' => $_content->id,
					'title' => $_content->course->name
				];
			} elseif($message_group->type == 1 || $message_group->type == 3) {
				$content = [
					'id' => $_content->id,
					'title' => $_content->name
				];
			}
		}

		$message = [
			'id' => $message_group->id,
			'title' => $message_group->name,
			'photo' => $message_group->photo(),
			'type' => $message_group->type,
			'muted' => $message_group->participation($student)->mute == 1,
			'content' => $content,
			'participants' => $participants
		];

		$response = self::response(200, $message);
		return response()->json($response);
	}

	public function messageMute(Request $request){
		$student = Auth::guard('api')->user();
		if(!$student)
			return response()->json(self::response(401));

		$message_group = MessageGroup::find($request->get('message'));
		if(!$message_group)
			return response()->json(self::response(404));

		if($request->get('mute'))
			$message_group->mute();
		else
			$message_group->unmute();

		$response = self::response();
		return response()->json($response);
	}

	public function messageLeave(Request $request){
		$student = Auth::guard('api')->user();
		if(!$student)
			return response()->json(self::response(401));

		$message_group = MessageGroup::find($request->get('message'));
		if(!$message_group)
			return response()->json(self::response(404));

		$response = self::response($student->absence($message_group->id) ? 200 : 500);
		return response()->json($response);
	}

	public function events($year, $month, $day){
		$auth = Auth::guard('api')->user();
		if(!$auth)
			return response()->json(self::response(401));

		$_events = Event::where('university_id', $auth->university_id)
			->where(DB::raw('YEAR(start)'), $year)
			->where(DB::raw('MONTH(start)'), $month)
			->orderBy('start')->get();

		$events = [];
		foreach($_events as $event){
			$events[] = [
				'id' => $event->id,
				'name' => $event->name,
				'place' => $event->place,
				'date' => date('Y-m-d', strtotime($event->start)),
				'photo' => $event->photo('small'),
				'attendees' => $event->attendees->count()
			];
		}

		$markeds = [];
		array_map(function($event) use (&$markeds) { $markeds[$event['date']] = ['marked' => true]; }, $events);

		$response = self::response(200, ['events' => $events, 'markeds' => $markeds]);

		return response()->json($response);
	}

	public function event($id){
		$auth = Auth::guard('api')->user();
		if(!$auth)
			return response()->json(self::response(401));

		$_event = Event::find($id);
		if(!$_event)
			return response()->json(self::response(404));

		if(date("m-d", strtotime($_event->start)) == date("m-d", strtotime($_event->end))){
			$date = date("d/m/Y\nH:i", strtotime($_event->start)).date(" - H:i", strtotime($_event->end));
		} else {
			$date = date("d/m/Y H:i\n", strtotime($_event->start)).date("d/m/Y H:i", strtotime($_event->end));
		}

		$event = [
			'id' => $_event->id,
			'name' => $_event->name,
			'place' => $_event->place,
			'description' => $_event->description,
			'date' => $date,
			'photo' => $_event->photo('big'),
			'is_attending' => $auth->isAttending($_event)
		];

		$attendees = [];
		foreach($_event->attendees as $attendee){
			$attendees[] = [
				'id' => $attendee->id,
				'name' => $attendee->name,
				'photo' => $attendee->photo('small'),
				'department' => $attendee->department->name,
			];
		}

		$response = self::response(200, ['event' => $event, 'attendees' => $attendees]);

		return response()->json($response);
	}

	public function createEvent(Request $request){
		$auth = Auth::guard('api')->user();
		if(!$auth)
			return response()->json(self::response(401));

		$validator = Validator::make($request->all(), [
			'name' => 'required|max:255',
			'place' => 'max:255',
			'start' => 'required',
			'end' => 'required',
			'photo' => 'image|nullable'
		]);

		if ($validator->fails()) {
			$response = self::response(400);
			return response()->json($response);
		}

		$event = Event::create([
			'name' => $request->get('name'),
			'place' => $request->get('place'),
			'description' => $request->get('description'),
			'start' => $request->get('start'),
			'end' => $request->get('end'),
			'created_by' => $auth->id,
			'university_id' => $auth->university_id
		]);

		if($request->file('photo')){
			$img = Image::make($request->file('photo')->getRealPath());
			$filename  = rand(1000,9999).'_'.rand(100000,999999).'_'.$event->id.'_'.substr(time(), 4, 6).'_'.rand(10000,99999);

			$img->backup();
			$img
				->fit(900, 900, function ($constraint) { $constraint->upsize(); })
				->save('images/event/big/' . $filename . '.jpg');

			$img->reset();
			$img
				->fit(100, 100, function ($constraint) { $constraint->upsize(); })
				->save('images/event/small/' . $filename . '.jpg');

			$event->photo = $filename;
			$event->save();
		}

		if($event){
			$message_group = MessageGroup::create([
				'name' => $event->name,
				'type' => 3
			]);
			$event->message_group = $message_group->id;
			$event->save();
		}

		$response = self::response($event ? 200 : 500);

		return response()->json($response);
	}

	public function attending(Request $request){
		$auth = Auth::guard('api')->user();
		$event = Event::find($request->get('event'));

		if(!$auth || !$event){
			$response = self::response(404);
		} else {

			if($request->get('type') == 'attend'){
				$_request = $auth->attend($event);
			} else
				$_request = $auth->absent($event);

			$response = self::response($_request ? 200:500);

		}

		return response()->json($response);
	}
	public function announcements(){
		$student = Auth::guard('api')->user();
		if(!$student)
			$response = self::response(401);

		$announcements = [];
		foreach($student->university->announcements->sortByDesc('created_at') as $announcement){
			$announcements[] = [
				'id' => $announcement->id,
				'title' => $announcement->title,
				'date' => date("d/m/Y", strtotime($announcement->created_at))
			];
		}

		$response = self::response(200, $announcements);

		return response()->json($response);
	}

	public function announcement($id){
		$student = Auth::guard('api')->user();
		if(!$student)
			return response()->json(self::response(401));

		$announcement = Announcement::find($id);
		if(!$student)
			return response()->json(self::response(404));

		$announcement = [
			'id' => $announcement->id,
			'title' => $announcement->title,
			'content' => $announcement->content,
			'date' => date("d/m/Y", strtotime($announcement->created_at))
		];

		$response = self::response(200, $announcement);

		return response()->json($response);
	}

	public function report(Request $request){
		$student = Auth::guard('api')->user();
		if(!$student)
			return response()->json(self::response(401));

		$report = Report::create([
			'content_id' => $request->get('id'),
			'type' => $request->get('type'),
			'description' => $request->get('description'),
			'reported_by' => $student->id
		]);

		$response = self::response($report ? 200 : 500);

		return response()->json($response);
	}

	public function check(){
		$student = Auth::guard('api')->user();
		if(!$student)
			return response()->json(self::response(401));

		$response = [
			'messages' => md5(json_encode($this->messageList()->original)),
			'requests' => md5(json_encode($this->friendRequests()->original)),
		];

		$response = self::response(200, $response);

		return response()->json($response);
	}

	public function hashtags(){
		$hashtags = [
			['key' => '1', 'hashtag' => 'itiraf', 'color' => 'pink'],
			['key' => '2', 'hashtag' => 'kayip', 'color' => '#333'],
			['key' => '3', 'hashtag' => 'ilan', 'color' => '#333'],
			['key' => '4', 'hashtag' => 'duyuru', 'color' => '#333'],
			['key' => '5', 'hashtag' => 'araniyor', 'color' => '#333'],
			['key' => '6', 'hashtag' => 'bulbeni', 'color' => 'pink'],
			['key' => '7', 'hashtag' => 'soru', 'color' => '#333'],
			['key' => '8', 'hashtag' => 'yurt', 'color' => '#333'],
			['key' => '9', 'hashtag' => 'ev', 'color' => '#333'],
			['key' => '10', 'hashtag' => 'evarkadasi', 'color' => '#333'],
		];

		$response = self::response(200, $hashtags);
		return response()->json($response);
	}

	public function post(Request $request){
		$validator = Validator::make($request->all(), [
			'content' => 'required',
			'image' => 'image|nullable'
		]);

		if ($validator->fails()) {
			$response = self::response(400);
			return response()->json($response);
		}

		$post = Post::post($request->get('content'), (boolean)($request->get('anonymous') == "true"));

		if($request->file('image')){
			$img = Image::make($request->file('image')->getRealPath());
			$filename  = rand(1000,9999).'_'.rand(100000,999999).'_'.$post->id.'_'.substr(time(), 4, 6).'_'.rand(10000,99999);

			$img
				->resize(900, null, function ($constraint) { $constraint->aspectRatio(); $constraint->upsize(); })
				->save('images/post/' . $filename . '.jpg');

			$post->image = $filename;
			$post->save();
		}

		$response = self::response($post ? 200 : 500);
		return response()->json($response);
	}

	public function posts(Request $request){
		$student = Auth::guard('api')->user();
		if(!$student)
			return response()->json(self::response(401));

		$type = $request->get('type');
		$hashtag = $request->get('hashtag');
		$_student = intval($request->get('student'));

		$posts = Post::where('university_id', $student->university_id);
		if($_student > 0){
			$posts = $posts->where('student_id', $_student)->where('anonymous', false);
		} elseif($hashtag){
			$posts = $posts->where(function($q) use ($hashtag){
				$q->where('content', 'like', "%#$hashtag %")
					->orWhere('content', 'like', "%#$hashtag");
			});
		}
		if($type == -1){
			$posts = $posts->where('id', '<', $request->get('first'));
		} elseif($type == 1){
			$posts = $posts->where('id', '>', $request->get('last'));
		}
		$posts = $posts->orderBy('id', 'desc')->limit(20)->get();

		$_posts = [];
		foreach($posts as $post){
			$_post = [
	          'id' => $post->id,
	          'content' => $post->content,
			  'image' => $post->image(),
	          'created_at' => Carbon::parse($post->created_at)->format('F d, Y H:i'),
			  'anonymous' => $post->anonymous==1,
			  'is_liked' => $post->isLiked(),
			  'likes' => $post->likes->count(),
			  'can_delete' => $student->can('delete-post', $post)
			];

			if($post->anonymous == 0)
				$_post['user'] = [
	 	            'id' => $post->student->id,
	 	            'name' => $post->student->name,
					'username' => $post->student->username,
	 	            'photo' => $post->student->photo('small'),
				];

			$_posts[] = $_post;
		}

		$response = self::response(200, $_posts);
		return response()->json($response);
	}

	public function likePost(Request $request){
		$post = Post::find($request->get('id'));

		$response = self::response($post->like() ? 200 : 500);
		return response()->json($response);
	}

	public function unlikePost(Request $request){
		$post = Post::find($request->get('id'));

		$response = self::response($post->unlike() ? 200 : 500);
		return response()->json($response);
	}

	public function deletePost(Request $request){
		$student = Auth::guard('api')->user();

		$post = Post::find($request->get('id'));

		if(!$post || $student->cannot('delete-post', $post))
    		return response()->json(['success' => false]);

		$response = self::response($post->delete() ? 200 : 500);
		return response()->json($response);
	}
}
