<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Image;
use Validator;

use App\Club;
use App\MessageGroup;

class ClubController extends Controller
{
	public function all()
	{
		$clubs = Club::orderBy('id', 'asc')->get();
		return view('management.club.list')->with('clubs', $clubs)->with('title', 'Kulüpler');
	}

	public function add()
	{
		return view('management.club.form')->with('title', 'Kulüp Ekle');
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
		  'name' => 'required|max:255',
		]);

		if ($validator->fails()) {
		  return back()->withErrors($validator)->withInput();
		}

		$club = new Club();
		$club->name = $request->get('name');
		$club->description = $request->get('description');
		$club->university_id = 1;
		$club->save();

		$group = MessageGroup::create(['name' => $club->name, 'type' => 1]);
		if($group){
			$club->message_group = $group->id;
			$club->save();
		}

		if($request->file('image')){
			$img = Image::make($request->file('image')->getRealPath());
			$filename  = rand(1000,9999).'_'.rand(100000,999999).'_'.$club->id.'_'.substr(time(), 4, 6).'_'.rand(10000,99999);
			$img->backup();
			$img
				->fit(900, 900, function ($constraint) { $constraint->upsize(); })
				->save('images/club/profile/big/' . $filename . '.jpg');

			$img->reset();
			$img
				->fit(300, 300, function ($constraint) { $constraint->upsize(); })
				->save('images/club/profile/medium/' . $filename . '.jpg');

			$img->reset();
			$img
				->fit(100, 100, function ($constraint) { $constraint->upsize(); })
				->save('images/club/profile/small/' . $filename . '.jpg');

			$club->photo = $filename;
			$club->save();
		}

		return back()->withMessage('Kulüp başarıyla eklendi.');

	}

	public function edit($id)
	{
		$club = Club::find($id);
		if(!$club) abort(404);

		return view('management.club.form')->with('club', $club)->with('title', 'Kulüp Düzenle');
	}

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
		  'name' => 'required|max:255',
		]);

		if ($validator->fails()) {
		  return back()->withErrors($validator)->withInput();
		}

		$club = Club::find($id);
		if(!$club) abort(404);

		$club->name = $request->get('name');
		$club->description = $request->get('description');
		$club->university_id = 1;

		$club->save();

		$message_group = $club->messageGroup;
		$message_group->name = $club->name;
		$message_group->save();

		if($request->file('image')){
			$img = Image::make($request->file('image')->getRealPath());
			$filename  = rand(1000,9999).'_'.rand(100000,999999).'_'.$club->id.'_'.substr(time(), 4, 6).'_'.rand(10000,99999);
			$img->backup();
			$img
				->fit(900, 900, function ($constraint) { $constraint->upsize(); })
				->save('images/club/profile/big/' . $filename . '.jpg');

			$img->reset();
			$img
				->fit(300, 300, function ($constraint) { $constraint->upsize(); })
				->save('images/club/profile/medium/' . $filename . '.jpg');

			$img->reset();
			$img
				->fit(100, 100, function ($constraint) { $constraint->upsize(); })
				->save('images/club/profile/small/' . $filename . '.jpg');

			$club->photo = $filename;
			$club->save();
		}

		return back()->withMessage('Kulüp başarıyla düzenlendi.');

	}

	public function delete($id)
	{
		$club = Club::find($id);
		$club->delete();

		return back()->withMessage('Kulüp başarıyla silindi.');
	}
}
