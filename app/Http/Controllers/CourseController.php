<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Course;
use App\Section;
use App\SectionTime;

class CourseController extends Controller
{
    public function import(Request $request){
        $days = array_flip(['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi']);
        if($request->hasFile('import_file')){
			$path = $request->file('import_file')->getRealPath();
			$data = file_get_contents($path);

            $courses = explode("\n", $data);
            $i = 0;
            $_courses = array();
            foreach($courses as $key => $course){
                $course = explode(';', $course);

                $parse = explode(' ', $course[2]);

                $day = $days[$parse[0]];
                $start = $parse[1];
                $end = $parse[3];
                $class = @$parse[4].' '.@$parse[5];

                if($course[0] && $course[1])
                    $_courses[$i] = array(
                        'code' => $course[0],
                        'name' => $course[1],
                        'sections' => array(
                            [
                                'day' => $day,
                                'start' => $start,
                                'end' => $end,
                                'class' => $class,
                            ]
                        )
                    );
                else{
                    if(isset($_courses[$i-1]['name']))
                        $_courses[$i-1]['sections'][] = [
                            'day' => $day,
                            'start' => $start,
                            'end' => $end,
                            'class' => $class,
                        ];
                    else
                        $_courses[$i-2]['sections'][] = [
                            'day' => $day,
                            'start' => $start,
                            'end' => $end,
                            'class' => $class,
                        ];
                }
                $i++;
            }

            // echo '<pre>';
            // print_r($_courses);
            // exit;

            foreach($_courses as $_course){
                $course = Course::updateOrCreate([
                    'code' => $_course['code']
                ], [
                    'name' => $_course['name'],
                    'university_id' => 1
                ]);

                $section = Section::create(['course_id' => $course->id]);

                foreach($_course['sections'] as $section_time){
                    SectionTime::create([
                        'day' => $section_time['day'],
                        'start' => $section_time['start'],
                        'end' => $section_time['end'],
                        'class' => $section_time['class'],
                        'section_id' => $section->id
                    ]);
                }
            }
		}
    }
}
