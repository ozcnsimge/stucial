<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Department;

use Validator;

class DepartmentController extends Controller
{
	public function all()
	{
		$departments = Department::orderBy('id', 'asc')->get();
		return view('management.department.list')->with('departments', $departments)->with('title', 'Bölümler');
	}

	public function add()
	{
		return view('management.department.form')->with('title', 'Bölüm Ekle');
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
		  'name' => 'required|max:255',
		]);

		if ($validator->fails()) {
		  return back()->withErrors($validator)->withInput();
		}

		$department = new Department();
		$department->name = trim($request->get('name'));
		$department->save();

		return back()->withMessage('Bölüm başarıyla eklendi.');

	}

	public function edit($id)
	{
		$department = Department::find($id);
		if(!$department) abort(404);

		return view('management.department.form')->with('department', $department)->with('title', 'Bölüm Düzenle');
	}

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
		  'name' => 'required|max:255',
		]);

		if ($validator->fails()) {
		  return back()->withErrors($validator)->withInput();
		}

		$department = Department::find($id);
		if(!$department) abort(404);

		$department->name = trim($request->get('name'));

		$department->save();

		return back()->withMessage('Bölüm başarıyla düzenlendi.');

	}

	public function delete($id)
	{
		$department = Department::find($id);
		$department->delete();

		return back()->withMessage('Bölüm başarıyla silindi.');
	}
}
