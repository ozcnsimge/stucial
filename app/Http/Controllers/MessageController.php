<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Club;
use App\Section;
use App\Department;
use App\MessageGroup;

class MessageController extends Controller
{

    public function createGroups(){
        $count = 0;
        foreach(Section::whereNull('message_group')->get() as $section){
            $group = MessageGroup::create(['name' => $section->course->code, 'type' => 2]);
            if($group){
                $section->message_group = $group->id;
                $section->save();
                $count++;
            }
        }

        foreach(Club::whereNull('message_group')->get() as $club){
            $group = MessageGroup::create(['name' => $club->name, 'type' => 1]);
            if($group){
                $club->message_group = $group->id;
                $club->save();
                $count++;
            }
        }

        foreach(Department::whereNull('message_group')->get() as $department){
            $group = MessageGroup::create(['name' => $department->name, 'type' => 4]);
            if($group){
                $department->message_group = $group->id;
                $department->save();

                foreach($department->students as $student){
                    $student->participate($group->id);
                }

                $count++;
            }
        }
        echo "$count created.";
    }
}
