<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Student;

use Carbon\Carbon;
use Validator;

class StudentController extends Controller
{

	public function all()
	{
		Student::find(1)->notify("Test", "Hello world!", 0);

		$students = Student::orderBy('id', 'asc')->get();
		return view('management.student.list')->with('students', $students)->with('title', 'Öğrenciler');
	}

	public function show($id)
	{
		$student = Student::find($id);
		if(!$student) abort(404);

		return view('management.student.show')->with('student', $student)->with('title', $student->name);
	}

	public function edit($id)
	{
		$student = Student::find($id);
		if(!$student) abort(404);

		return view('management.student.form')->with('student', $student)->with('title', 'Öğrenci Düzenle');
	}

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required|max:255',
			'university' => 'required|integer',
			'department' => 'required|integer',
			'student_id' => 'required|integer',
			'username' => 'required|max:255',
			'email' => 'required|email|max:255',
		]);

		if ($validator->fails()) {
			return back()->withErrors($validator)->withInput();
		}

		$student = Student::find($id);
		if(!$student) abort(404);

		$student->name = trim($request->get('name'));
		$student->university_id = intval($request->get('university'));
		$student->department_id = intval($request->get('department'));
		$student->student_id = trim($request->get('student_id'));
		$student->username = trim($request->get('username'));
		$student->email = trim($request->get('email'));
		if($request->get('password'))
			$student->password = bcrypt($request->get('password'));

		$student->save();

		return back()->withMessage('Öğrenci başarıyla düzenlendi.');

	}

	public function delete($id)
	{
		$student = Student::find($id);
		$student->delete();

		return back()->withMessage('Öğrenci başarıyla silindi.');
	}
}
