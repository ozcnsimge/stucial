<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\University;

use Validator;

class UniversityController extends Controller
{
	  public function all()
	  {
	    $universities = University::orderBy('id', 'asc')->get();
	    return view('management.university.list')->with('universities', $universities)->with('title', 'Üniversiteler');
	  }

	  public function add()
	  {
	    return view('management.university.form')->with('title', 'Üniversite Ekle');
	  }

	  public function store(Request $request)
	  {
	    $validator = Validator::make($request->all(), [
		  'name' => 'required|max:255',
		  'website' => 'string|max:255|nullable',
		  'phone' => 'max:255',
		  'address' => 'required',
		  'city' => 'required|max:255',
		  'coordinate_lat' => 'max:255',
		  'coordinate_lng' => 'max:255',
		  'mail_extension' => 'required|max:255',
	    ]);

	    if ($validator->fails()) {
	      return back()->withErrors($validator)->withInput();
	    }

	    $university = new University();
	    $university->name = trim($request->get('name'));
	    $university->website = trim($request->get('website'));
	    $university->phone = trim($request->get('phone'));
	    $university->address = trim($request->get('address'));
	    $university->city = trim($request->get('city'));
	    $university->coordinate_lat = trim($request->get('coordinate_lat'));
	    $university->coordinate_lng = trim($request->get('coordinate_lng'));
	    $university->mail_extension = trim($request->get('mail_extension'));
	    $university->save();

	    return back()->withMessage('Üniversite başarıyla eklendi.');

	  }

	  public function edit($id)
	  {
	    $university = University::find($id);
	    if(!$university) abort(404);

	    return view('management.university.form')->with('university', $university)->with('title', 'Üniversite Düzenle');
	  }

	  public function update(Request $request, $id)
	  {
	    $validator = Validator::make($request->all(), [
		  'name' => 'required|max:255',
		  'website' => 'string|max:255|nullable',
		  'phone' => 'max:255',
		  'address' => 'required',
		  'city' => 'required|max:255',
		  'coordinate_lat' => 'max:255',
		  'coordinate_lng' => 'max:255',
		  'mail_extension' => 'required|max:255',
	    ]);

	    if ($validator->fails()) {
	      return back()->withErrors($validator)->withInput();
	    }

	    $university = University::find($id);
	    if(!$university) abort(404);

		$university->name = trim($request->get('name'));
		$university->website = trim($request->get('website'));
		$university->phone = trim($request->get('phone'));
		$university->address = trim($request->get('address'));
		$university->city = trim($request->get('city'));
		$university->coordinate_lat = trim($request->get('coordinate_lat'));
		$university->coordinate_lng = trim($request->get('coordinate_lng'));
		$university->mail_extension = trim($request->get('mail_extension'));

	    $university->save();

	    return back()->withMessage('Üniversite başarıyla düzenlendi.');

	  }

	  public function delete($id)
	  {
	    $university = University::find($id);
	    $university->delete();

	    return back()->withMessage('Üniversite başarıyla silindi.');
	  }
}
