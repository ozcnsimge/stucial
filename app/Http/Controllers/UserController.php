<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use Carbon\Carbon;
use Validator;

class UserController extends Controller
{

	public function all()
	{
		$users = User::orderBy('id', 'asc')->get();
		return view('management.user.list')->with('users', $users)->with('title', 'Kullanıcılar');
	}

	public function add()
	{
		return view('management.user.form')->with('title', 'Kullanıcı Ekle');
	}

	public function store(Request $request)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'required|max:255',
			'group' => 'required|integer',
			'phone' => 'required|max:30',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|min:6|confirmed',
		]);

		if ($validator->fails()) {
			return back()->withErrors($validator)->withInput();
		}
		$user = new User();
		$user->group_id = intval($request->get('group'));
		$user->name = trim($request->get('name'));
		$user->email = trim($request->get('email'));
		$user->phone = trim($request->get('phone'));
		$user->password = bcrypt($request->get('password'));
		$user->api_token = str_random(64);
		$user->save();

		return back()->withMessage('Kullanıcı başarıyla eklendi.');

	}

	public function edit($id)
	{
		$user = User::find($id);
		if(!$user) abort(404);

		return view('management.user.form')->with('user', $user)->with('title', 'Kullanıcı Düzenle');
	}

	public function update(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
			'name' => 'max:255',
			'email' => 'email|max:255',
			'phone' => 'required',
			'group' => 'required|integer',
		]);

		if ($validator->fails()) {
			return back()->withErrors($validator)->withInput();
		}

		$user = User::find($id);
		if(!$user) abort(404);

		$user->group_id = intval($request->get('group'));
		$user->name = trim($request->get('name'));
		$user->email = trim($request->get('email'));
		$user->phone = trim($request->get('phone'));
		if($request->get('password'))
			$user->password = bcrypt($request->get('password'));

		$user->save();

		return back()->withMessage('Kullanıcı başarıyla düzenlendi.');

	}

	public function delete($id)
	{
		$user = User::find($id);
		$user->delete();

		return back()->withMessage('Kullanıcı başarıyla silindi.');
	}
}
