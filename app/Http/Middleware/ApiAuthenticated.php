<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

use App\StudentSession;

class ApiAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->bearerToken();
        $session = StudentSession::find($token);

        if (!$session) {
            return response()->json(['successful' => false, 'code' => 401, 'response' => []]);
        }

        Auth::guard('api')->loginUsingId($session->student_id);

        return $next($request);
    }
}
