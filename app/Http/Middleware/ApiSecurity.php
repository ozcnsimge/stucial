<?php

namespace App\Http\Middleware;

use Closure;

class ApiSecurity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if ($request->key!='NOFr6oWbduHcHz07HefP5EhW7pfuiPXo') {
          return response()->json(['error' => true, 'error_code' => 'What the fuck?']);
      }

      return $next($request);
    }
}
