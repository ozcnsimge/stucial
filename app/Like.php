<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'post_id', 'student_id'
	];

	public function post(){
		return $this->belongsTo('App\Post', 'post_id');
	}

	public function student(){
		return $this->belongsTo('App\Student', 'student_id');
	}
}
