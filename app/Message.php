<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['message', 'sender_id', 'group_id'];

    public function sender(){
        return $this->belongsTo('App\Student', 'sender_id');
    }

    public function group(){
		return $this->belongsTo('App\MessageGroup', 'group_id');
    }
}
