<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageGroup extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'type'];

    private $types = [ 0 => 'Private', 1 => 'Club', 2 => 'Session', 3 => 'Event', 4 => 'Department'];

    public function type(){
        return static::$types[$this->type];
    }

    public function messages(){
        return $this->hasMany('App\Message', 'group_id');
    }

    public function participation($student){
        return MessageGroupParticipant::where('group_id', $this->id)->where('student_id', $student->id)->first();
    }

    public function participants(){
        return $this->belongsToMany('App\Student', 'message_group_participants', 'group_id', 'student_id')->whereNull('message_group_participants.deleted_at');
    }

    public function unmuted_participants(){
        return $this->belongsToMany('App\Student', 'message_group_participants', 'group_id', 'student_id')->where('message_group_participants.mute', 0)->whereNull('message_group_participants.deleted_at');
    }

    public function content(){
        if($this->type == 1){
            return Club::where('message_group', $this->id)->first();
        } elseif($this->type == 2){
            return Section::where('message_group', $this->id)->first();
        } elseif($this->type == 3){
            return Event::where('message_group', $this->id)->first();
        } elseif($this->type == 4){
            return Department::where('message_group', $this->id)->first();
        } else {
            return null;
        }
    }

    public function mute(){
        $student = Student::auth();
        $participation = $this->participation($student);
        $participation->mute = true;
        $participation->save();
        return true;
    }

    public function unmute(){
        $student = Student::auth();
        $participation = $this->participation($student);
        $participation->mute = false;
        $participation->save();
        return true;
    }

	public function unseenCount($student){
		$participation = $this->participation($student);
		$unseens = Message::where('group_id', $this->id)->where('id', '>', $participation->last_seen)->whereNotNull('sender_id')->count();
		return $unseens;
	}

    public function setLastSeen($student, $message){
        $item = MessageGroupParticipant::where('group_id', $this->id)
            ->where('student_id', $student->id)
            ->first();
        $item->last_seen = $message->id;
        return $item->save();
    }

    public function photo(){
        if($this->type == 1 || $this->type == 3){
            $content = $this->content();
            if($content)
                return $content->photo();
        }
        return null;
    }

    public function short(){
        $words = explode(" ", $this->name);
        $acronym = "";

        foreach ($words as $w) {
            if(isset($w[0]) && ctype_upper($w[0]))
                $acronym .= $w[0];
        }

        return strtoupper($acronym);
    }
}
