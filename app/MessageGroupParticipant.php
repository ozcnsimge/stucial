<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MessageGroupParticipant extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['group_id', 'student_id', 'last_seen', 'mute'];

    public function group(){
		return $this->belongsTo('App\MessageGroup', 'group_id');
    }

    public function student(){
		return $this->belongsTo('App\Student', 'student_id');
    }
}
