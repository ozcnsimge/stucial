<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Auth;
use Setting;

class Post extends Model
{
	use SoftDeletes;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'content', 'image', 'student_id', 'university_id', 'anonymous', 'status'
	];

	/**
	 * Soft Delete Date.
	 *
	 * @var array
	 */
	protected $dates = ['deleted_at'];

	public function student(){
		return $this->belongsTo('App\Student', 'student_id');
	}

	public function university(){
		return $this->belongsTo('App\University', 'university_id');
	}

	public function comments(){
		return $this->hasMany('App\Comment', 'post_id');
	}

	public function likes(){
		return $this->hasMany('App\Like', 'post_id');
	}

	public function image(){
		if($this->image)
			return url('images/post/'.$this->image.'.jpg');
		return null;
	}

	public static function post($content, $anonymous){
		return Post::create([
			'content' => $content,
			'student_id' => Auth::guard('api')->user()->id,
			'anonymous' => $anonymous ? 1:0,
			'university_id' => Auth::guard('api')->user()->university_id
		]);
	}

	public function comment($content){
		return Comment::create([
			'content' => $content,
			'post_id' => $this->id,
			'student_id' => Auth::guard('api')->user()->id
		]);
	}

	public function like(){
		return Like::updateOrCreate([
			'post_id' => $this->id,
			'student_id' => Auth::guard('api')->user()->id
		]);
	}

	public function unlike(){
		return Like::where([
			'post_id' => $this->id,
			'student_id' => Auth::guard('api')->user()->id
		])->delete();
	}

	public function isLiked(){
		return Like::where([
			'post_id' => $this->id,
			'student_id' => Auth::guard('api')->user()->id
		])->exists();
	}
}
