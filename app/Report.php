<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['content_id', 'type', 'description', 'reported_by', 'status'];

    private $types = [ 1 => 'Student', 2 => 'Message', 3 => 'File', 4 => 'Post'];

    public function type(){
        return static::$types[$this->type];
    }

}
