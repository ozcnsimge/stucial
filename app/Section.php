<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Section extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['course_id', 'message_group'];

    public function course(){
		return $this->belongsTo('App\Course', 'course_id');
    }

    public function times(){
		return $this->hasMany('App\SectionTime', 'section_id');
    }

	public function students(){
        return $this->belongsToMany('App\Student', 'course_enrollments', 'section_id', 'student_id')->whereNull('course_enrollments.deleted_at');
    }

    public function getStudents(){
        $students = [];
        foreach($this->students as $student){
            $students[] = [
                'id' => $student->id,
                'name' => $student->name,
                'username' => $student->username,
                'photo' => $student->photo(),
                'department' => $student->department->name,
            ];
        }
        return $students;
    }
}
