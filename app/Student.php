<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Database\Eloquent\Model;
use Hootlex\Friendships\Traits\Friendable;
use Illuminate\Database\Eloquent\SoftDeletes;

use Hootlex\Friendships\Models\Friendship;

use DB;
use Auth;
use Push;
use Request;
use Identicon;
use Carbon\Carbon;

class Student extends Authenticatable
{
	use Friendable;
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'student_id', 'username', 'email', 'password', 'phone', 'photo', 'university_id', 'department_id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password'];

    public function university(){
		return $this->belongsTo('App\University', 'university_id');
    }

    public function department(){
		return $this->belongsTo('App\Department', 'department_id');
    }

    public function enrollments(){
		return $this->hasMany('App\CourseEnrollment', 'student_id');
    }

	public function clubs(){
        return $this->belongsToMany('App\Club', 'club_members', 'student_id', 'club_id')->whereNull('club_members.deleted_at');
    }
	public function sections(){
        return $this->belongsToMany('App\Section', 'course_enrollments', 'student_id', 'section_id')->whereNull('course_enrollments.deleted_at');
    }
	public function messageGroups(){
        return $this->belongsToMany('App\MessageGroup', 'message_group_participants', 'student_id', 'group_id')->whereNull('message_group_participants.deleted_at');
    }
	public function posts(){
        return $this->hasMany('App\Post', 'student_id');
    }

	public function isAdmin(){
		return $this->group_id == 10;
	}

	public static function auth(){
        $token = Request::bearerToken();

		$session = StudentSession::where('token', $token)->first();
		return $session->student;
	}

	public function participate($message_group){
		if(!$message_group)
			return false;

		$participation = MessageGroupParticipant::where(['group_id' => $message_group, 'student_id' => $this->id])->exists();
		if(!$participation){
			MessageGroupParticipant::create(['group_id' => $message_group, 'student_id' => $this->id]);
			Message::create(['message' => $this->name.' joined.','group_id' => $message_group]);
		}
		return true;
	}

	public function absence($message_group){
		if(!$message_group)
			return false;

		$participation = MessageGroupParticipant::where(['group_id' => $message_group, 'student_id' => $this->id]);
		if($participation->exists()){
			$participation->delete();
			Message::create(['message' => $this->name.' left.','group_id' => $message_group]);
		}
		return true;
	}

	public function applyClub($club){
		$apply = ClubMember::updateOrCreate(['student_id' => $this->id, 'club_id' => $club->id]);
		$this->participate($club->message_group);
		return $apply;
	}

	public function leaveClub($club){
		$leave = ClubMember::where(['student_id' => $this->id, 'club_id' => $club->id])->delete();
		$this->absence($club->message_group);
		return $leave;
	}

	public function isMember($club){
		return ClubMember::where('student_id', $this->id)->where('club_id', $club->id)->exists();
	}


	public function enroll($section){
		$enroll = CourseEnrollment::updateOrCreate(['student_id' => $this->id, 'section_id' => $section->id]);
		$this->participate($section->message_group);
		return $enroll;
	}

	public function disenroll($course){
		$sectionIds = $course->sections->pluck('id')->toArray();
		$sectionGroups = $course->sections->pluck('message_group')->toArray();
		CourseEnrollment::where('student_id', $this->id)->whereIn('section_id', $sectionIds)->delete();
		foreach($sectionGroups as $message_group)
			$this->absence($message_group);
		return true;
	}

	public function isEnrolled($section, $is_course = false){
		if($is_course){
			$sections = Section::where('course_id', $section->id)->pluck('id')->toArray();
			return CourseEnrollment::where('student_id', $this->id)->whereIn('section_id', $sections)->exists();
		} else {
			return CourseEnrollment::where('student_id', $this->id)->where('section_id', $section->id)->exists();
		}
	}

	public function attend($event){
		$attend = EventAttendee::updateOrCreate(['student_id' => $this->id, 'event_id' => $event->id]);
		$this->participate($event->message_group);
		return $attend;
	}

	public function absent($event){
		$absent = EventAttendee::where(['student_id' => $this->id, 'event_id' => $event->id])->delete();
		$this->absence($event->message_group);
		return $absent;
	}

	public function isAttending($event){
		return EventAttendee::where('student_id', $this->id)->where('event_id', $event->id)->exists();
	}

	public function photo($size = 'medium'){
		if($this->photo)
			return url('images/student/profile/'.$size.'/'.$this->photo.'.jpg');
		else
			return Identicon::getImageDataUri($this->id.$this->department_id.$this->name.$this->student_id);
	}

    public function schedule(){
		$schedule = [];
        foreach($this->enrollments as $enrollment){
			foreach($enrollment->section->times->sortBy('day') as $time){
				$schedule[$time->day][] = [
					'section' => $enrollment->section->id,
					'course' => [
							'code' => $enrollment->section->course->code,
							'name' => $enrollment->section->course->name,
						],
					'start' => date('H:i', strtotime($time->start)),
					'end' => date('H:i', strtotime($time->end)),
					'class' => trim($time->class),
					'students' => count($enrollment->section->getStudents())
				];
			}
		}
		foreach($schedule as $_day => &$day){
			usort($day, function($a, $b) {
			    return $a['start'] <=> $b['start'];
			});

			$temp = null;
			foreach($day as $section){
				// if(!isset($temp))
				// 	continue;

				$start = Carbon::parse($temp);
				$end = Carbon::parse($section['start']);
				if($start->diffInMinutes() > 10){
					$schedule[$_day][] = [
						'free' => true,
						'start' => $temp,
						'end' => $section['start'],
						'students' => Auth::guard('api')->user()->freeTimeFriends($_day, $temp, $section['start'])->count()
					];
				}
				$temp = $section['end'];
			}
			usort($day, function($a, $b) {
			    return $a['start'] <=> $b['start'];
			});
		}
		return $schedule;
    }

	public function notify($title, $body, $data = null){
		$tokens = StudentSession::where('student_id', $this->id)->whereNotNull('push_token')->pluck('push_token')->toArray();

		foreach($tokens as $to)
			Push::send($to, $title, $body, $data);

		return true;
	}


	public function freeTimeFriends($day, $start, $end){
		$start = Carbon::parse($start);
		$end = Carbon::parse($end);
		$friends = $this->getFriends()->pluck('id')->toArray();

		$times = [];
		$start->addMinutes(40);
		while($start < $end){
			$times[] = $start->format("H:i");
			$start->addHours(1);
		}

		$frees = [];
		foreach($times as $time){
			$befores = DB::select(
				DB::raw("SELECT DISTINCT course_enrollments.student_id AS id FROM course_enrollments
					INNER JOIN section_times ON course_enrollments.section_id = section_times.section_id
					WHERE section_times.day=$day
						AND section_times.end<'$time'
						AND course_enrollments.student_id IN(".($friends ? implode(',', $friends) : -1).")
						AND course_enrollments.deleted_at IS NULL
						AND section_times.deleted_at IS NULL
				")
			);
			$_befores = [];
			foreach($befores as $before){
				$_befores[] = $before->id;
			}

			$afters = DB::select(
				DB::raw("SELECT DISTINCT course_enrollments.student_id AS id FROM course_enrollments
					INNER JOIN section_times ON course_enrollments.section_id = section_times.section_id
					WHERE section_times.day=$day
						AND section_times.start>'$time'
						AND course_enrollments.student_id IN(".($friends ? implode(',', $friends) : -1).")
						AND course_enrollments.deleted_at IS NULL
						AND section_times.deleted_at IS NULL
				")
			);
			$_afters = [];
			foreach($afters as $after){
				$_afters[] = $after->id;
			}

			$possibles = array_intersect($_befores, $_afters);

			$unavailables = DB::select(
				DB::raw("SELECT DISTINCT course_enrollments.student_id AS id FROM course_enrollments
					INNER JOIN section_times ON course_enrollments.section_id = section_times.section_id
					WHERE section_times.day=$day
						AND section_times.start<'$time'
						AND section_times.end>'$time'
						AND course_enrollments.student_id IN(".($possibles ? implode(',', $possibles) : -1).")
						AND course_enrollments.deleted_at IS NULL
						AND section_times.deleted_at IS NULL
				")
			);

			$_unavailables = [];
			foreach($unavailables as $unavailable){
				$_unavailables[] = $unavailable->id;
			}

			$list = array_diff($possibles, $_unavailables);
			$frees = array_unique(array_merge($frees, $list));
		}

		return self::whereIn('id', $frees)->get();
	}

	/**
     * This method will not return Friendship models
     * It will return the 'friends' models. ex: App\User
     *
     * @param int $perPage Number
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getFriendSuggestions($perPage = 0)
    {
        return $this->getOrPaginate($this->friendSuggestionsQueryBuilder(), $perPage);
    }

	/**
      * Get the query builder for friendsOfFriends ('friend' model)
      *
      * @return \Illuminate\Database\Eloquent\Builder
      */
	private function friendSuggestionsQueryBuilder()
    {
        $friendships = $this->findFriendships()->get(['sender_id', 'recipient_id']);
        $recipients = $friendships->pluck('recipient_id')->all();
        $senders = $friendships->pluck('sender_id')->all();
        $friendIds = array_unique(array_merge($recipients, $senders));
        $suggestions = Friendship::where('status', 1)
                            ->where(function ($query) use ($friendIds) {
                                $query->where(function ($q) use ($friendIds) {
                                    $q->where('recipient_id', '!=', $this->id)->whereIn('sender_id', $friendIds);
                                })->orWhere(function ($q) use ($friendIds) {
                                    $q->where('sender_id', '!=', $this->id)->whereIn('recipient_id', $friendIds);
                                });
                            })
                            ->get(['sender_id', 'recipient_id']);
        $fofIds = array_unique(
            array_merge($suggestions->pluck('sender_id')->all(), $suggestions->pluck('recipient_id')->all())
        );

        return $this->whereIn('id', $fofIds)->whereNotIn('id', $friendIds);
    }
}
