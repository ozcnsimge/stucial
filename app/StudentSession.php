<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StudentSession extends Model
{
	protected $primaryKey = 'token'; // or null

    public $incrementing = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['token', 'student_id', 'push_token'];

	public function student(){
		return $this->belongsTo('App\Student', 'student_id');
	}

    public function auth(){

	}

	public static function generate($student, $push_token){
		if($push_token)
			self::where('push_token', $push_token)->delete();

		$session = self::create([
			'student_id' => $student->id,
			'token' => str_random(64),
			'push_token' => $push_token
		]);
		return $session->token;
	}

	public static function destroy($token){
		return self::find($token)->delete();
	}
}
