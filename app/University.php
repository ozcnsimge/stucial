<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class University extends Model
{
    use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'website', 'phone', 'address', 'city', 'coordinate_lat', 'coordinate_lng', 'mail_extension'];

    public function departments(){
		return $this->belongsToMany('App\Department', 'university_departments', 'university_id', 'department_id');
    }

    public function clubs(){
		return $this->hasMany('App\Club', 'university_id');
    }

    public function announcements(){
		return $this->hasMany('App\Announcement', 'university_id');
    }
}
