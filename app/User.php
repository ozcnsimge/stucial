<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
	use Notifiable;
	use SoftDeletes;

	/**
	* Soft Delete Date.
	*
	* @var array
	*/
	protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

	/**
	* User Groups
	*
	* @var array
	*/
	private static $groups = array(
		10 => 'Yönetici',
	);

	public function isAdmin(){
		return $this->group_id == 10;
	}

	public function group()
	{
		return self::$groups[$this->group_id];
	}

	public static function groups()
	{
		return self::$groups;
	}
}
