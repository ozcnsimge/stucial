<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('student_id');
			$table->string('username');
			$table->string('email')->nullable();
			$table->string('password');
			$table->string('phone', 30)->nullable();
            $table->string('photo', 44)->nullable();
			$table->integer('university_id')->unsigned();
			$table->integer('department_id')->unsigned();
			$table->boolean('notification_messages')->default(1);
			$table->boolean('notification_friendship')->default(1);
			$table->boolean('notification_announcements')->default(1);
            $table->boolean('privacy_private')->default(0);
            $table->tinyInteger('group_id')->default(1);
			$table->timestamps();
			$table->softDeletes();

            $table->foreign('university_id')->references('id')->on('universities');
            $table->foreign('department_id')->references('id')->on('departments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
