<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionTimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_times', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('day');
            $table->time('start');
            $table->time('end');
            $table->string('class');
            $table->integer('section_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('section_id')->references('id')->on('sections');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_times');
    }
}
