<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->longText('description')->nullable();
            $table->string('photo', 44)->nullable();
            $table->integer('chairman_id')->nullable()->unsigned();
            $table->integer('university_id')->unsigned();
            $table->integer('message_group')->nullable()->unsigned();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('chairman_id')->references('id')->on('students');
            $table->foreign('university_id')->references('id')->on('universities');
            $table->foreign('message_group')->references('id')->on('message_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clubs');
    }
}
