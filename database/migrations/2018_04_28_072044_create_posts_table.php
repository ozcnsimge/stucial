<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
			$table->increments('id');
			$table->longText('content');
            $table->string('image')->nullable();
			$table->integer('student_id')->unsigned();
			$table->integer('university_id')->unsigned();
			$table->tinyInteger('anonymous')->default(0);
			$table->integer('status')->default(1);
			$table->timestamps();
			$table->softDeletes();

            $table->foreign('student_id')->references('id')->on('students');
            $table->foreign('university_id')->references('id')->on('universities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
