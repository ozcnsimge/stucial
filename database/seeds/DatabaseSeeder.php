<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert([
			'name' => 'Furkan Kavlak',
			'email' => 'furkan@kavlak.me',
			'password' => bcrypt('1'),
			'group_id' => 10
		]);

		DB::table('universities')->insert(['name' => 'Izmir University of Economics', 'mail_extension' => 'std.ieu.edu.tr']);
		DB::table('departments')->insert(['name' => 'Software Engineering']);
		DB::table('departments')->insert(['name' => 'Industrial Engineering']);

		DB::table('university_departments')->insert(['university_id' => 1, 'department_id' => 1]);
		DB::table('university_departments')->insert(['university_id' => 1, 'department_id' => 2]);

		DB::table('students')->insert([
			'name' => 'Furkan Kavlak',
			'username' => 'furkankavlak',
			'student_id' => '20130601026',
			'email' => 'furkan@std.ieu.edu.tr',
			'password' => bcrypt('1'),
			'phone' => '905436456686',
			'university_id' => 1,
			'department_id' => 1,
		]);
		DB::table('students')->insert([
			'name' => 'Hatice Ertuğrul',
			'username' => 'haticeertugrul',
			'student_id' => '20130601021',
			'email' => 'hatice.ertugrul@std.ieu.edu.tr',
			'password' => bcrypt('1'),
			'phone' => '905436456682',
			'university_id' => 1,
			'department_id' => 1,
		]);

		DB::table('friendships')->insert(['sender_id' => 2, 'sender_type' => 'App\Student', 'recipient_id' => 1, 'recipient_type' => 'App\Student', 'status' => 1]);


		DB::table('clubs')->insert([
			'name' => 'Yazılım Kulübü',
			'description' => 'İzmir Ekonomi Üniversitesi Yazılım Kulübü yani IEU Software Community (ISC), 2017 yılı güz döneminde faaliyete geçmiş bir öğrenci topluluğudur. 2017-2018 öğretim yılında başta Mühendislik öğrencileri olmak üzere okulumuzun tüm bölümlerinden aktif üyelerimiz bulunmaktadır. Topluluğumuzun amacı bilişim sektörüne meraklı ve kendini bu alanda geliştirmek istenen öğrencilerle birlikte, yazılım dünyasına daha donanımlı insanlar yetiştirmeye yardımcı olurken sosyal etkinlikleri de göz ardı etmemek. Üniversite öğrencilerini iş hayatına hazırlamaya başlayıp, okul sonrası hayatlarında bağlantılarını kurmalarında yardımcı olmayı umuyoruz. Yaptığımız eğitim, seminer ve workshoplar sayesinde günümüz dünyasında aktif rol oynayan yazılımı herkesin hayatının bir parçası haline getirmek istiyoruz. Bunun yanında sosyal sorumluluk projelerini de bir arada yürütüyoruz.
Detaylı bilgi: ieusoftwarecommunity.com',
			'chairman_id' => 1,
			'university_id' => 1,
		]);

		DB::table('clubs')->insert([
			'name' => 'Oyun Geliştirme Takımı',
			'description' => 'Ülkemizde emekleme aşamasında olan oyun sektörü, bu tür organizasyonlar sayesinde ilerlemeye ve büyümeye devam ediyor. Bizim için bu çabalar çok önemli çünkü Türkiye\'de halen lisans düzeyinde \'Bilgisayar Oyunları\' eğitimi ilk ve tek olarak İzmir Ekonomi Üniversitesi\'nde veriliyor.',
			'chairman_id' => 1,
			'university_id' => 1,
		]);
		// INSERT INTO course_enrollments(student_id, section_id) VALUES (1,1515),(1,97),(1,1151),(1,660);
    }
}
