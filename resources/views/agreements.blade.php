@extends('layouts.master')

@php($title = "Kullanım Sözleşmesi ve Gizlilik Koşulları")

@section('style')
<style type="text/css">
	h1{
		font-size: 44px;
	}
	h5{
		margin-top: 30px;
		margin-bottom: 10px;
	}
	p{
		margin-top:0 !important;
	}
</style>
@endsection

@section('content')
<div class="advance-feature">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 wow fadeInLeft">
				<div class="feature-text">
					<div class="theme-title-two">
						<h1>Kullanım Sözleşmesi ve Gizlilik Koşulları</h1>
					</div>

					<h5>1. Kullanıcı Uygunluğu</h5>
					<p>a. Sisteme kaydolarak; bağlı olduğunuz yasalara göre sözleşme imzalama hakkına, yetkisine ve hukuki ehliyetine sahip ve 18 yaşın üzerinde olduğunuzu, bu sözleşmeyi okuduğunuzu, anladığınızı ve sözleşmede yazan şartlarla bağlı olduğunuzu kabul etmiş sayılırsınız.</p>

					<h5>2. Kurallar</h5>
					<p>a. Bireysel yada toplu sohbetler içerisinde aşağılayıcı konuşmak, hakaret etmek, propaganda yapmak, siyasi bir görüşü savunmak, siyaset yapmak, telif hakkına tabi içerikler paylaşmak, cinsel yada pornografik mesajlar ve içerikler göndermek ve bunlar gibi tüm toplumsal huzuru ve düzeni bozmaya yönelik hareketler/paylaşımlar yasaktır.</p>
					<p>b. Kullanıcılar dersler için içerik yükleyebilir fakat yükledikleri her içerikten kendileri sorumlu tutulacaktır. Bu hususta Madde 3.b. geçerlidir.</p>
					<p>c. Kullanıcılar sistem üzerinde etkinlik oluşturabilir ve oluşturdukları etkinliklerin içeriğinden tamamen kendileri sorumludur. Gerçek dışı/sahte veya siyasi etkinlik(ler) oluşturulması yasaktır. Bu hususta Madde 3.b. geçerlidir.</p>
					<p>d. Kullanıcılar öğrenci olmak zorunda ve kayıt esnasında yada sonradan beyan ettiği üniversitenin ilgili bölümünde okuyor olmalıdır.</p>
					<p>e. Kurallara uymayan kullanıcılar sistem üzerinden engellenebilir, üyelikleri silinebilir, sözleşme ihlali ile huzuru ve güveni bozmak, sisteme ve/veya sisteme dahil olan bireylere zarar vermek sebepleri ile hakkında suç duyurusunda bulunulabilir ve/veya haklarında dava açılabilir ve sebep oldukları maddi/manevi zararların tazmini talep edilebilir.</p>

					<h5>3. Sorumluluklar</h5>
					<p>a. Kayıt olan tüm kullanıcılar bilgilerinin doğruluğunu onaylamış kabul edilir ve oluşabilecek olumsuz durumlarda beyan ettiği bilgilerden ve doğruluğundan sadece kendileri sorumlu tutulacak, Stucial ve/veya bir başka yetkili kurum, kuruluş yada kişi sorumlu tutulmayacaktır.</p>
					<p>b. Kullanıcı, Stucial üzerinden yaptığı tüm paylaşımlar, sosyal aktiviteler, etkinlik oluşturma, dosya ve içerik paylaşımı, mesaj gönderimleri gibi sistem üzerinden gerçekleştirdiği bütün eylemler, işlemler, aktiviteler ve paylaşımlar üzerinde tüm mesuliyeti kabul eder, hiçbir koşul altında Stucial’ı, kurucularını veya çalışanlarını suçlayamaz, suça sorumlu gösteremez.</p>
					<p>c. Kullanıcılar paylaştıkları bilgilerden ve bu bilerileri kimlerle paylaşmak istediklerini gizlilik ayarlarında belirttiği kişilerle paylaşılmasından sorumludur. Paylaştıkları bilgiler neticesinde doğabilecek olumsuzluklardan tamamiyle kullanıcı sorumludur.</p>

					<h5>4. Gizlilik</h5>
					<p>a. Stucial bilgilerinizi hiçbir kurum, kuruluş ve kişiye satmayacağını garanti eder.</p>
					<p>b. E-mail, telefon numarası ve şifre bilgileri hariç paylaşmış olduğunuz tüm bilgiler diğer kullanıcılar tarafından görüntülenebilir.</p>
					<p>c. Ders programınız siz aksine bir gizlilik ayarı yapmadıkça tüm kullanıcılar tarafından görülebilir. Ders programınızı gizlemek ve sadece arkadaşlarınıza görünür yapmak için gizlilik ayarlarından ders programı gizliliğini aktif etmeniz gerekmektedir.</p>
					<p>d. Katılacağınızı belirttiğiniz etkinliklerden, öğrencisi olduğunuz eğitim kurumundan, okuduğunuz bölümden, aldığınız derslerden ve kayıtlı olduğunuz öğrenci kulüplerinden diğer kullanıcılar haberdar olabilir.</p>
					<p>e. Arkadaşlarınız sizlerle çakışan ders aralarında sizi kendi ders arası listesinde görebilir.</p>
					<p>f. Sizinle aynı dersi alan öğrenciler sizi sınıf listesinde görebilir.</p>

					<h5>5. Kayıt ve Güvenlik</h5>
					<p>a. Kullanıcı, doğru, eksiksiz ve güncel kayıt bilgilerini vermek zorundadır. Aksi halde bu Sözleşme ihlal edilmiş sayılacak ve Kullanıcı bilgilendirilmeksizin hesap kapatılabilecektir.</p>
					<p>b. Kullanıcı, site ve üçüncü taraf sitelerdeki şifre ve hesap güvenliğinden kendisi sorumludur. Aksi halde oluşacak veri kayıplarından ve güvenlik ihlallerinden veya donanım ve cihazların zarar görmesinden Firma sorumlu tutulamaz.</p>

					<h5>6. Değişiklikler</h5>
					<p>a. Stucial bu Kullanım Koşulları ve Gizlilik Sözleşmesini istediği zaman revize edebilir ve/veya güncelleyebilir.  Bu Kullanım Koşullarındaki herhangi bir değişiklikten sonra Web Sitesi veya Mobil Uygulamayı kullanmaya devam ettiğiniz takdirde, bu değişiklikleri kabul etmiş sayılırsınız.</p>
					<p>b. Web Sitesinin ve Mobil Uygulamanın tüm noktaları, kararı yalnızca Stucial’a ait olmak üzere ve haber vermeksizin değiştirilebilir, eklenebilir, silinebilir veya güncellenebilir.</p>
					<p>c. Stucial, Web Sitesi ve Mobil Uygulama üzerinden sağlanan ürün ve hizmetler için istediği zaman ve kararı yalnızca kendisine ait olmak üzere ücret isteyebilir veya bu ücretleri değiştirebilir.</p>
					<p>d. Stucial, istediği zaman ve kararı yalnızca kendisine ait olmak üzere diğer Stucial ürün ve hizmetleriyle ilgili genel uygulamalar oluşturabilir ve sınırlar koyabilir veya bunları değiştirebilir.</p>
					<p>e. Stucial, istediği zaman ve kararı yalnızca kendisine ait olmak üzere Web Sitesini ve Mobil Uygulamayı yayından kaldırabilir, kapatabilir veya yönlendirebilir.</p>

					<p>İşbu sözleşme 6 ana maddeden oluşmakta ve her kayıt olan kullanıcı tüm ana ve alt maddeleri ile birlikte sözleşmeyi okumuş, anlamış, kabul etmiş, ve anayasal düzeyde kabul etme, sorumluluk alma ve cezai ehliyete sahip sayılmaktadır.</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
