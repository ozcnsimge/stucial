@extends('layouts.auth')

@section('content')


      <form class="form-signin" role="form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <h2 class="form-signin-heading">Giriş Yap</h2>
        <div class="login-wrap">
          <div class="user-login-info">
            <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-mail" required autofocus>
            @if ($errors->has('email'))
                <div class="alert alert-block alert-danger fade in">
                    <strong>{{ $errors->first('email') }}</strong>
                </div>
            @endif

            <input id="password" type="password" class="form-control" name="password" placeholder="Şifre" required>
            @if ($errors->has('password'))
                <div class="alert alert-block alert-danger fade in">
                    <strong>{{ $errors->first('password') }}</strong>
                </div>
            @endif
          </div>
          <label class="checkbox">
              <input type="checkbox" name="remember"> Beni hatırla
              <span class="pull-right">
                  <a data-toggle="modal" href="{{ url('/admin/sifre/sifirla') }}"> Şifremi unuttum</a>

              </span>
          </label>

          <button class="btn btn-lg btn-warning btn-block" type="submit">Giriş</button>

        </div>

      </form>

@endsection
