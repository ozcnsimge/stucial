@extends('layouts.master')

@php($description = "Üniversite hayatın artık eskisi gibi olmayacak! Kampüs yaşamındaki tüm iletişim ve stalk eksikliğine çözümü Stucial ile bulacaksın..")

@section('content')
<!--
=====================================================
	Theme Main SLider
=====================================================
-->
<div id="theme-main-banner" class="banner-one">
	<div data-src="{{ asset('theme/images/home/slide-1.jpg') }}">
		<div class="camera_caption">
			<div class="main-container">
				<div class="container">
					<h1 class="wow fadeInUp animated">Stucial</h1>
					<p class="wow fadeInUp animated">Be stucial!</p>
					<a href="https://itunes.apple.com/us/app/stucial/id1370668213" class="tran3s wow fadeInLeft animated button-one" data-wow-delay="0.499s"><i class="fa fa-apple" aria-hidden="true"></i>App Store</a>
					<a href="https://play.google.com/store/apps/details?id=com.stucial" class="tran3s wow fadeInRight animated button-two" data-wow-delay="0.499s"><img src="{{ asset('theme/images/icon/2.png') }}" alt="">GooglePlay</a>
					<div class="image-wrapper wow fadeInUp animated" data-wow-delay="0.5s">
						<img src="{{ asset('theme/images/home/shape-4.png') }}" alt="">
					</div>
				</div> <!-- /.container -->
			</div> <!-- /.main-container -->
		</div> <!-- /.camera_caption -->
	</div>
</div> <!-- /#theme-main-banner -->

<div class="advance-feature">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12 wow fadeInLeft">
				<div class="feature-text">
					<div class="theme-title-two">
						<h1>Nedir bu Stucial?</h1>
					</div>
					<p>
						Üniversite hayatın artık eskisi gibi olmayacak!<br>
						Kampüs yaşamındaki tüm iletişim ve stalk eksikliğine çözümü Stucial ile bulacaksın..<br>
					</p>
					<p>
						Stucial ile;<br>
						- Ders programına kolayca erişebilecek<br>
						- Arkadaşlarının ders programlarını takip edebilecek<br>
						- Seninle aynı derste hatta derslikte olan kişileri görebilecek ve sınıf grubundan mesajlaşabilecek<br>
						- Ders aralarında başka kimlerin boşta olduğunu görebilecek<br>
						- Üniversitendeki etkinliklerden ve o etkinliğe kimlerin katıldığından haberdar olabilecek, hatta katılımcılar ile iletişime geçebilecek<br>
						- Üyesi olduğun öğrenci kulüplerini takip edebilecek ve kulüp içi sohbet edebilecek<br>
						- Ders içeriklerine ve notlarına kolayca ulaşabilecek<br>
						<br>
						Bu liste yaratıcılığınıza bağlı olarak uzar da gider..<br>
						Kısaca söyleyelim; stalk’un ve sohbetin dibine vuracaksınız..<br>
						<br>
						E hadi ne duruyorsun, seni bekliyoruz!
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

<!--
=====================================================
	App ScreenShot
=====================================================
-->
<div class="app-screenshot">
	<h2>Ekran Görüntüleri</h2>
	<div class="screenshot-container">
		<div class="phone-mockup"><img src="{{ asset('theme/images/home/mobile-mockup.png') }}" alt="" class=""></div>
		<div class="slider-row">
			<div class="screenshoot-slider">
				<div class="item"><img src="{{ asset('theme/images/screenshots/1.png') }}" alt=""></div>
				<div class="item"><img src="{{ asset('theme/images/screenshots/2.png') }}" alt=""></div>
				<div class="item"><img src="{{ asset('theme/images/screenshots/3.png') }}" alt=""></div>
				<div class="item"><img src="{{ asset('theme/images/screenshots/4.png') }}" alt=""></div>
				<div class="item"><img src="{{ asset('theme/images/screenshots/5.png') }}" alt=""></div>
				<div class="item"><img src="{{ asset('theme/images/screenshots/6.png') }}" alt=""></div>
			</div> <!-- /.screenshoot-slider -->
		</div> <!-- /.row -->
	</div> <!-- /.screenshot-container -->
</div> <!-- /.app-screenshot -->
@endsection
