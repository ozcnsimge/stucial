<!DOCTYPE html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="ThemeBucket">

    <meta http-equiv=”X-UA-Compatible” content=”IE=EmulateIE9”>
    <meta http-equiv=”X-UA-Compatible” content=”IE=9”>

    <link rel="shortcut icon" href="images/favicon.png">

    <title>{{ isset($title) ? $title.' - ':'' }}{{ config('app.name') }}</title>

    <!--Core CSS -->
    <link href="{{ asset('bucket/bs3/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bucket/js/jquery-ui/jquery-ui-1.10.1.custom.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bucket/css/bootstrap-reset.css') }}" rel="stylesheet">
    <link href="{{ asset('bucket/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ asset('bucket/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('bucket/css/style-responsive.css') }}" rel="stylesheet"/>
    <link href="{{ asset('bucket/css/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('bucket/css/orange-theme.css') }}" rel="stylesheet">

    @yield('style')

    <!-- Just for debugging purposes. Don't actually copy this line! -->
    <!--[if lt IE 9]>
    <script src="js/ie8-responsive-file-warning.js"></script><![endif]-->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<section id="container">
<!--header start-->
<header class="header fixed-top clearfix">
<!--logo start-->
<div class="brand">

    <a href="{{ url('management') }}" class="logo">
        {{ config('app.name') }}
    </a>
    <div class="sidebar-toggle-box">
        <div class="fa fa-bars"></div>
    </div>
</div>
<!--logo end-->

<div class="top-nav clearfix">
  <!--search & user info start-->
  <ul class="nav pull-right top-menu">
    <!-- user login dropdown start-->
    <li class="dropdown">
      <a data-toggle="dropdown" class="dropdown-toggle" href="#">
        <img alt="" src="{{ Auth::user()->photo ? Auth::user()->photo : '/images/pp-default.jpg' }}">
        <span class="username">{{ Auth::user()->name }}</span>
        <b class="caret"></b>
      </a>
      <ul class="dropdown-menu extended logout">
        <li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out"></i> Çıkış</a></li>
        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      </ul>
    </li>
    <!-- user login dropdown end -->
  </ul>
  <!--search & user info end-->
</div>
</header>
<!--header end-->
<!--sidebar start-->
@include('layouts.management.sidebar')
<!--sidebar end-->
<!--main content start-->
<section id="main-content">
	<section class="wrapper">
		@yield('content')
	</section>
</section>
<!--main content end-->
</section>
<!-- Placed js at the end of the document so the pages load faster -->
<!--Core js-->
<script src="{{ asset('bucket/js/jquery.js') }}"></script>
<script src="{{ asset('bucket/js/jquery-ui/jquery-ui-1.10.1.custom.min.js') }}"></script>
<script src="{{ asset('bucket/bs3/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('bucket/js/jquery.dcjqaccordion.2.7.js') }}"></script>
<script src="{{ asset('bucket/js/jquery.scrollTo.min.js') }}"></script>
<script src="{{ asset('bucket/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('bucket/js/jquery.nicescroll.js') }}"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/flot-chart/excanvas.min.js"></script><![endif]-->
<script src="{{ asset('bucket/js/skycons/skycons.js') }}"></script>
<script src="{{ asset('bucket/js/jquery.scrollTo/jquery.scrollTo.js') }}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="{{ asset('bucket/js/calendar/clndr.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
<script src="{{ asset('bucket/js/calendar/moment-2.2.1.js') }}"></script>
<script src="{{ asset('bucket/js/evnt.calendar.init.js') }}"></script>
<script src="{{ asset('bucket/js/jvector-map/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('bucket/js/jvector-map/jquery-jvectormap-us-lcc-en.js') }}"></script>
<script src="{{ asset('bucket/js/gauge/gauge.js') }}"></script>
<script src="{{ asset('bucket/js/scripts.js') }}"></script>


@yield('script')

</body>
</html>
