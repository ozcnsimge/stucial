<aside>
    <div id="sidebar" class="nav-collapse">
        <!-- sidebar menu start-->
        <div class="leftside-navigation">
            <ul class="sidebar-menu" id="nav-accordion">
                <li>
                    <a href="{{ url('management') }}" @if(Request::is('management')) class="active" @endif >
                        <i class="fa fa-home"></i>
                        <span>Anasayfa</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('management/user/list') }}" @if(Request::is('management/user*')) class="active" @endif >
                        <i class="fa fa-users"></i>
                        <span>Kullanıcılar</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('management/university/list') }}" @if(Request::is('management/university*')) class="active" @endif >
                        <i class="fa fa-building"></i>
                        <span>Üniversiteler</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('management/department/list') }}" @if(Request::is('management/department*')) class="active" @endif >
                        <i class="fa fa-book"></i>
                        <span>Bölümler</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('management/club/list') }}" @if(Request::is('management/club*')) class="active" @endif >
                        <i class="fa fa-book"></i>
                        <span>Kulüpler</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('management/student/list') }}" @if(Request::is('management/student*')) class="active" @endif >
                        <i class="fa fa-graduation-cap"></i>
                        <span>Öğrenciler</span>
                    </a>
                </li>
                <li>
                    <a href="{{ url('management/settings') }}" @if(Request::is('management/setting*')) class="active" @endif >
                        <i class="fa fa-cogs"></i>
                        <span>Ayarlar</span>
                    </a>
                </li>
            </ul>
          </div>
        <!-- sidebar menu end-->
    </div>
</aside>
