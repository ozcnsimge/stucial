<!DOCTYPE html>
<html lang="tr">
	<head>
		<meta charset="UTF-8">
		<!-- For IE -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- For Resposive Device -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
@if(isset($description))
	    <meta name="description" content="{{ $description }}">
@endif

		<title>{{ isset($title) ? $title.' - ':'' }}Stucial - be stucial!</title>

		<!-- Favicon -->
		<link rel="icon" type="image/png" sizes="56x56" href="{{ asset('theme/images/fav-icon/icon.png') }}">


		<!-- Main style sheet -->
		<link rel="stylesheet" type="text/css" href="{{ asset('theme/css/style.css') }}">
		<!-- responsive style sheet -->
		<link rel="stylesheet" type="text/css" href="{{ asset('theme/css/responsive.css') }}">

        @yield('style')


		<!-- Fix Internet Explorer ______________________________________-->

		<!--[if lt IE 9]>
			<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
			<script src="{{ asset('theme/vendor/html5shiv.js') }}"></script>
			<script src="{{ asset('theme/vendor/respond.js') }}"></script>
		<![endif]-->

		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-29099895-14', 'auto');
		  ga('send', 'pageview');

		</script>

	</head>

	<body>
		<div class="main-page-wrapper home-page-one">


			<div class="html-top-content">

				<!--
				=============================================
					Theme Header
				==============================================
				-->
				<header class="theme-main-header">
					<div class="container">
						<div class="menu-wrapper clearfix">
							<div class="logo float-left"><a href="{{ url('/') }}"><img src="{{ asset('theme/images/logo/logo.png') }}" alt="Logo"></a></div>
						</div> <!-- /.menu-wrapper -->
					</div> <!-- /.container -->
				</header> <!-- /.theme-main-header -->

				@yield('content')

			</div> <!-- /.html-top-content -->



			<!--
			=====================================================
				Footer
			=====================================================
			-->
			<footer>
				<div class="container">
					<div class="footer-data-wrapper">
						<div class="bottom-footer">
							<div class="row">
								<div class="col-lg-6 col-md-7 col-xs-12 text-right pull-right">
									<ul class="footer-menu">
										<li><a href="{{ url('agreements') }}" class="tran3s">Kullanım Sözleşmesi ve Gizlilik Koşulları</a></li>
									</ul>
									<ul class="social-icon">
										<li><a href="https://www.facebook.com/stucialapp" target="_blank" class="tran3s"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
										<li><a href="https://twitter.com/stucialapp" target="_blank" class="tran3s"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
										<li><a href="https://www.instagram.com/stucialapp" target="_blank" class="tran3s"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
									</ul>
								</div>
								<div class="col-lg-6 col-md-5 col-xs-12 footer-logo">
									<div class="logo"><a href="{{ url('/') }}"><img src="{{ asset('theme/images/logo/logo.png') }}" alt="Logo"></a></div>
									<p>Her Hakkı Saklıdır 2018 &copy; <strong>Stucial.</strong></p>
								</div>
							</div> <!-- /.row -->
						</div> <!-- /.bottom-footer -->
					</div>
				</div> <!-- /.container -->
			</footer>




	        <!-- Scroll Top Button -->
			<button class="scroll-top tran3s color-one-bg">
				<i class="fa fa-long-arrow-up" aria-hidden="true"></i>
			</button>




		<!-- Js File_________________________________ -->

		<!-- j Query -->
		<script type="text/javascript" src="{{ asset('theme/vendor/jquery.2.2.3.min.js') }}"></script>

		<!-- Bootstrap JS -->
		<script type="text/javascript" src="{{ asset('theme/vendor/bootstrap/bootstrap.min.js') }}"></script>

		<!-- Vendor js _________ -->
		<!-- Camera Slider -->
		<script type="text/javascript" src="{{ asset('theme/vendor/Camera-master/scripts/jquery.mobile.customized.min.js') }}"></script>
	    <script type="text/javascript" src="{{ asset('theme/vendor/Camera-master/scripts/jquery.easing.1.3.js') }}"></script>
	    <script type="text/javascript" src="{{ asset('theme/vendor/Camera-master/scripts/camera.min.js') }}"></script>
	    <!-- WOW js -->
		<script type="text/javascript" src="{{ asset('theme/vendor/WOW-master/dist/wow.min.js') }}"></script>
		<!-- Tilt js -->
		<script type="text/javascript" src="{{ asset('theme/vendor/tilt.js') }}/src/tilt.jquery.js') }}"></script>
		<!-- Fancybox -->
		<script type="text/javascript" src="{{ asset('theme/vendor/fancybox/dist/jquery.fancybox.min.js') }}"></script>
		<!-- Validation -->
		<script type="text/javascript" src="{{ asset('theme/vendor/contact-form/validate.js') }}"></script>
		<script type="text/javascript" src="{{ asset('theme/vendor/contact-form/jquery.form.js') }}"></script>

		<!-- owl.carousel -->
		<script type="text/javascript" src="{{ asset('theme/vendor/owl-carousel/owl.carousel.min.js') }}"></script>

		<!-- Theme js -->
		<script type="text/javascript" src="{{ asset('theme/js/theme.js') }}"></script>

        @yield('script')

		</div> <!-- /.main-page-wrapper -->
	</body>
</html>
