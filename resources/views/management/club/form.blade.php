@extends('layouts.management.master')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                {{ isset($title) ? $title:'' }}
            </header>
            <div class="panel-body">
                <div class="form">
					@if (Session::has('message'))
					<div class="flash alert-success">
							<p class="panel-body">
									{{ Session::get('message') }}
							</p>
					</div>
					@elseif (Session::has('error'))
					<div class="flash alert-danger">
							<p class="panel-body">
									{{ Session::get('error') }}
							</p>
					</div>
					@endif
                    <form class="form-horizontal" id="club-form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Ad *</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ isset($club) ? $club->name : old('name') }}" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                            <label for="description" class="col-md-4 control-label">Açıklama *</label>

                            <div class="col-md-6">
                                <textarea id="description" class="form-control" name="description">{{ isset($club) ? $club->description : old('description') }}</textarea>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                            <label for="image" class="col-md-4 control-label">Fotoğraf *</label>

                            <div class="col-md-6">
                                <input id="image" type="file" class="form-control" name="image">

                                @if ($errors->has('image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
                                <button class="btn btn-primary" type="submit">Kaydet</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </section>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('bucket/js/bootstrap-inputmask/bootstrap-inputmask.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bucket/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript">
  $("#club-form").validate();
</script>
@endsection
