@extends('layouts.management.master')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                {{ isset($title) ? $title:'' }}
            </header>
            <div class="panel-body">
                <div class="form">
					@if (Session::has('message'))
					<div class="flash alert-success">
							<p class="panel-body">
									{{ Session::get('message') }}
							</p>
					</div>
					@elseif (Session::has('error'))
					<div class="flash alert-danger">
							<p class="panel-body">
									{{ Session::get('error') }}
							</p>
					</div>
					@endif
                    <form class="form-horizontal" id="student-form" method="post">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Ad *</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ isset($student) ? $student->name : old('name') }}" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('university') ? ' has-error' : '' }}">
                            <label for="university" class="col-md-4 control-label">Üniversite *</label>

                            <div class="col-md-6">
                                <select class="form-control" name="university" required>
                                  <option value=""></option>
                                  @foreach(App\University::orderBy('name', 'asc')->get() as $university)
                                  <option value="{{ $university->id }}"{{ $university->id==(isset($student) ? $student->university_id : old('university')) ? ' selected':'' }}>{{ $university->name }}</option>
                                  @endforeach
                                </select>

                                @if ($errors->has('university'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('university') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('department') ? ' has-error' : '' }}">
                            <label for="department" class="col-md-4 control-label">Bölüm *</label>

                            <div class="col-md-6">
                                <select class="form-control" name="department" required>
                                  <option value=""></option>
                                  @foreach(App\Department::orderBy('name', 'asc')->get() as $department)
                                  <option value="{{ $department->id }}"{{ $department->id==(isset($student) ? $student->university_id : old('university')) ? ' selected':'' }}>{{ $department->name }}</option>
                                  @endforeach
                                </select>

                                @if ($errors->has('department'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('department') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('student_id') ? ' has-error' : '' }}">
                            <label for="student_id" class="col-md-4 control-label">Öğrenci Numarası *</label>

                            <div class="col-md-6">
                                <input id="student_id" type="text" class="form-control" name="student_id" value="{{ isset($student) ? $student->student_id : old('student_id') }}" required>

                                @if ($errors->has('student_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('student_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Telefon</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ isset($student) ? $student->phone : old('phone') }}">

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                            <label for="username" class="col-md-4 control-label">Kullanıcı Adı *</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control" name="username" value="{{ isset($student) ? $student->username : old('username') }}" required>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail *</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ isset($student) ? $student->email : old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Şifre {{ isset($student) ? '':' *' }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" placeholder="{{ isset($student) ? 'Değiştirmek istemiyorsanız boş bırakın.' : '' }}">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        @if(!isset($student))
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Şifre Tekrar *</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                        @endif

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
                                <button class="btn btn-primary" type="submit">Kaydet</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </section>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('bucket/js/bootstrap-inputmask/bootstrap-inputmask.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bucket/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript">
  $("#student-form").validate();
</script>
@endsection
