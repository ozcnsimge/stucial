@extends('layouts.management.master')

@section('style')
<!--dynamic table-->
    <link href="{{ asset('bucket/js/datatables/css/dataTables.material.min.css') }}" rel="stylesheet" />
    <style media="screen">
      .dataTables input.form-control{
        float: left;
      }
    </style>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                {{ isset($title) ? $title:'' }}
                <span class="tools pull-right">
                    <a class="fa fa-chevron-down" href="javascript:;"></a>
                    <a class="fa fa-cog" href="javascript:;"></a>
                    <a class="fa fa-times" href="javascript:;"></a>
                 </span>
            </header>
            <div class="panel-body">
              @if (Session::has('message'))
              <div class="flash alert-success">
                  <p class="panel-body">
                      {{ Session::get('message') }}
                  </p>
              </div>
              @elseif (Session::has('error'))
              <div class="flash alert-danger">
                  <p class="panel-body">
                      {{ Session::get('error') }}
                  </p>
              </div>
              @endif
              <table class="display table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Fotoğraf</th>
                    <th>Ad Soyad</th>
                    <th>Telefon</th>
                    <th>Üniversite</th>
                    <th>Bölüm</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($students as $student)
                  <tr class="gradeU">
                    <td>{{ $student->id }}</td>
                    <td><a href="{{ $student->photo('big') }}" target="_blank"><img src="{{ $student->photo('small') }}" height=30></a></td>
                    <td>{{ $student->name }}</td>
                    <td>+{{ $student->phone }}</td>
                    <td>{{ $student->university->name }}</td>
                    <td>{{ $student->department->name }}</td>
                    <td class="icon-column">
                      <a href="show/{{ $student->id }}"><span rel="tooltip" title="Görüntüle" data-placement="bottom"><i class="fa fa-search" aria-hidden="true"></i></span></a>
                      <a href="edit/{{ $student->id }}"><span rel="tooltip" title="Düzenle" data-placement="bottom"><i class="fa fa-pencil" aria-hidden="true"></i></span></a>
                      <a href="delete/{{ $student->id }}" onclick="return confirm('Bu kaydı silmek istediğinize emin misiniz?');"><span rel="tooltip" title="Sil" data-placement="bottom"><i class="fa fa-trash" aria-hidden="true"></i></span></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
        </section>
    </div>
</div>
@endsection

@section('script')
<!--dynamic table-->
<script type="text/javascript" language="javascript" src="{{ asset('bucket/js/datatables/js/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" language="javascript" src="{{ asset('bucket/js/datatables/js/dataTables.bootstrap.min.js') }}"></script>
<!--script for this page-->
<script type="text/javascript">
  $('table').dataTable({"aaSorting": []});
  $('[rel="tooltip"]').tooltip();
</script>

@endsection
