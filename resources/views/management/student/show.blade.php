@extends('layouts.management.master')

@section('content')
  <section class="panel">
    <div class="panel-body">
      <h1 style="color:#1fb5ad;margin:0 0 0 20px;font-weight:bold;font-size:20pt;">{{ $student->name }}</h1>
    </div>
  </section>
  @if (Session::has('message'))
    <div class="flash alert-{{ Session::get('color') }}">
        <p class="panel-body">
            {{ Session::get('message') }}
        </p>
    </div>
  @endif
  <section class="panel">
    <div class="panel-body">
        <dl class="dl-horizontal">

          <dt>Telefon</dt>
          <dd>{{ $student->phone }}</dd>

          <dt>E-mail</dt>
          <dd>{{ $student->email }}</dd>
        </dl>

      </div>
    </section>
@endsection
