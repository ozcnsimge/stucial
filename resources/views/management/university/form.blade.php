@extends('layouts.management.master')

@section('content')
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                {{ isset($title) ? $title:'' }}
            </header>
            <div class="panel-body">
                <div class="form">
					@if (Session::has('message'))
					<div class="flash alert-success">
							<p class="panel-body">
									{{ Session::get('message') }}
							</p>
					</div>
					@elseif (Session::has('error'))
					<div class="flash alert-danger">
							<p class="panel-body">
									{{ Session::get('error') }}
							</p>
					</div>
					@endif
                    <form class="form-horizontal" id="user-form" method="post">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Ad *</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ isset($university) ? $university->name : old('name') }}" required>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                            <label for="city" class="col-md-4 control-label">Şehir *</label>

                            <div class="col-md-6">
                                <input id="city" type="text" class="form-control" name="city" value="{{ isset($university) ? $university->city : old('city') }}" required>

                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Telefon</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ isset($university) ? $university->phone : old('phone') }}">

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                            <label for="address" class="col-md-4 control-label">Adres *</label>

                            <div class="col-md-6">
                                <textarea id="address" class="form-control" name="address" required>{{ isset($university) ? $university->address : old('address') }}</textarea>

                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('coordinate_lat') || $errors->has('coordinate_lng') ? ' has-error' : '' }}">
                            <label for="coordinate_lat" class="col-md-4 control-label">Koordinatlar</label>

                            <div class="col-md-3">
                                <input id="coordinate_lat" type="text" class="form-control" name="coordinate_lat" value="{{ isset($university) ? $university->coordinate_lat : old('coordinate_lat') }}" placeholder="Lat">

                                @if ($errors->has('coordinate_lat'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('coordinate_lat') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-md-3">
                                <input id="coordinate_lng" type="text" class="form-control" name="coordinate_lng" value="{{ isset($university) ? $university->coordinate_lng : old('coordinate_lng') }}" placeholder="Lng">

                                @if ($errors->has('coordinate_lng'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('coordinate_lng') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('website') ? ' has-error' : '' }}">
                            <label for="website" class="col-md-4 control-label">Website</label>

                            <div class="col-md-6">
                                <input id="website" type="text" class="form-control" name="website" value="{{ isset($university) ? $university->website : old('website') }}">

                                @if ($errors->has('website'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('website') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('mail_extension') ? ' has-error' : '' }}">
                            <label for="mail_extension" class="col-md-4 control-label">Mail Uzantısı *</label>

                            <div class="col-md-6">
                                <input id="mail_extension" type="text" class="form-control" name="mail_extension" value="{{ isset($university) ? $university->mail_extension : old('mail_extension') }}" required>

                                @if ($errors->has('mail_extension'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('mail_extension') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-offset-3 col-lg-6">
                                <button class="btn btn-primary" type="submit">Kaydet</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </section>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('bucket/js/bootstrap-inputmask/bootstrap-inputmask.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('bucket/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript">
  $("#user-form").validate();
</script>
@endsection
