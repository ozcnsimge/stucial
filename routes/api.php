<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => []], function () {

	Route::post('login', 'ApiController@login');
	Route::post('register', 'ApiController@register');
	Route::get('register-data', 'ApiController@registerData');

	Route::group(['middleware' => ['auth_api']], function () {
		Route::get('check', 'ApiController@check');

		Route::post('change-photo', 'ApiController@changePhoto');

		Route::get('settings', 'ApiController@getSettings');
		Route::post('settings', 'ApiController@setSettings');

		Route::get('logout', 'ApiController@logout');

		Route::get('profile/{id}/friends', 'ApiController@profileFriends');
		Route::get('profile/{id}/clubs', 'ApiController@profileClubs');
		Route::get('profile/{id}', 'ApiController@profile');
		Route::get('schedule', 'ApiController@schedule');

		Route::post('search', 'ApiController@search');

		Route::get('clubs', 'ApiController@clubs');
		Route::post('club/apply', 'ApiController@applyClub');
		Route::post('club/leave', 'ApiController@leaveClub');
		Route::get('club/{id}', 'ApiController@club');

		Route::get('departments', 'ApiController@departments');
		Route::get('department/{id}', 'ApiController@department');

		Route::get('announcements', 'ApiController@announcements');
		Route::get('announcement/{id}', 'ApiController@announcement');

		Route::post('course/enroll', 'ApiController@enrollSection');
		Route::post('course/disenroll', 'ApiController@disenrollSection');
		Route::post('course/{id}/add-file', 'ApiController@courseAddFile');
		Route::get('course/{id}', 'ApiController@course');

		Route::get('section/{id}', 'ApiController@section');
		Route::post('free-time', 'ApiController@freeTime');

		Route::get('friendship/requests', 'ApiController@friendRequests');
		Route::get('friendship/suggests', 'ApiController@friendSuggestions');
		Route::post('friendship/add', 'ApiController@sendFriendRequest');
		Route::post('friendship/answer', 'ApiController@answerFriendRequest');
		Route::post('friendship/remove', 'ApiController@removeFriend');
		Route::post('friendship/block', 'ApiController@blockFriend');
		Route::post('friendship/unblock', 'ApiController@unblockFriend');

		Route::get('messages', 'ApiController@messageList');
		Route::post('messages/send', 'ApiController@sendMessage');
		Route::post('messages/mute', 'ApiController@messageMute');
		Route::post('messages/leave', 'ApiController@messageLeave');
		Route::get('messages/{id}/info', 'ApiController@messageInfo');
		Route::post('messages/{id}', 'ApiController@messages');

		Route::post('event/create', 'ApiController@createEvent');
		Route::post('event/attending', 'ApiController@attending');
		Route::get('events/{year}-{month}-{day}', 'ApiController@events');
		Route::get('event/{id}', 'ApiController@event');

		Route::post('posts', 'ApiController@posts');
		Route::post('post', 'ApiController@post');
		Route::post('post/like', 'ApiController@likePost');
		Route::post('post/unlike', 'ApiController@unlikePost');
		Route::post('post/delete', 'ApiController@deletePost');
		Route::get('post/hashtags', 'ApiController@hashtags');

		Route::post('report', 'ApiController@report');
	});
});
