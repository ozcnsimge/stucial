<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('agreements', function () {
    return view('agreements');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware' => ['auth', 'admin']], function () {

	Route::get('management', 'HomeController@management');

	Route::get('management/user/list', 'UserController@all');
	Route::get('management/user/add', 'UserController@add');
	Route::post('management/user/add', 'UserController@store');
	Route::get('management/user/show/{id}', 'UserController@show');
	Route::get('management/user/edit/{id}', 'UserController@edit');
	Route::post('management/user/edit/{id}', 'UserController@update');
	Route::get('management/user/delete/{id}', 'UserController@delete');

	Route::get('management/university/list', 'UniversityController@all');
	Route::get('management/university/add', 'UniversityController@add');
	Route::post('management/university/add', 'UniversityController@store');
	Route::get('management/university/edit/{id}', 'UniversityController@edit');
	Route::post('management/university/edit/{id}', 'UniversityController@update');
	Route::get('management/university/delete/{id}', 'UniversityController@delete');

	Route::get('management/department/list', 'DepartmentController@all');
	Route::get('management/department/add', 'DepartmentController@add');
	Route::post('management/department/add', 'DepartmentController@store');
	Route::get('management/department/edit/{id}', 'DepartmentController@edit');
	Route::post('management/department/edit/{id}', 'DepartmentController@update');
	Route::get('management/department/delete/{id}', 'DepartmentController@delete');

	Route::get('management/club/list', 'ClubController@all');
	Route::get('management/club/add', 'ClubController@add');
	Route::post('management/club/add', 'ClubController@store');
	Route::get('management/club/edit/{id}', 'ClubController@edit');
	Route::post('management/club/edit/{id}', 'ClubController@update');
	Route::get('management/club/delete/{id}', 'ClubController@delete');

	Route::get('management/student/list', 'StudentController@all');
	Route::get('management/student/show/{id}', 'StudentController@show');
	Route::get('management/student/edit/{id}', 'StudentController@edit');
	Route::post('management/student/edit/{id}', 'StudentController@update');
	Route::get('management/student/delete/{id}', 'StudentController@delete');

	Route::get('management/course/import', function(){ return view('management.course.import'); });
	Route::post('management/course/import', 'CourseController@import');
	Route::get('management/create-groups', 'MessageController@createGroups');

});
